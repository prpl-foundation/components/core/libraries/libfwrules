/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stddef.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include <amxc/amxc_macros.h>
#include <amxc/amxc_llist.h>
#include <amxc/amxc_common.h>
#include <fwrules/fw_folder.h>
#include <fwrules/fw_features.h>
#include <fwrules/fw_rule.h>
#include "fw_folder_priv.h"
#include "fw_rule_priv.h"

#include <debug/sahtrace.h>

#define ME "fw_folder"

/**
   @ingroup folder
   @brief
   Global list to store all folders.
 */
static amxc_llist_t folders;

/**
   @ingroup folder
   @brief
   Anonymous struct representing a folder.
 */
typedef struct _fw_folder {
    /** Is the folder enabled? */
    bool enabled;
    /** Bitmask representing the minimum set of required features (@ref
       fw_feature_t) before the rules in the folder can be enabled. */
    int feature_requirements;
    /** Bitmask representing the current features (@ref fw_feature_t) present
       in the folder. */
    int feature_marks;
    /** Linked list of all rules (@ref fw_rule_t) in this folder. */
    amxc_llist_t* rules;
    /** Pointer to the currently fetched rule. Set by @ref
       fw_folder_fetch_default_rule or @ref fw_folder_fetch_feature_rule and
       unset by @ref fw_folder_push_rule. */
    fw_rule_t* fetched_rule;
    /** List iterator used to add the folder to the global folder list. */
    amxc_llist_it_t it;
    /** The order, which can be used to shift folders in the global folder
       list. */
    unsigned int order;
} fw_folder_t;

static inline fw_rule_t* fw_folder_get_fetched_rule(const fw_folder_t* const folder) {
    return (folder ? folder->fetched_rule : NULL);
}

static inline int fw_folder_set_fetched_rule(fw_folder_t* const folder, fw_rule_t* const rule) {
    int retval = -1;

    when_null(folder, exit);

    folder->fetched_rule = rule;
    retval = 0;

exit:
    return retval;
}

static inline bool fw_folder_features_equal(const fw_folder_t* const folder) {
    return (folder->feature_requirements == folder->feature_marks);
}

static inline bool fw_folder_has_feature_mark(const fw_folder_t* const folder,
                                              const fw_feature_t feature) {

    bool has_mark = false;

    when_null(folder, exit);
    when_false(fw_feature_is_valid(feature), exit);

    has_mark = fw_feature_contains(folder->feature_marks, feature);

exit:
    return has_mark;
}

static inline bool fw_folder_has_feature_requirement(const fw_folder_t* const folder,
                                                     const fw_feature_t feature) {

    bool has_requirement = false;

    when_null(folder, exit);
    when_false(fw_feature_is_valid(feature), exit);

    has_requirement = fw_feature_contains(folder->feature_requirements, feature);

exit:
    return has_requirement;
}

static int fw_folder_feature_mark(fw_folder_t* const folder, fw_feature_t feature, int set) {
    int retval = -1;

    when_null(folder, exit);
    when_false(fw_feature_is_valid(feature), exit);

    if(set) {
        fw_feature_set(&folder->feature_marks, feature);
    } else {
        fw_feature_unset(&folder->feature_marks, feature);
    }

    retval = 0;

exit:
    return retval;
}

static int fw_folder_set_feature_mark(fw_folder_t* const folder, fw_feature_t feature) {
    return fw_folder_feature_mark(folder, feature, 1);
}

static int fw_folder_unset_feature_mark(fw_folder_t* const folder, fw_feature_t feature) {
    return fw_folder_feature_mark(folder, feature, 0);
}

static int fw_folder_unset_feature_marks(fw_folder_t* const folder) {
    int retval = -1;

    when_null(folder, exit);
    folder->feature_marks = 0;
    retval = 0;

exit:
    return retval;
}

static void fw_folder_delete_rule(fw_folder_t* const folder, fw_rule_t* rule) {

    if(folder && rule) {

        if(fw_rule_get_flag(rule) == FW_RULE_FLAG_NEW) {
            //This new rule is still untouched. Remove it.
            fw_rule_delete(&rule);
        } else {
            fw_rule_set_flag(rule, FW_RULE_FLAG_DELETED);
            fw_rule_set_traversed(rule, 0);
        }
    }
}

static int fw_folder_reorder_rules(fw_folder_t* const folder) {
    int retval = -1;
    fw_folder_t* prev_folder = NULL;
    fw_folder_t* next_folder = NULL;
    amxc_llist_it_t* iter = NULL;
    fw_rule_t* last_rule_prev_folder = NULL;
    fw_rule_t* first_rule_next_folder = NULL;
    fw_rule_t* clone = NULL;
    fw_rule_t* prev_clone = NULL;
    fw_rule_t* prev_rule = NULL;

    when_null(folder, exit);

    iter = amxc_llist_it_get_previous(&folder->it);
    prev_folder = (iter ? amxc_container_of(iter, fw_folder_t, it) : NULL);

    iter = amxc_llist_it_get_next(&folder->it);
    next_folder = (iter ? amxc_container_of(iter, fw_folder_t, it) : NULL);

    if(next_folder) {
        iter = amxc_llist_get_first(next_folder->rules);
        first_rule_next_folder = (iter ? fw_rule_container_of_it(iter) : NULL);
    }

    if(prev_folder) {
        iter = amxc_llist_get_last(prev_folder->rules);
        last_rule_prev_folder = (iter ? fw_rule_container_of_it(iter) : NULL);
    }

    amxc_llist_for_each(it, folder->rules) {
        fw_rule_t* rule = fw_rule_container_of_it(it);
        bool is_first = (it == amxc_llist_get_first(folder->rules));
        bool is_last = (it == amxc_llist_get_last(folder->rules));
        clone = NULL;

        if(fw_rule_get_flag(rule) != FW_RULE_FLAG_NEW) {
            //Mark existing rules for deletion. Required to preserve the index in
            //the global list.
            fw_folder_delete_rule(folder, rule);

            //Clone existing rule, which are then marked as new. Required to
            //determine the new index in the global list.
            retval = fw_rule_clone(&clone, rule);
            when_failed(retval, error);

            //Insert into the folder's rules list.
            retval = amxc_llist_it_insert_after(it, fw_rule_get_list_iterator(clone));
            when_failed(retval, error);

            //Overwrite rule for further processing.
            rule = clone;
        }

        //Move rule to the correct position in global rules list.
        if(is_first) {
            //First new rule.
            if(last_rule_prev_folder) {
                retval = amxc_llist_it_insert_after(fw_rule_get_global_list_iterator(last_rule_prev_folder),
                                                    fw_rule_get_global_list_iterator(rule));
            } else {
                //No previous folder, add to the beginning of global list.
                retval = amxc_llist_prepend((amxc_llist_t*) fw_rule_get_global_list(),
                                            fw_rule_get_global_list_iterator(rule));
            }

        }

        if(is_last && first_rule_next_folder) {
            retval = amxc_llist_it_insert_before(fw_rule_get_global_list_iterator(first_rule_next_folder),
                                                 fw_rule_get_global_list_iterator(rule));
        }

        when_failed(retval, error);

        if(prev_clone != NULL) {
            retval = amxc_llist_it_insert_after(fw_rule_get_global_list_iterator(prev_clone),
                                                fw_rule_get_global_list_iterator(rule));
            when_failed(retval, error);
        } else if(prev_rule != NULL) {
            retval = amxc_llist_it_insert_after(fw_rule_get_global_list_iterator(prev_rule),
                                                fw_rule_get_global_list_iterator(rule));
            when_failed(retval, error);
        }

        prev_clone = clone;
        prev_rule = rule;

        if(clone) {
            //Skip clone.
            it = amxc_llist_it_get_next(it);
            if(it == NULL) {
                break;
            }
        }
    }

exit:
    return retval;

error:
    fw_rule_delete(&clone);
    return retval;
}

static int fw_folder_reorder(fw_folder_t* const folder, unsigned int order) {
    int retval = -1;
    unsigned int index = 0;

    when_null(folder, exit);
    when_true(!order, exit);

    retval = 0;

    if(order != folder->order) {
        amxc_llist_it_t* folder_at_order = amxc_llist_get_at(&folders, order - 1);

        if((folder_at_order != &folder->it) && (order < folder->order)) {
            retval = amxc_llist_it_insert_before(folder_at_order, &folder->it);
        } else if(folder_at_order != &folder->it) {
            retval = amxc_llist_it_insert_after(amxc_llist_get_at(&folders, order - 1), &folder->it);
        }

        when_failed(retval, exit);

        amxc_llist_for_each(it, &folders) {
            fw_folder_t* f = amxc_container_of(it, fw_folder_t, it);

            if(f) {
                f->order = (++index);
            }
        }

        retval = fw_folder_reorder_rules(folder);
    }

exit:
    return retval;
}

static int fw_folder_init_rules(fw_folder_t* const folder) {
    int retval = -1;

    when_null(folder, exit);
    retval = amxc_llist_new(&folder->rules);

exit:
    return retval;
}

static void fw_folder_enable_rules(const fw_folder_t* const folder) {
    bool enable = 0;

    if(!folder) {
        return;
    }

    enable = fw_folder_get_enabled(folder) && fw_folder_features_equal(folder);

    amxc_llist_for_each(it, folder->rules) {
        fw_rule_t* rule = fw_rule_container_of_it(it);

        if(!rule) {
            continue;
        }

        if(enable != fw_rule_is_enabled(rule)) {
            fw_rule_set_enabled(rule, enable);

            if(fw_rule_get_flag(rule) == FW_RULE_FLAG_HANDLED) {
                fw_rule_set_flag(rule, FW_RULE_FLAG_MODIFIED);
            }
        }
    }

}

static int fw_folder_create_rule(fw_folder_t* folder) {
    int retval = -1;
    fw_rule_t* rule = NULL;
    amxc_llist_it_t* it = NULL;
    fw_rule_t* prev_rule = NULL;

    when_null(folder, exit);

    retval = fw_rule_new(&rule);
    when_failed(retval, exit);

    retval = amxc_llist_append(folder->rules, fw_rule_get_list_iterator(rule));
    when_failed(retval, error);

    /**
     * A new rule is appended to the global rule list. If rules are already present
     * in this folder, the rule must be appended to the last rule in this folder, and
     * therefore, the global list must be altered too.
     */
    it = amxc_llist_it_get_previous(fw_rule_get_list_iterator(rule));
    prev_rule = (it ? fw_rule_container_of_it(it) : NULL);

    if(prev_rule) {
        amxc_llist_it_insert_after(fw_rule_get_global_list_iterator(prev_rule),
                                   fw_rule_get_global_list_iterator(rule));
    }

exit:
    return retval;
error:
    fw_rule_delete(&rule);
    return retval;
}

int fw_folder_init(fw_folder_t* folder) {
    int retval = -1;

    when_null(folder, exit);
    memset(folder, 0, sizeof(*folder));

    retval = fw_folder_init_rules(folder);
    when_failed(retval, error);

    retval = fw_folder_create_rule(folder);
    when_failed(retval, error);

    retval = fw_folder_set_order(folder, 0);
    when_failed(retval, error);

exit:
    return retval;
error:
    fw_folder_deinit(folder);
    return retval;
}

int fw_folder_new(fw_folder_t** folder) {
    int retval = -1;

    when_null(folder, exit);
    when_not_null(*folder, exit);

    *folder = (fw_folder_t*) malloc(sizeof(fw_folder_t));
    when_null(*folder, exit);

    retval = fw_folder_init(*folder);
    when_failed(retval, error);

exit:
    return retval;
error:
    fw_folder_delete(folder);
    return retval;
}

static amxc_llist_it_t* fw_folder_get_previous_folder_global_iterator(const fw_folder_t* const folder) {
    amxc_llist_it_t* previous_it = NULL;
    amxc_llist_it_t* folder_first_it = NULL;
    amxc_llist_it_t* folder_first_global_it = NULL;
    fw_rule_t* folder_first_rule = NULL;

    when_null(folder, exit);

    folder_first_it = amxc_llist_get_first(folder->rules);
    when_null(folder_first_it, exit);

    folder_first_rule = fw_rule_container_of_it(folder_first_it);
    folder_first_global_it = fw_rule_get_global_list_iterator(folder_first_rule);
    when_null(folder_first_global_it, exit);

    previous_it = amxc_llist_it_get_previous(folder_first_global_it);

exit:
    return previous_it;
}

static amxc_llist_it_t* fw_folder_get_next_folder_global_iterator(const fw_folder_t* const folder) {
    amxc_llist_it_t* next_global_it = NULL;
    amxc_llist_it_t* folder_last_it = NULL;
    amxc_llist_it_t* folder_last_global_it = NULL;
    fw_rule_t* folder_last_rule = NULL;

    when_null(folder, exit);

    folder_last_it = amxc_llist_get_last(folder->rules);
    when_null(folder_last_it, exit);

    folder_last_rule = fw_rule_container_of_it(folder_last_it);
    folder_last_global_it = fw_rule_get_global_list_iterator(folder_last_rule);
    when_null(folder_last_global_it, exit);

    next_global_it = amxc_llist_it_get_next(folder_last_global_it);

exit:
    return next_global_it;
}

static int fw_folder_delete_rules_internal(fw_folder_t* const folder, const bool reinit_rule) {
    int retval = -1;
    fw_rule_t* rule = NULL;
    amxc_llist_it_t* prev_global = NULL;
    amxc_llist_it_t* next_global = NULL;

    when_null(folder, exit);

    prev_global = fw_folder_get_previous_folder_global_iterator(folder);
    next_global = fw_folder_get_next_folder_global_iterator(folder);

    amxc_llist_for_each(it, folder->rules) {
        rule = fw_rule_container_of_it(it);

        if(!rule) {
            continue;
        }

        fw_folder_delete_rule(folder, rule);
    }

    retval = fw_folder_unset_feature_marks(folder);
    when_failed(retval, exit);

    if(reinit_rule) {
        retval = fw_folder_create_rule(folder);
        when_failed(retval, exit);
        if(prev_global != NULL) {
            amxc_llist_it_insert_after(prev_global,
                                       fw_rule_get_global_list_iterator(fw_rule_container_of_it(amxc_llist_get_first(folder->rules))));
        } else if(next_global != NULL) {
            amxc_llist_it_insert_before(next_global,
                                        fw_rule_get_global_list_iterator(fw_rule_container_of_it(amxc_llist_get_first(folder->rules))));
        }
    }

    fw_folder_enable_rules(folder);

exit:
    return retval;
}

int fw_folder_delete_rules(fw_folder_t* const folder) {
    return fw_folder_delete_rules_internal(folder, true);
}

int fw_folder_deinit(fw_folder_t* folder) {
    int retval = -1;

    when_null(folder, exit);

    retval = fw_folder_delete_rules_internal(folder, false);
    when_failed(retval, exit);

    amxc_llist_delete(&folder->rules, NULL);
    amxc_llist_it_take(&folder->it);
    memset(folder, 0, sizeof(*folder));
    retval = 0;

exit:
    return retval;
}

int fw_folder_delete(fw_folder_t** folder) {
    int retval = -1;

    when_null(folder, exit);
    when_null(*folder, exit);

    retval = fw_folder_deinit(*folder);
    when_failed(retval, exit);

    free(*folder);
    *folder = NULL;
    retval = 0;

exit:
    return retval;
}

int fw_folder_set_enabled(fw_folder_t* const folder, const bool enable) {
    int retval = -1;

    when_null(folder, exit);

    folder->enabled = enable;
    fw_folder_enable_rules(folder);

    retval = 0;

exit:
    return retval;
}

bool fw_folder_get_enabled(const fw_folder_t* const folder) {
    bool retval = false;

    when_null(folder, exit);
    retval = folder->enabled;

exit:
    return retval;
}

static int fw_folder_feature(fw_folder_t* const folder, fw_feature_t feature, int set) {
    int retval = -1;
    bool old_features = false;
    bool new_features = false;

    when_null(folder, exit);
    when_false(fw_feature_is_valid(feature), exit);

    old_features = fw_folder_features_equal(folder);

    if(set) {
        fw_feature_set(&folder->feature_requirements, feature);
    } else {
        fw_feature_unset(&folder->feature_requirements, feature);
    }

    new_features = fw_folder_features_equal(folder);

    if(old_features != new_features) {
        fw_folder_enable_rules(folder);
    }

    retval = 0;

exit:
    return retval;
}

int fw_folder_set_feature(fw_folder_t* const folder, fw_feature_t feature) {
    return fw_folder_feature(folder, feature, 1);
}

int fw_folder_unset_feature(fw_folder_t* const folder, fw_feature_t feature) {
    return fw_folder_feature(folder, feature, 0);
}

int fw_folder_get_feature_requirements(const fw_folder_t* const folder) {
    int retval = 0;

    when_null(folder, exit);
    retval = folder->feature_requirements;
exit:
    return retval;
}

int fw_folder_get_feature_marks(const fw_folder_t* const folder) {
    int retval = 0;

    when_null(folder, exit);
    retval = folder->feature_marks;
exit:
    return retval;
}

fw_rule_t* fw_folder_fetch_default_rule(fw_folder_t* const folder) {
    fw_rule_t* rule = NULL;

    when_null(folder, exit);
    when_not_null(fw_folder_get_fetched_rule(folder), exit);
    when_true((amxc_llist_size(folder->rules) > 1), exit);

    rule = fw_rule_container_of_it(amxc_llist_get_last(folder->rules));

    fw_folder_set_fetched_rule(folder, rule);

exit:
    return rule;
}

fw_rule_t* fw_folder_fetch_feature_rule(fw_folder_t* const folder, const fw_feature_t feature) {
    int retval = -1;
    fw_rule_t* rule = NULL;
    fw_rule_t* clone = NULL;

    when_null(folder, exit);
    when_false(fw_feature_is_valid(feature), exit);
    when_false(fw_folder_has_feature_requirement(folder, feature), exit);
    when_not_null(fw_folder_get_fetched_rule(folder), exit);

    //Do we have a least one rule with this feature?
    if(!fw_folder_has_feature_mark(folder, feature)) {

        retval = fw_folder_set_feature_mark(folder, feature);
        when_failed(retval, exit);

        fw_folder_enable_rules(folder);

    } else {
        amxc_llist_for_each(it, folder->rules) {
            clone = NULL;
            rule = fw_rule_container_of_it(it);

            if(!rule) {
                continue;
            }

            if(fw_feature_contains(fw_rule_get_feature_marks(rule), feature)) {
                continue;
            }

            retval = fw_rule_clone(&clone, rule);
            when_failed(retval, error);

            retval = amxc_llist_it_insert_after(it, fw_rule_get_list_iterator(clone));
            when_failed(retval, error);

            retval = fw_rule_set_feature_mark(rule, feature);
            when_failed(retval, error);

            //Skip clone.
            it = amxc_llist_it_get_next(it);
            if(it == NULL) {
                break;
            }
        }
    }

    rule = fw_rule_container_of_it(amxc_llist_get_last(folder->rules));
    fw_rule_set_current_feature(rule, feature);
    fw_folder_set_fetched_rule(folder, rule);
exit:
    return rule;
error:
    fw_rule_delete(&clone);
    return NULL;
}

int fw_folder_push_rule(fw_folder_t* const folder, fw_rule_t* const rule) {
    int retval = -1;
    fw_feature_t feature = FW_FEATURE_LAST;

    when_null(folder, exit);
    when_null(rule, exit);

    if(rule != fw_folder_get_fetched_rule(folder)) {
        goto exit;
    }

    feature = fw_rule_get_current_feature(rule);

    if(FW_FEATURE_LAST == feature) {
        //Only sync features rules.
        goto out;
    }

    amxc_llist_for_each(it, folder->rules) {
        fw_rule_t* ll_rule = fw_rule_container_of_it(it);

        if(!ll_rule) {
            continue;
        }

        if(ll_rule == rule) {
            continue;
        }

        if(fw_feature_contains(fw_rule_get_feature_marks(ll_rule), feature)) {
            continue;
        }

        retval = fw_rule_sync(ll_rule, rule);
        when_failed(retval, exit);

        if(fw_rule_get_flag(rule) == FW_RULE_FLAG_HANDLED) {
            fw_rule_set_flag(ll_rule, FW_RULE_FLAG_MODIFIED);
        }
        fw_rule_set_traversed(ll_rule, 0);
    }

    fw_rule_set_current_feature(rule, FW_FEATURE_LAST);

out:
    if(fw_rule_get_flag(rule) == FW_RULE_FLAG_HANDLED) {
        fw_rule_set_flag(rule, FW_RULE_FLAG_MODIFIED);
    }
    fw_rule_set_traversed(rule, 0);
    fw_folder_set_fetched_rule(folder, NULL);
    retval = 0;

exit:
    return retval;
}

int fw_folder_delete_feature_rules(fw_folder_t* const folder, const fw_feature_t feature) {
    int retval = -1;

    when_null(folder, exit);
    when_false(fw_feature_is_valid(feature), exit);
    when_false(fw_folder_has_feature_requirement(folder, feature), exit);
    when_not_null(fw_folder_get_fetched_rule(folder), exit);

    amxc_llist_for_each(it, folder->rules) {
        fw_rule_t* rule = fw_rule_container_of_it(it);

        if(!rule) {
            continue;
        }

        if(!fw_feature_contains(fw_rule_get_feature_marks(rule), feature)) {
            continue;
        }

        fw_folder_delete_rule(folder, rule);
    }

    fw_folder_unset_feature_mark(folder, feature);
    fw_folder_enable_rules(folder);
    retval = 0;

exit:
    return retval;
}

int fw_folder_set_order(fw_folder_t* const folder, unsigned int order) {
    int retval = -1;
    unsigned int next_order = amxc_llist_size(&folders) + 1;

    when_null(folder, exit);
    when_not_null(fw_folder_get_fetched_rule(folder), exit);

    retval = 0;

    if(!order && !folder->order) {
        //Auto assign
        folder->order = next_order;
        retval = amxc_llist_append(&folders, &folder->it);

    } else if(order != folder->order) {
        if(order > next_order - 1) {
            //Re-assign
            order = next_order - 1;
        }

        retval = fw_folder_reorder(folder, order);
    }

exit:
    return retval;
}

unsigned int fw_folder_get_order(fw_folder_t* const folder) {
    return (folder ? folder->order : 0);
}

