/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include <amxc/amxc_macros.h>
#include <amxc/amxc_llist.h>
#include <amxc/amxc_variant.h>
#include <fwrules/fw_rule.h>
#include <fwrules/fw.h>
#include "fw_rule_priv.h"

#include <debug/sahtrace.h>

#define ME "fw_rule"

/**
   @ingroup rule
   @brief
   Anonymous struct representing a firewall rule.
 */
typedef struct _fw_rule {
    /** List iterator used to add the rule to a folder. */
    amxc_llist_it_t it;
    /** List iterator used to add the rule to the global rules list. */
    amxc_llist_it_t g_it;
    /** The flag, representing the rule's state: new, modified or deleted. */
    fw_rule_flag_t flag;
    /** Is the rule enabled? */
    bool enabled;
    /** Bitmask of feature flags as defined in @ref fw_feature_t. */
    int feature_marks;
    /** Hashtable containing the rule's parameters, except the frequently used ones. */
    amxc_var_t* ht;
    /** The current feature for the rule. Used by the folder API. */
    fw_feature_t current_feature;
    /** Internal state, used when committing the folder by @ref fw_commit. */
    int traversed;
    /** Frequently used parameters, like the table and chain. */
    struct {
        char old_table[FW_RULE_TABLE_LEN];
        char old_chain[FW_RULE_CHAIN_LEN];
        char table[FW_RULE_TABLE_LEN];
        char chain[FW_RULE_CHAIN_LEN];
    } params;
} fw_rule_t;

/**
   @ingroup rule
   @brief
   Table of supported ICMP type/code combinations,
   a code value of -1 means it is not used.
 */
std_icmp_pair_t icmp_code_type[] = {
    {255, -1}, /**< any */
    {0, -1},   /**< echo-reply */
    {0, 0},    /**< echo-reply */
    {3, -1},   /**< destination-unreachable */
    {3, 0},    /**< network-unreachable */
    {3, 1},    /**< host-unreachable */
    {3, 2},    /**< protocol-unreachable */
    {3, 3},    /**< port-unreachable */
    {3, 4},    /**< fragmentation-needed */
    {3, 5},    /**< source-route-failed */
    {3, 6},    /**< network-unknown */
    {3, 7},    /**< host-unknown */
    {3, 8},    /**< source host isolated */
    {3, 9},    /**< network-prohibited */
    {3, 10},   /**< host-prohibited */
    {3, 11},   /**< TOS-network-unreachable */
    {3, 12},   /**< TOS-host-unreachable */
    {3, 13},   /**< communication-prohibited */
    {3, 14},   /**< host-precedence-violation */
    {3, 15},   /**< precedence-cutoff */
    {4, -1},   /**< source-quench */
    {4, 0},    /**< source-quench */
    {5, -1},   /**< redirect */
    {5, 0},    /**< network-redirect */
    {5, 1},    /**< host-redirect */
    {5, 2},    /**< TOS-network-redirect */
    {5, 3},    /**< TOS-host-redirect */
    {6, -1},   /**< alternate-host-address */
    {6, 0},    /**< alternate-host-address */
    {8, -1},   /**< echo-request */
    {8, 0},    /**< echo-request */
    {9, -1},   /**< router-advertisement */
    {9, 0},    /**< normal-router-advertisement */
    {9, 16},   /**< does-not-route-common-traffic */
    {10, -1},  /**< router-solicitation */
    {10, 0},   /**< router-solicitation */
    {11, -1},  /**< time-exceeded */
    {11, 0},   /**< ttl-zero-during-transit */
    {11, 1},   /**< ttl-zero-during-reassembly */
    {12, -1},  /**< parameter-problem */
    {12, 0},   /**< ip-header-bad */
    {12, 1},   /**< required-option-missing */
    {12, 2},   /**< bad-length */
    {13, -1},  /**< timestamp-request */
    {13, 0},   /**< timestamp-request */
    {14, -1},  /**< timestamp-reply */
    {14, 0},   /**< timestamp-reply */
    {15, -1},  /**< info-request */
    {15, 0},   /**< info-request */
    {16, -1},  /**< info-reply */
    {16, 0},   /**< info-reply */
    {17, -1},  /**< address-mask-request */
    {17, 0},   /**< address-mask-request */
    {18, -1},  /**< address-mask-reply */
    {18, 0},   /**< address-mask-reply */
    {30, -1},  /**< traceroute */
    {30, 0},   /**< traceroute */
    {31, -1},  /**< dgram-conversion-error */
    {31, 0},   /**< dgram-conversion-error */
    {32, -1},  /**< mobile-host-redirect */
    {32, 0},   /**< mobile-host-redirect */
    {33, -1},  /**< where-are-you */
    {33, 0},   /**< where-are-you */
    {34, -1},  /**< i-am-here */
    {34, 0},   /**< i-am-here */
    {35, -1},  /**< mobile-registration-request */
    {35, 0},   /**< mobile-registration-request */
    {36, -1},  /**< mobile-registration-reply */
    {36, 0},   /**< mobile-registration-reply */
    {37, -1},  /**< domain-name-request */
    {37, 0},   /**< domain-name-request */
    {38, -1},  /**< domain-name-reply */
    {38, 0},   /**< domain-name-reply */
    {39, -1},  /**< SKIP */
    {39, 0},   /**< SKIP */
    {40, -1},  /**< photuris */
    {40, 0},   /**< bad-SPI */
    {40, 1},   /**< authentication-failed */
    {40, 2},   /**< decompression-failed */
    {40, 3},   /**< decryption-failed */
    {40, 4},   /**< need-authentication */
    {40, 5},   /**< need-authorization */
    {41, -1},  /**< seamoby */
    {41, 0},   /**< seamoby */
    {42, -1},  /**< extended-echo-request */
    {42, 0},   /**< no-error */
    {43, -1},  /**< extended-echo-reply */
    {43, 0},   /**< no-error */
    {43, 1},   /**< malformed-query */
    {43, 2},   /**< no-such-interface */
    {43, 3},   /**< no-such-table-entry */
    {43, 4},   /**< multiple-interfaces */
    {253, -1}, /**< RFC3692-experiment-1 */
    {253, 0},  /**< RFC3692-experiment-1 */
    {254, -1}, /**< RFC3692-experiment-2 */
    {254, 0},  /**< RFC3692-experiment-2 */
    {-1, -1}   /**< delimiter */
};

/**
   @ingroup rule
   @brief
   Table of supported ICMPv6 type/code combinations,
   a code value of -1 means it is not used.
 */
std_icmp_pair_t icmpv6_code_type[] = {
    {1, -1},    /**< destination-unreachable */
    {1, 0},     /**< no-route */
    {1, 1},     /**< communication-prohibited */
    {1, 2},     /**< beyond-scope */
    {1, 3},     /**< address-unreachable */
    {1, 4},     /**< port-unreachable */
    {1, 5},     /**< failed-policy */
    {1, 6},     /**< reject-route */
    {1, 7},     /**< source-routing-header-error */
    {1, 8},     /**< headers-too-long */
    {2, -1},    /**< packet-too-big */
    {2, 0},     /**< packet-too-big */
    {3, -1},    /**< time-exceeded */
    {3, 0},     /**< ttl-zero-during-transit */
    {3, 1},     /**< ttl-zero-during-reassembly */
    {4, -1},    /**< parameter-problem */
    {4, 0},     /**< bad-header */
    {4, 1},     /**< unknown-header-type */
    {4, 2},     /**< unknown-option */
    {4, 3},     /**< first-fragment-incomplete */
    {4, 4},     /**< SR-header-error */
    {4, 5},     /**< unrecognized-header-type */
    {4, 6},     /**< header-too-big */
    {4, 7},     /**< header-too-long */
    {4, 8},     /**< too-many-headers */
    {4, 9},     /**< too-many-header-options */
    {4, 10},    /**< option-too-big */
    {128, -1},  /**< echo-request */
    {128, 0},   /**< echo-request */
    {129, -1},  /**< echo-reply */
    {129, 0},   /**< echo-reply */
    {130, -1},  /**< mcast-listener-query */
    {130, 0},   /**< mcast-listener-query */
    {131, -1},  /**< mcast-listener-report */
    {131, 0},   /**< mcast-listener-report */
    {132, -1},  /**< mcast-listener-done */
    {132, 0},   /**< mcast-listener-done */
    {133, -1},  /**< router-solicitation */
    {133, 0},   /**< router-solicitation */
    {134, -1},  /**< router-advertisement */
    {134, 0},   /**< router-advertisement */
    {135, -1},  /**< neighbour-solicitation */
    {135, 0},   /**< neighbour-solicitation */
    {136, -1},  /**< neighbour-advertisement */
    {136, 0},   /**< neighbour-advertisement */
    {137, -1},  /**< redirect */
    {137, 0},   /**< redirect */
    {138, -1},  /**< router-renumbering */
    {138, 0},   /**< router-renumbering-command */
    {138, 1},   /**< router-renumbering-result */
    {138, 255}, /**< sequence-number-reset */
    {139, -1},  /**< node-info-query */
    {139, 0},   /**< contains-ipv6 */
    {139, 1},   /**< contains-name */
    {139, 2},   /**< contains-ipv4 */
    {140, -1},  /**< node-info-response */
    {140, 0},   /**< success */
    {140, 1},   /**< refusal */
    {140, 2},   /**< unknown */
    {141, -1},  /**< inverse-neighbor-disco */
    {141, 0},   /**< inverse-neighbor-disco */
    {142, -1},  /**< inverse-neighbor-disco */
    {142, 0},   /**< inverse-neighbor-disco */
    {144, -1},  /**< home-agent-disco */
    {144, 0},   /**< home-agent-disco */
    {145, -1},  /**< home-agent-disco */
    {145, 0},   /**< home-agent-disco */
    {146, -1},  /**< mobile-prefix-solicitation */
    {146, 0},   /**< mobile-prefix-solicitation */
    {147, -1},  /**< mobile-prefix-advertisement */
    {147, 0},   /**< mobile-prefix-advertisement */
    {157, -1},  /**< duplicate-addr-request-code-suffix */
    {157, 0},   /**< DAR */
    {157, 1},   /**< EDAR-64 */
    {157, 2},   /**< EDAR-128 */
    {157, 3},   /**< EDAR-192 */
    {157, 4},   /**< EDAR-256 */
    {158, -1},  /**< duplicate-addr-confirmation-code-suffix */
    {158, 0},   /**< DAC */
    {158, 1},   /**< EDAC-64 */
    {158, 2},   /**< EDAC-128 */
    {158, 3},   /**< EDAC-192 */
    {158, 4},   /**< EDAC-256 */
    {160, -1},  /**< extended-echo-request */
    {160, 0},   /**< no-error */
    {161, -1},  /**< extended-echo-reply */
    {161, 0},   /**< no-error */
    {161, 1},   /**< malformed-query */
    {161, 2},   /**< no-such-interface */
    {161, 3},   /**< no-such-table-entry */
    {161, 4},   /**< multiple-interfaces */
    {-1, -1}    /**< delimiter */
};

/**
   @ingroup rule
   @brief
   Global list to store all rules.
 */
static amxc_llist_t g_rules;

#define fw_rule_ht_set(type, ht, key, value) \
    ({ \
        int ret = 0; \
        amxc_var_t* var = amxc_var_get_key(ht, key, AMXC_VAR_FLAG_DEFAULT); \
        ret = (var != NULL ? \
               (amxc_var_set_ ## type(var, value) == -1) : \
               -1); \
        var = (ret == -1 ? \
               amxc_var_add_new_key_ ## type(ht, key, value) : \
               (ret == 1 ? NULL : var)); \
        ret = (var == NULL ? -1 : 0); \
        ret; \
    });

#define fw_rule_ht_unset(ht, key) \
    ({ \
        amxc_var_t* var = amxc_var_get_key(ht, key, AMXC_VAR_FLAG_DEFAULT); \
        amxc_var_delete(&var); \
    })

#define fw_rule_ht_get(type, ht, key) \
    ({ \
        amxc_var_t* var = amxc_var_get_key(ht, key, AMXC_VAR_FLAG_DEFAULT); \
        amxc_var_get_const_ ## type(var); \
    })

#define fw_rule_sync_cstring_t(dest, src, param) \
    ({ \
        int ret = -1; \
        const char* value = fw_rule_get_ ## param(src); \
        ret = (value != NULL ? fw_rule_set_ ## param(dest, value) : 0); \
        ret; \
    })

#define fw_rule_sync_uint32_t(dest, src, param) \
    ({ \
        int ret = -1; \
        uint32_t value = fw_rule_get_ ## param(src); \
        ret = fw_rule_set_ ## param(dest, value); \
        ret; \
    })

#define fw_rule_sync_bool(dest, src, param) \
    ({ \
        int ret = -1; \
        bool value = fw_rule_get_ ## param(src); \
        ret = fw_rule_set_ ## param(dest, value); \
        ret; \
    })

#define fw_rule_sync_string(dest, src, param) \
    ({ \
        int ret = -1; \
        ret = (strncpy(dest->params.param, src->params.param, \
                       sizeof(dest->params.param) - 1) == dest->params.param) \
            ? 0 : -1; \
        ret; \
    })

static bool fw_rule_flag_is_valid(const fw_rule_flag_t flag) {
    return (FW_RULE_FLAG_LAST > flag);
}

inline fw_feature_t fw_rule_get_current_feature(const fw_rule_t* const rule) {
    return (rule ? rule->current_feature : FW_FEATURE_LAST);
}

int fw_rule_set_current_feature(fw_rule_t* const rule, fw_feature_t feature) {
    int retval = -1;

    when_null(rule, exit);
    //No feature check here.
    rule->current_feature = feature;
    retval = 0;

exit:
    return retval;
}

static int fw_rule_feature_mark(fw_rule_t* const rule, fw_feature_t feature, int set) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_feature_is_valid(feature), exit);

    if(set) {
        fw_feature_set(&rule->feature_marks, feature);
    } else {
        fw_feature_unset(&rule->feature_marks, feature);
    }

    retval = 0;

exit:
    return retval;
}

int fw_rule_set_feature_mark(fw_rule_t* const rule, fw_feature_t feature) {
    return fw_rule_feature_mark(rule, feature, 1);
}

int fw_rule_set_flag(fw_rule_t* const rule, const fw_rule_flag_t flag) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_flag_is_valid(flag), exit);

    rule->flag = flag;
    retval = 0;

exit:
    return retval;
}

int fw_rule_init(fw_rule_t* rule) {
    int retval = -1;

    when_null(rule, exit);

    retval = amxc_var_new(&rule->ht);
    when_failed(retval, exit);

    retval = fw_rule_set_flag(rule, FW_RULE_FLAG_NEW);
    when_failed(retval, exit);

    retval = amxc_var_set_type(rule->ht, AMXC_VAR_ID_HTABLE);
    when_failed(retval, exit);

    retval = fw_rule_set_current_feature(rule, FW_FEATURE_LAST);
    when_failed(retval, exit);

    retval = amxc_llist_append(&g_rules, &rule->g_it);

exit:
    return retval;
}

int fw_rule_new(fw_rule_t** rule) {
    int retval = -1;

    when_null(rule, exit);
    when_not_null(*rule, exit);

    *rule = (fw_rule_t*) calloc(1, sizeof(fw_rule_t));
    when_null(*rule, exit);

    retval = fw_rule_init(*rule);
    when_failed(retval, error);

exit:
    return retval;
error:
    fw_rule_delete(rule);
    return retval;
}

int fw_rule_clone(fw_rule_t** dest, fw_rule_t* const src) {
    int retval = -1;

    when_null(src, exit);
    when_null(dest, exit);
    when_not_null(*dest, exit);

    retval = fw_rule_new(dest);
    when_failed(retval, exit);

    retval = amxc_var_copy((*dest)->ht, src->ht);
    when_failed(retval, error);

    retval = fw_rule_set_table(*dest, fw_rule_get_table(src));
    when_failed(retval, error);

    retval = fw_rule_set_chain(*dest, fw_rule_get_chain(src));
    when_failed(retval, error);

    (*dest)->enabled = src->enabled;
    (*dest)->feature_marks = src->feature_marks;

    retval = amxc_llist_it_insert_after(&src->g_it, &(*dest)->g_it);
    when_failed(retval, error);

exit:
    return retval;
error:
    fw_rule_delete(dest);
    return retval;
}

static inline int fw_rule_clear_target(fw_rule_t* rule) {
    int retval = -1;
    amxc_var_t* target = NULL;
    amxc_var_t* target_option = NULL;

    when_null(rule, exit);

    target = amxc_var_take_key(rule->ht, "target");
    target_option = amxc_var_take_key(rule->ht, "target_option");

    if(target) {
        amxc_var_delete(&target);
    }

    if(target_option) {
        amxc_var_delete(&target_option);
    }

    retval = 0;

exit:
    return retval;
}

int fw_rule_deinit(fw_rule_t* rule) {
    int retval = -1;

    when_null(rule, exit);

    amxc_llist_it_take(&rule->it);
    amxc_llist_it_take(&rule->g_it);
    amxc_llist_it_take(&rule->ht->lit);
    fw_rule_clear_target(rule);
    amxc_var_delete(&rule->ht);

    retval = 0;

exit:
    return retval;
}

int fw_rule_delete(fw_rule_t** rule) {
    int retval = -1;

    when_null(rule, exit);
    when_null(*rule, exit);

    retval = fw_rule_deinit(*rule);
    when_failed(retval, exit);

    free(*rule);
    *rule = NULL;
    retval = 0;

exit:
    return retval;
}

amxc_llist_it_t* fw_rule_get_list_iterator(fw_rule_t* const rule) {
    amxc_llist_it_t* it = NULL;

    when_null(rule, exit);
    it = &rule->it;

exit:
    return it;
}

amxc_llist_it_t* fw_rule_get_global_list_iterator(fw_rule_t* const rule) {
    amxc_llist_it_t* it = NULL;

    when_null(rule, exit);
    it = &rule->g_it;

exit:
    return it;
}

fw_rule_t* fw_rule_container_of_it(amxc_llist_it_t* it) {
    return ((fw_rule_t*) (((char*) it) - offsetof(fw_rule_t, it)));
}

fw_rule_t* fw_rule_container_of(amxc_llist_it_t* it) {
    return ((fw_rule_t*) (((char*) it) - offsetof(fw_rule_t, g_it)));
}

fw_rule_flag_t fw_rule_get_flag(const fw_rule_t* const rule) {
    return (rule ? rule->flag : FW_RULE_FLAG_LAST);
}

int fw_rule_get_feature_marks(fw_rule_t* const rule) {
    return (rule ? rule->feature_marks : 0);
}

bool fw_rule_is_enabled(const fw_rule_t* const rule) {
    return (rule ? rule->enabled : false);
}

int fw_rule_set_enabled(fw_rule_t* const rule, bool enable) {
    int retval = -1;

    when_null(rule, exit);
    rule->enabled = enable;
    retval = 0;

exit:
    return retval;
}

int fw_rule_dump(const fw_rule_t* rule, int fd) {
    amxc_var_t var_dump;
    int retval = -1;

    amxc_var_init(&var_dump);

    when_null(rule, exit);
    when_false(fd >= 0, exit);

    retval = amxc_var_set_type(&var_dump, AMXC_VAR_ID_HTABLE);
    when_failed(retval, exit);

    amxc_var_copy(&var_dump, rule->ht);
    amxc_var_add_key(cstring_t, &var_dump, "table", rule->params.table);
    amxc_var_add_key(cstring_t, &var_dump, "chain", rule->params.chain);

    retval = amxc_var_dump(&var_dump, fd);

exit:
    amxc_var_clean(&var_dump);
    return retval;
}

int fw_rule_set_ipv6(fw_rule_t* const rule, bool is_ipv6) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_IPVERSION,
               exit);

    retval = fw_rule_ht_set(bool, rule->ht, "ipv6", is_ipv6);

exit:
    return retval;
}

int fw_rule_set_ipv4(fw_rule_t* const rule, bool is_ipv4) {
    return fw_rule_set_ipv6(rule, !is_ipv4);
}

bool fw_rule_get_ipv6(const fw_rule_t* const rule) {
    bool ipv6 = false;

    when_null(rule, exit);
    ipv6 = fw_rule_ht_get(bool, rule->ht, "ipv6");

exit:
    return ipv6;
}

bool fw_rule_get_ipv4(const fw_rule_t* const rule) {
    return !fw_rule_get_ipv6(rule);
}

int fw_rule_set_table(fw_rule_t* const rule, const char* table) {
    int retval = -1;

    when_null(rule, exit);
    when_null(table, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_TABLE,
               exit);

    when_false(strncpy(rule->params.table, table, sizeof(rule->params.table) - 1)
               == rule->params.table,
               exit);

    retval = 0;

exit:
    return retval;
}

int fw_rule_set_old_table(fw_rule_t* const rule, const char* old_table) {
    int retval = -1;

    when_null(rule, exit);
    when_null(old_table, exit);
    //No feature checks.

    when_false(strncpy(rule->params.old_table, old_table, sizeof(rule->params.old_table) - 1)
               == rule->params.old_table,
               exit);

    retval = 0;

exit:
    return retval;
}

const char* fw_rule_get_table(const fw_rule_t* const rule) {
    const char* table = NULL;

    when_null(rule, exit);
    table = rule->params.table;

exit:
    return table;
}

const char* fw_rule_get_old_table(const fw_rule_t* const rule) {
    const char* old_table = NULL;

    when_null(rule, exit);
    old_table = rule->params.old_table;

exit:
    return old_table;
}

int fw_rule_set_in_interface(fw_rule_t* const rule, const char* in_interface) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_WHITELIST ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_POLICY ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_SPOOFING ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_ISOLATION ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_IN_INTERFACE,
               exit);

    retval = fw_rule_ht_set(cstring_t, rule->ht, "in_interface", in_interface);
exit:
    return retval;
}

const char* fw_rule_get_in_interface(const fw_rule_t* const rule) {
    const char* in_interface = NULL;

    when_null(rule, exit);
    in_interface = fw_rule_ht_get(cstring_t, rule->ht, "in_interface");

exit:
    return in_interface;
}

int fw_rule_set_chain(fw_rule_t* const rule, const char* chain) {
    int retval = -1;

    when_null(rule, exit);
    when_null(chain, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_IN_INTERFACE,
               exit);

    when_false(strncpy(rule->params.chain, chain, sizeof(rule->params.chain) - 1)
               == rule->params.chain,
               exit);

    retval = 0;

exit:
    return retval;
}

int fw_rule_set_old_chain(fw_rule_t* const rule, const char* old_chain) {
    int retval = -1;

    when_null(rule, exit);
    when_null(old_chain, exit);
    //No feature checks.

    when_false(strncpy(rule->params.old_chain, old_chain, sizeof(rule->params.old_chain) - 1)
               == rule->params.old_chain,
               exit);

    retval = 0;

exit:
    return retval;
}

const char* fw_rule_get_chain(const fw_rule_t* const rule) {
    const char* chain = NULL;

    when_null(rule, exit);
    chain = rule->params.chain;

exit:
    return chain;
}

const char* fw_rule_get_old_chain(const fw_rule_t* const rule) {
    const char* old_chain = NULL;

    when_null(rule, exit);
    old_chain = rule->params.old_chain;

exit:
    return old_chain;
}

int fw_rule_set_destination(fw_rule_t* const rule, const char* destination) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_DESTINATION ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_SPOOFING ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_ISOLATION,
               exit);

    retval = fw_rule_ht_set(cstring_t, rule->ht, "destination", destination);
exit:
    return retval;
}

const char* fw_rule_get_destination(const fw_rule_t* const rule) {
    const char* destination = NULL;

    when_null(rule, exit);
    destination = fw_rule_ht_get(cstring_t, rule->ht, "destination");

exit:
    return destination;
}

int fw_rule_set_destination_mask(fw_rule_t* const rule, const char* destination_mask) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_DESTINATION ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_SPOOFING ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_ISOLATION,
               exit);

    retval = fw_rule_ht_set(cstring_t, rule->ht, "destination_mask", destination_mask);
exit:
    return retval;
}

const char* fw_rule_get_destination_mask(const fw_rule_t* const rule) {
    const char* destination_mask = NULL;

    when_null(rule, exit);
    destination_mask = fw_rule_ht_get(cstring_t, rule->ht, "destination_mask");

exit:
    return destination_mask;
}

int fw_rule_set_destination_ipset(fw_rule_t* const rule, const char* destination_set) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_DESTINATION_IPSET,
               exit);

    retval = fw_rule_ht_set(cstring_t, rule->ht, "destination_ipset", destination_set);
exit:
    return retval;
}

const char* fw_rule_get_destination_ipset(const fw_rule_t* const rule) {
    const char* destination_set = NULL;

    when_null(rule, exit);
    destination_set = fw_rule_ht_get(cstring_t, rule->ht, "destination_ipset");

exit:
    return destination_set;
}

int fw_rule_set_destination_port(fw_rule_t* const rule, const uint32_t destination_port) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_DESTINATION_PORT ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_SNAT || // SNAT for HairpinNAT
               fw_rule_get_current_feature(rule) == FW_FEATURE_DNAT,
               exit);

    retval = fw_rule_ht_set(uint32_t, rule->ht, "destination_port", destination_port);
exit:
    return retval;
}

uint32_t fw_rule_get_destination_port(const fw_rule_t* const rule) {
    uint32_t destination_port = 0;

    when_null(rule, exit);
    destination_port = fw_rule_ht_get(uint32_t, rule->ht, "destination_port");

exit:
    return destination_port;
}

int fw_rule_set_source_port(fw_rule_t* const rule, const uint32_t source_port) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_SOURCE_PORT ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_SNAT,
               exit);

    retval = fw_rule_ht_set(uint32_t, rule->ht, "source_port", source_port);
exit:
    return retval;
}

uint32_t fw_rule_get_source_port(const fw_rule_t* const rule) {
    uint32_t source_port = 0;

    when_null(rule, exit);
    source_port = fw_rule_ht_get(uint32_t, rule->ht, "source_port");

exit:
    return source_port;
}

int fw_rule_set_destination_port_range_max(fw_rule_t* const rule, const uint32_t destination_port_range_max) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_DESTINATION_PORT ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_DNAT,
               exit);

    retval = fw_rule_ht_set(uint32_t, rule->ht, "destination_port_range_max", destination_port_range_max);
exit:
    return retval;
}

uint32_t fw_rule_get_destination_port_range_max(const fw_rule_t* const rule) {
    uint32_t destination_port_range_max = 0;

    when_null(rule, exit);
    destination_port_range_max = fw_rule_ht_get(uint32_t, rule->ht, "destination_port_range_max");

exit:
    return destination_port_range_max;
}

int fw_rule_set_source_port_range_max(fw_rule_t* const rule, const uint32_t source_port_range_max) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_SOURCE_PORT ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_SNAT,
               exit);

    retval = fw_rule_ht_set(uint32_t, rule->ht, "source_port_range_max", source_port_range_max);
exit:
    return retval;
}

uint32_t fw_rule_get_source_port_range_max(const fw_rule_t* const rule) {
    uint32_t source_port_range_max = 0;

    when_null(rule, exit);
    source_port_range_max = fw_rule_ht_get(uint32_t, rule->ht, "source_port_range_max");

exit:
    return source_port_range_max;
}

int fw_rule_set_protocol(fw_rule_t* const rule, const uint32_t protocol) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_PROTOCOL,
               exit);

    retval = fw_rule_ht_set(uint32_t, rule->ht, "protocol", protocol);
exit:
    return retval;
}

uint32_t fw_rule_get_protocol(const fw_rule_t* const rule) {
    uint32_t protocol = 0;

    when_null(rule, exit);
    protocol = fw_rule_ht_get(uint32_t, rule->ht, "protocol");

exit:
    return protocol;
}

int fw_rule_set_source(fw_rule_t* const rule, const char* source) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_SOURCE ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_WHITELIST ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_SPOOFING,
               exit);

    retval = fw_rule_ht_set(cstring_t, rule->ht, "source", source);
exit:
    return retval;
}

const char* fw_rule_get_source(const fw_rule_t* const rule) {
    const char* source = NULL;

    when_null(rule, exit);
    source = fw_rule_ht_get(cstring_t, rule->ht, "source");

exit:
    return source;
}

int fw_rule_set_source_mask(fw_rule_t* const rule, const char* source_mask) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_SOURCE ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_WHITELIST ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_SPOOFING,
               exit);

    retval = fw_rule_ht_set(cstring_t, rule->ht, "source_mask", source_mask);
exit:
    return retval;
}

const char* fw_rule_get_source_mask(const fw_rule_t* const rule) {
    const char* source_mask = NULL;

    when_null(rule, exit);
    source_mask = fw_rule_ht_get(cstring_t, rule->ht, "source_mask");

exit:
    return source_mask;
}

int fw_rule_set_source_ipset(fw_rule_t* const rule, const char* source_set) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_SOURCE_IPSET,
               exit);

    retval = fw_rule_ht_set(cstring_t, rule->ht, "source_ipset", source_set);
exit:
    return retval;
}

const char* fw_rule_get_source_ipset(const fw_rule_t* const rule) {
    const char* source_set = NULL;

    when_null(rule, exit);
    source_set = fw_rule_ht_get(cstring_t, rule->ht, "source_ipset");

exit:
    return source_set;
}

int fw_rule_set_source_mac_address(fw_rule_t* const rule, const char* source_mac_address) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_SOURCE_MAC ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_SPOOFING ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_WHITELIST,
               exit);

    retval = fw_rule_ht_set(cstring_t, rule->ht, "source_mac_address", source_mac_address);
exit:
    return retval;
}

const char* fw_rule_get_source_mac_address(const fw_rule_t* const rule) {
    const char* source_mac_address = NULL;

    when_null(rule, exit);
    source_mac_address = fw_rule_ht_get(cstring_t, rule->ht, "source_mac_address");

exit:
    return source_mac_address;
}

int fw_rule_set_dscp(fw_rule_t* const rule, const uint32_t dscp) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_DSCP,
               exit);

    retval = fw_rule_ht_set(uint32_t, rule->ht, "dscp", dscp);
exit:
    return retval;
}

uint32_t fw_rule_get_dscp(const fw_rule_t* const rule) {
    uint32_t dscp = 0;

    when_null(rule, exit);
    dscp = fw_rule_ht_get(uint32_t, rule->ht, "dscp");

exit:
    return dscp;
}

int fw_rule_set_out_interface(fw_rule_t* const rule, const char* out_interface) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_WHITELIST ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_POLICY ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_SPOOFING ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_ISOLATION ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_OUT_INTERFACE,
               exit);

    retval = fw_rule_ht_set(cstring_t, rule->ht, "out_interface", out_interface);
exit:
    return retval;
}

const char* fw_rule_get_out_interface(const fw_rule_t* const rule) {
    const char* out_interface = NULL;

    when_null(rule, exit);
    out_interface = fw_rule_ht_get(cstring_t, rule->ht, "out_interface");

exit:
    return out_interface;
}

int fw_rule_set_in_interface_excluded(fw_rule_t* const rule, const bool is_in_interface_excluded) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_SPOOFING,
               exit);

    retval = fw_rule_ht_set(bool, rule->ht, "in_interface_excluded", is_in_interface_excluded);

exit:
    return retval;
}

int fw_rule_set_out_interface_excluded(fw_rule_t* const rule, const bool is_out_interface_excluded) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST,
               exit);

    retval = fw_rule_ht_set(bool, rule->ht, "out_interface_excluded", is_out_interface_excluded);

exit:
    return retval;
}

bool fw_rule_get_in_interface_excluded(const fw_rule_t* const rule) {
    bool in_interface_excluded = false;

    when_null(rule, exit);
    in_interface_excluded = fw_rule_ht_get(bool, rule->ht, "in_interface_excluded");

exit:
    return in_interface_excluded;
}

bool fw_rule_get_out_interface_excluded(const fw_rule_t* const rule) {
    bool out_interface_excluded = false;

    when_null(rule, exit);
    out_interface_excluded = fw_rule_ht_get(bool, rule->ht, "out_interface_excluded");

exit:
    return out_interface_excluded;
}

int fw_rule_set_source_excluded(fw_rule_t* const rule, const bool is_source_excluded) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_SOURCE ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_SPOOFING,
               exit);

    retval = fw_rule_ht_set(bool, rule->ht, "source_excluded", is_source_excluded);

exit:
    return retval;
}

int fw_rule_set_destination_excluded(fw_rule_t* const rule, const bool is_destination_excluded) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_DESTINATION,
               exit);

    retval = fw_rule_ht_set(bool, rule->ht, "destination_excluded", is_destination_excluded);

exit:
    return retval;
}

int fw_rule_set_source_ipset_excluded(fw_rule_t* const rule, const bool is_source_set_excluded) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_SOURCE_IPSET,
               exit);

    retval = fw_rule_ht_set(bool, rule->ht, "source_ipset_excluded", is_source_set_excluded);

exit:
    return retval;
}

int fw_rule_set_destination_ipset_excluded(fw_rule_t* const rule, const bool is_destination_set_excluded) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_DESTINATION_IPSET,
               exit);

    retval = fw_rule_ht_set(bool, rule->ht, "destination_ipset_excluded", is_destination_set_excluded);

exit:
    return retval;
}

int fw_rule_set_source_port_excluded(fw_rule_t* const rule, const bool is_source_port_excluded) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_SOURCE_PORT ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_SNAT,
               exit);

    retval = fw_rule_ht_set(bool, rule->ht, "source_port_excluded", is_source_port_excluded);

exit:
    return retval;
}

int fw_rule_set_destination_port_excluded(fw_rule_t* const rule, const bool is_destination_port_excluded) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_DESTINATION_PORT ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_DNAT,
               exit);

    retval = fw_rule_ht_set(bool, rule->ht, "destination_port_excluded", is_destination_port_excluded);

exit:
    return retval;
}

int fw_rule_set_protocol_excluded(fw_rule_t* const rule, const bool is_protocol_excluded) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST,
               exit);

    retval = fw_rule_ht_set(bool, rule->ht, "protocol_excluded", is_protocol_excluded);

exit:
    return retval;
}

int fw_rule_set_dscp_excluded(fw_rule_t* const rule, const bool is_dscp_excluded) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_DSCP,
               exit);

    retval = fw_rule_ht_set(bool, rule->ht, "dscp_excluded", is_dscp_excluded);

exit:
    return retval;
}

bool fw_rule_get_source_excluded(const fw_rule_t* const rule) {
    bool source_excluded = false;

    when_null(rule, exit);
    source_excluded = fw_rule_ht_get(bool, rule->ht, "source_excluded");

exit:
    return source_excluded;
}

bool fw_rule_get_destination_excluded(const fw_rule_t* const rule) {
    bool destination_excluded = false;

    when_null(rule, exit);
    destination_excluded = fw_rule_ht_get(bool, rule->ht, "destination_excluded");

exit:
    return destination_excluded;
}

bool fw_rule_get_source_ipset_excluded(const fw_rule_t* const rule) {
    bool source_set_excluded = false;

    when_null(rule, exit);
    source_set_excluded = fw_rule_ht_get(bool, rule->ht, "source_ipset_excluded");

exit:
    return source_set_excluded;
}

bool fw_rule_get_destination_ipset_excluded(const fw_rule_t* const rule) {
    bool destination_set_excluded = false;

    when_null(rule, exit);
    destination_set_excluded = fw_rule_ht_get(bool, rule->ht, "destination_ipset_excluded");

exit:
    return destination_set_excluded;
}

bool fw_rule_get_source_port_excluded(const fw_rule_t* const rule) {
    bool source_port_excluded = false;

    when_null(rule, exit);
    source_port_excluded = fw_rule_ht_get(bool, rule->ht, "source_port_excluded");

exit:
    return source_port_excluded;
}

bool fw_rule_get_destination_port_excluded(const fw_rule_t* const rule) {
    bool destination_port_excluded = false;

    when_null(rule, exit);
    destination_port_excluded = fw_rule_ht_get(bool, rule->ht, "destination_port_excluded");

exit:
    return destination_port_excluded;
}

bool fw_rule_get_protocol_excluded(const fw_rule_t* const rule) {
    bool protocol_excluded = false;

    when_null(rule, exit);
    protocol_excluded = fw_rule_ht_get(bool, rule->ht, "protocol_excluded");

exit:
    return protocol_excluded;
}

bool fw_rule_get_dscp_excluded(const fw_rule_t* const rule) {
    bool dscp_excluded = false;

    when_null(rule, exit);
    dscp_excluded = fw_rule_ht_get(bool, rule->ht, "dscp_excluded");

exit:
    return dscp_excluded;
}

int fw_rule_set_source_mac_excluded(fw_rule_t* const rule, const bool is_source_mac_excluded) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_SOURCE_MAC ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_SPOOFING,
               exit);

    retval = fw_rule_ht_set(bool, rule->ht, "source_mac_excluded", is_source_mac_excluded);

exit:
    return retval;
}

int fw_rule_set_destination_mac_excluded(fw_rule_t* const rule, const bool is_destination_mac_excluded) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST,
               exit);

    retval = fw_rule_ht_set(bool, rule->ht, "destination_mac_excluded", is_destination_mac_excluded);

exit:
    return retval;
}

bool fw_rule_get_source_mac_excluded(const fw_rule_t* const rule) {
    bool source_mac_excluded = false;

    when_null(rule, exit);
    source_mac_excluded = fw_rule_ht_get(bool, rule->ht, "source_mac_excluded");

exit:
    return source_mac_excluded;
}

bool fw_rule_get_destination_mac_excluded(const fw_rule_t* const rule) {
    bool destination_mac_excluded = false;

    when_null(rule, exit);
    destination_mac_excluded = fw_rule_ht_get(bool, rule->ht, "destination_mac_excluded");

exit:
    return destination_mac_excluded;
}

int fw_rule_set_ip_min_length(fw_rule_t* const rule, const uint32_t ip_min_length) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST,
               exit);

    retval = fw_rule_ht_set(uint32_t, rule->ht, "ip_min_length", ip_min_length);
exit:
    return retval;
}

uint32_t fw_rule_get_ip_min_length(const fw_rule_t* const rule) {
    uint32_t ip_min_length = 0;

    when_null(rule, exit);
    ip_min_length = fw_rule_ht_get(uint32_t, rule->ht, "ip_min_length");

exit:
    return ip_min_length;
}

int fw_rule_set_ip_max_length(fw_rule_t* const rule, const uint32_t ip_max_length) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST,
               exit);

    retval = fw_rule_ht_set(uint32_t, rule->ht, "ip_max_length", ip_max_length);
exit:
    return retval;
}

uint32_t fw_rule_get_ip_max_length(const fw_rule_t* const rule) {
    uint32_t ip_max_length = 0;

    when_null(rule, exit);
    ip_max_length = fw_rule_ht_get(uint32_t, rule->ht, "ip_max_length");

exit:
    return ip_max_length;
}

int fw_rule_set_icmp_type(fw_rule_t* const rule, const int32_t icmp_type) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_ipv4(rule), exit);
    when_true(icmp_type < 0 || icmp_type > 255, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST,
               exit);

    retval = fw_rule_ht_set(int32_t, rule->ht, "icmp_type", icmp_type);

exit:
    return retval;
}

int fw_rule_set_icmpv6_type(fw_rule_t* const rule, const int32_t icmpv6_type) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_ipv6(rule), exit);
    when_true(icmpv6_type < 0 || icmpv6_type > 255, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST,
               exit);

    retval = fw_rule_ht_set(int32_t, rule->ht, "icmpv6_type", icmpv6_type);

exit:
    return retval;
}

int fw_rule_set_icmp_code(fw_rule_t* const rule, const int32_t icmp_code) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST,
               exit);

    retval = fw_rule_ht_set(int32_t, rule->ht, "icmp_code", icmp_code);

exit:
    return retval;
}

int fw_rule_set_icmpv6_code(fw_rule_t* const rule, const int32_t icmpv6_code) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST,
               exit);

    retval = fw_rule_ht_set(int32_t, rule->ht, "icmpv6_code", icmpv6_code);

exit:
    return retval;
}

int fw_rule_set_filtered_icmp_code(fw_rule_t* const rule, const int32_t icmp_code) {
    int retval = -1;
    int32_t type = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_ipv4(rule), exit);
    type = fw_rule_get_icmp_type(rule);
    when_true(type == -1, exit);
    for(int i = 0; -1 != icmp_code_type[i].type; i++) {
        if((icmp_code_type[i].type == type) && (icmp_code_type[i].code == icmp_code)) {
            retval = fw_rule_set_icmp_code(rule, icmp_code);
            break;
        }
    }

exit:
    return retval;
}

int fw_rule_set_filtered_icmpv6_code(fw_rule_t* const rule, const int32_t icmpv6_code) {
    int retval = -1;
    int32_t type = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_ipv6(rule), exit);
    type = fw_rule_get_icmpv6_type(rule);
    when_true(type == -1, exit);
    for(int i = 0; -1 != icmpv6_code_type[i].type; i++) {
        if((icmpv6_code_type[i].type == type) && (icmpv6_code_type[i].code == icmpv6_code)) {
            retval = fw_rule_set_icmpv6_code(rule, icmpv6_code);
            break;
        }
    }

exit:
    return retval;
}

int32_t fw_rule_get_icmp_type(const fw_rule_t* const rule) {
    int32_t icmp_type = -1;

    when_null(rule, exit);
    icmp_type = fw_rule_ht_get(int32_t, rule->ht, "icmp_type");

exit:
    return icmp_type;
}

int32_t fw_rule_get_icmpv6_type(const fw_rule_t* const rule) {
    int32_t icmpv6_type = -1;

    when_null(rule, exit);
    icmpv6_type = fw_rule_ht_get(int32_t, rule->ht, "icmpv6_type");

exit:
    return icmpv6_type;
}

int32_t fw_rule_get_icmp_code(const fw_rule_t* const rule) {
    int32_t icmp_code = -1;

    when_null(rule, exit);
    icmp_code = fw_rule_ht_get(int32_t, rule->ht, "icmp_code");

exit:
    return icmp_code;
}

int32_t fw_rule_get_icmpv6_code(const fw_rule_t* const rule) {
    int32_t icmpv6_code = -1;

    when_null(rule, exit);
    icmpv6_code = fw_rule_ht_get(int32_t, rule->ht, "icmpv6_code");

exit:
    return icmpv6_code;
}

int fw_rule_set_conntrack_state(fw_rule_t* const rule, const char* state) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST,
               exit);

    retval = fw_rule_ht_set(cstring_t, rule->ht, "conntrack_state", state);
exit:
    return retval;
}

const char* fw_rule_get_conntrack_state(const fw_rule_t* const rule) {
    const char* state = NULL;

    when_null(rule, exit);
    state = fw_rule_ht_get(cstring_t, rule->ht, "conntrack_state");

exit:
    return state;
}

int fw_rule_set_connbytes_min(fw_rule_t* const rule, const uint64_t min) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST,
               exit);

    retval = fw_rule_ht_set(uint64_t, rule->ht, "connbytes_min", min);

exit:
    return retval;
}

int fw_rule_set_connbytes_max(fw_rule_t* const rule, const uint64_t max) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST,
               exit);

    retval = fw_rule_ht_set(uint64_t, rule->ht, "connbytes_max", max);

exit:
    return retval;
}

int fw_rule_set_connbytes_param(fw_rule_t* const rule, const char* param) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST,
               exit);

    retval = fw_rule_ht_set(cstring_t, rule->ht, "connbytes_param", param);
exit:
    return retval;
}

int fw_rule_set_connbytes_direction(fw_rule_t* const rule, const char* param) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST,
               exit);

    retval = fw_rule_ht_set(cstring_t, rule->ht, "connbytes_direction", param);
exit:
    return retval;
}

uint64_t fw_rule_get_connbytes_min(const fw_rule_t* const rule) {
    uint64_t min = 0;

    when_null(rule, exit);
    min = fw_rule_ht_get(uint64_t, rule->ht, "connbytes_min");

exit:
    return min;
}

uint64_t fw_rule_get_connbytes_max(const fw_rule_t* const rule) {
    uint64_t max = 0;

    when_null(rule, exit);
    max = fw_rule_ht_get(uint64_t, rule->ht, "connbytes_max");

exit:
    return max;
}

const char* fw_rule_get_connbytes_param(const fw_rule_t* const rule) {
    const char* param = NULL;

    when_null(rule, exit);
    param = fw_rule_ht_get(cstring_t, rule->ht, "connbytes_param");

exit:
    return param;
}

const char* fw_rule_get_connbytes_direction(const fw_rule_t* const rule) {
    const char* direction = NULL;

    when_null(rule, exit);
    direction = fw_rule_ht_get(cstring_t, rule->ht, "connbytes_direction");

exit:
    return direction;
}

int fw_rule_set_skb_mark(fw_rule_t* const rule, const uint32_t mark) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST,
               exit);

    retval = fw_rule_ht_set(uint32_t, rule->ht, "skb_mark", mark);

exit:
    return retval;
}

int fw_rule_set_skb_mark_mask(fw_rule_t* const rule, const uint32_t mask) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST,
               exit);

    retval = fw_rule_ht_set(uint32_t, rule->ht, "skb_mark_mask", mask);

exit:
    return retval;
}

int fw_rule_set_skb_mark_excluded(fw_rule_t* const rule, const bool is_skb_mark_excluded) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST,
               exit);

    retval = fw_rule_ht_set(bool, rule->ht, "skb_mark_excluded", is_skb_mark_excluded);

exit:
    return retval;
}

int fw_rule_set_conn_mark(fw_rule_t* const rule, const uint32_t mark) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST,
               exit);

    retval = fw_rule_ht_set(uint32_t, rule->ht, "conn_mark", mark);

exit:
    return retval;
}

int fw_rule_set_conn_mark_mask(fw_rule_t* const rule, const uint32_t mask) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST,
               exit);

    retval = fw_rule_ht_set(uint32_t, rule->ht, "conn_mark_mask", mask);

exit:
    return retval;
}

int fw_rule_set_conn_mark_excluded(fw_rule_t* const rule, const bool is_conn_mark_excluded) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST,
               exit);

    retval = fw_rule_ht_set(bool, rule->ht, "conn_mark_excluded", is_conn_mark_excluded);

exit:
    return retval;
}

uint32_t fw_rule_get_skb_mark(const fw_rule_t* const rule) {
    uint32_t mark = 0;

    when_null(rule, exit);
    mark = fw_rule_ht_get(uint32_t, rule->ht, "skb_mark");

exit:
    return mark;
}

uint32_t fw_rule_get_skb_mark_mask(const fw_rule_t* const rule) {
    uint32_t mask = 0;

    when_null(rule, exit);
    mask = fw_rule_ht_get(uint32_t, rule->ht, "skb_mark_mask");

exit:
    return mask;
}

bool fw_rule_get_skb_mark_excluded(const fw_rule_t* const rule) {
    bool skb_mark_excluded = false;

    when_null(rule, exit);
    skb_mark_excluded = fw_rule_ht_get(bool, rule->ht, "skb_mark_excluded");

exit:
    return skb_mark_excluded;
}

uint32_t fw_rule_get_conn_mark(const fw_rule_t* const rule) {
    uint32_t mark = 0;

    when_null(rule, exit);
    mark = fw_rule_ht_get(uint32_t, rule->ht, "conn_mark");

exit:
    return mark;
}

uint32_t fw_rule_get_conn_mark_mask(const fw_rule_t* const rule) {
    uint32_t mask = 0;

    when_null(rule, exit);
    mask = fw_rule_ht_get(uint32_t, rule->ht, "conn_mark_mask");

exit:
    return mask;
}

bool fw_rule_get_conn_mark_excluded(const fw_rule_t* const rule) {
    bool conn_mark_excluded = false;

    when_null(rule, exit);
    conn_mark_excluded = fw_rule_ht_get(bool, rule->ht, "conn_mark_excluded");

exit:
    return conn_mark_excluded;
}

static bool fw_rule_is_feature_valid_for_target(fw_rule_t* rule) {
    bool retval = false;
    fw_feature_t feature = FW_FEATURE_LAST;

    when_null(rule, exit);

    feature = fw_rule_get_current_feature(rule);

    switch(feature) {
    case FW_FEATURE_TARGET:
    case FW_FEATURE_DNAT:
    case FW_FEATURE_SNAT:
    case FW_FEATURE_SPOOFING:
    case FW_FEATURE_ISOLATION:
    case FW_FEATURE_POLICY:
    case FW_FEATURE_DESTINATION:
    case FW_FEATURE_LAST:
        retval = true;
    default:
        break;
    }

exit:
    return retval;
}

int fw_rule_set_target_return(fw_rule_t* rule) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_is_feature_valid_for_target(rule), exit);
    when_true(fw_rule_clear_target(rule), exit);

    retval = fw_rule_ht_set(uint32_t, rule->ht, "target", (uint32_t) FW_RULE_TARGET_RETURN);

exit:
    return retval;
}

int fw_rule_set_target_mark(fw_rule_t* rule, const uint32_t mark, const uint32_t mask) {
    int retval = -1;
    amxc_var_t* ht_mark = NULL;

    when_null(rule, exit);
    when_false(fw_rule_is_feature_valid_for_target(rule), exit);
    when_true(fw_rule_clear_target(rule), exit);

    ht_mark = amxc_var_add_key(amxc_htable_t, rule->ht, "target_option", NULL);
    when_null(ht_mark, error);

    when_null(amxc_var_add_key(uint32_t, ht_mark, "mark", mark), error);
    when_null(amxc_var_add_key(uint32_t, ht_mark, "mask", mask), error);

    retval = fw_rule_ht_set(uint32_t, rule->ht, "target", (uint32_t) FW_RULE_TARGET_MARK);
    when_failed(retval, error);

exit:
    return retval;

error:
    fw_rule_clear_target(rule);
    amxc_var_delete(&ht_mark);
    return retval;
}

int fw_rule_set_target_classify(fw_rule_t* rule, const uint32_t class) {
    int retval = -1;
    amxc_var_t* ht_class = NULL;

    when_null(rule, exit);
    when_false(fw_rule_is_feature_valid_for_target(rule), exit);
    when_true(fw_rule_clear_target(rule), exit);

    ht_class = amxc_var_add_key(amxc_htable_t, rule->ht, "target_option", NULL);
    when_null(ht_class, error);

    when_null(amxc_var_add_key(uint32_t, ht_class, "class", class), error);

    retval = fw_rule_ht_set(uint32_t, rule->ht, "target", (uint32_t) FW_RULE_TARGET_CLASSIFY);
    when_failed(retval, error);

exit:
    return retval;

error:
    fw_rule_clear_target(rule);
    amxc_var_delete(&ht_class);
    return retval;
}

int fw_rule_set_target_dscp(fw_rule_t* rule, const uint32_t dscp) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_is_feature_valid_for_target(rule), exit);
    when_true(fw_rule_clear_target(rule), exit);

    retval = 0;

    retval |= fw_rule_ht_set(uint32_t, rule->ht, "target", (uint32_t) FW_RULE_TARGET_DSCP);
    retval |= fw_rule_ht_set(uint32_t, rule->ht, "target_option", dscp);

    if(retval) {
        fw_rule_clear_target(rule);
    }

exit:
    return retval;
}

int fw_rule_set_target_queue(fw_rule_t* rule, const uint32_t queue) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_is_feature_valid_for_target(rule), exit);
    when_true(fw_rule_clear_target(rule), exit);

    retval = 0;

    retval |= fw_rule_ht_set(uint32_t, rule->ht, "target", (uint32_t) FW_RULE_TARGET_QUEUE);
    retval |= fw_rule_ht_set(uint32_t, rule->ht, "target_option", queue);

    if(retval) {
        fw_rule_clear_target(rule);
    }

exit:
    return retval;
}

int fw_rule_set_target_pbit(fw_rule_t* rule, const uint32_t pbit) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_is_feature_valid_for_target(rule), exit);
    when_true(fw_rule_clear_target(rule), exit);

    retval = 0;

    retval |= fw_rule_ht_set(uint32_t, rule->ht, "target", (uint32_t) FW_RULE_TARGET_PBIT);
    retval |= fw_rule_ht_set(uint32_t, rule->ht, "target_option", pbit);

    if(retval) {
        fw_rule_clear_target(rule);
    }

exit:
    return retval;
}

int fw_rule_set_target_policy(fw_rule_t* rule, const fw_rule_policy_t policy) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_is_feature_valid_for_target(rule), exit);
    when_true(policy >= FW_RULE_POLICY_LAST, exit);
    when_true(fw_rule_clear_target(rule), exit);

    retval = 0;

    retval |= fw_rule_ht_set(uint32_t, rule->ht, "target", (uint32_t) FW_RULE_TARGET_POLICY);
    retval |= fw_rule_ht_set(uint32_t, rule->ht, "target_option", (uint32_t) policy);

    if(retval) {
        fw_rule_clear_target(rule);
    }

exit:
    return retval;
}

static int fw_rule_set_target_nat(fw_rule_t* rule, const char* ip, const uint32_t min_port,
                                  const uint32_t max_port, fw_rule_target_t target) {

    int retval = -1;
    amxc_var_t* ht_nat = NULL;

    when_null(rule, exit);
    when_false(fw_rule_is_feature_valid_for_target(rule), exit);
    when_null(ip, exit);
    when_true(fw_rule_clear_target(rule), exit);

    ht_nat = amxc_var_add_key(amxc_htable_t, rule->ht, "target_option", NULL);
    when_null(ht_nat, error);

    when_null(amxc_var_add_key(cstring_t, ht_nat, "ip", ip), error);
    when_null(amxc_var_add_key(uint32_t, ht_nat, "min_port", min_port), error);
    when_null(amxc_var_add_key(uint32_t, ht_nat, "max_port", max_port), error);

    retval = fw_rule_ht_set(uint32_t, rule->ht, "target", (uint32_t) target);
    when_failed(retval, error);

exit:
    return retval;
error:
    fw_rule_clear_target(rule);
    amxc_var_delete(&ht_nat);
    return retval;
}

int fw_rule_set_target_dnat(fw_rule_t* rule, const char* ip, const uint32_t min_port, const uint32_t max_port) {
    return fw_rule_set_target_nat(rule, ip, min_port, max_port, FW_RULE_TARGET_DNAT);
}

int fw_rule_set_target_snat(fw_rule_t* rule, const char* ip, const uint32_t min_port, const uint32_t max_port) {
    return fw_rule_set_target_nat(rule, ip, min_port, max_port, FW_RULE_TARGET_SNAT);
}


fw_rule_target_t fw_rule_get_target(const fw_rule_t* const rule) {
    fw_rule_target_t target = FW_RULE_TARGET_LAST;

    when_null(rule, exit);
    target = (fw_rule_target_t) fw_rule_ht_get(uint32_t, rule->ht, "target");

exit:
    return target;
}

int fw_rule_get_target_mark_options(const fw_rule_t* const rule, uint32_t* mark, uint32_t* mask) {
    int retval = -1;
    amxc_var_t* ht_mark;

    when_null(rule, exit);
    when_null(mark, exit);
    when_null(mask, exit);
    when_true(fw_rule_get_target(rule) != FW_RULE_TARGET_MARK, exit);

    ht_mark = amxc_var_get_key(rule->ht, "target_option", AMXC_VAR_FLAG_DEFAULT);
    when_null(ht_mark, exit);

    *mark = fw_rule_ht_get(uint32_t, ht_mark, "mark");
    *mask = fw_rule_ht_get(uint32_t, ht_mark, "mask");

    retval = 0;

exit:
    return retval;
}

int fw_rule_get_target_classify_option(const fw_rule_t* const rule, uint32_t* class) {
    int retval = -1;
    amxc_var_t* ht_class = NULL;

    when_null(rule, exit);
    when_null(class, exit);
    when_true(fw_rule_get_target(rule) != FW_RULE_TARGET_CLASSIFY, exit);

    ht_class = amxc_var_get_key(rule->ht, "target_option", AMXC_VAR_FLAG_DEFAULT);
    when_null(ht_class, exit);

    *class = fw_rule_ht_get(uint32_t, ht_class, "class");
    retval = 0;

exit:
    return retval;
}

uint32_t fw_rule_get_target_dscp_option(const fw_rule_t* const rule) {
    uint32_t dscp = 0;

    when_true(fw_rule_get_target(rule) != FW_RULE_TARGET_DSCP, exit);
    dscp = fw_rule_ht_get(uint32_t, rule->ht, "target_option");

exit:
    return dscp;
}

uint32_t fw_rule_get_target_queue_option(const fw_rule_t* const rule) {
    uint32_t queue = 0;

    when_true(fw_rule_get_target(rule) != FW_RULE_TARGET_QUEUE, exit);
    queue = fw_rule_ht_get(uint32_t, rule->ht, "target_option");

exit:
    return queue;
}

uint32_t fw_rule_get_target_pbit_option(const fw_rule_t* const rule) {
    uint32_t pbit = 0;

    when_true(fw_rule_get_target(rule) != FW_RULE_TARGET_PBIT, exit);
    pbit = fw_rule_ht_get(uint32_t, rule->ht, "target_option");

exit:
    return pbit;
}

fw_rule_policy_t fw_rule_get_target_policy_option(const fw_rule_t* const rule) {
    fw_rule_policy_t policy = FW_RULE_POLICY_LAST;

    when_true(fw_rule_get_target(rule) != FW_RULE_TARGET_POLICY, exit);
    policy = (fw_rule_policy_t) fw_rule_ht_get(uint32_t, rule->ht, "target_option");

exit:
    return policy;
}

static int fw_rule_get_target_nat_options(const fw_rule_t* const rule, const char** ip, uint32_t* min_port, uint32_t* max_port, fw_rule_target_t target) {
    int retval = -1;
    amxc_var_t* ht_nat = NULL;

    when_null(rule, exit);
    when_null(ip, exit);
    when_not_null(*ip, exit);
    when_null(min_port, exit);
    when_null(max_port, exit);
    when_true(fw_rule_get_target(rule) != target, exit);

    ht_nat = amxc_var_get_key(rule->ht, "target_option", AMXC_VAR_FLAG_DEFAULT);
    when_null(ht_nat, exit);

    *ip = fw_rule_ht_get(cstring_t, ht_nat, "ip");
    *min_port = fw_rule_ht_get(uint32_t, ht_nat, "min_port");
    *max_port = fw_rule_ht_get(uint32_t, ht_nat, "max_port");

    retval = 0;

exit:
    return retval;
}

int fw_rule_get_target_dnat_options(const fw_rule_t* const rule, const char** ip, uint32_t* min_port, uint32_t* max_port) {
    return fw_rule_get_target_nat_options(rule, ip, min_port, max_port, FW_RULE_TARGET_DNAT);
}

int fw_rule_get_target_snat_options(const fw_rule_t* const rule, const char** ip, uint32_t* min_port, uint32_t* max_port) {
    return fw_rule_get_target_nat_options(rule, ip, min_port, max_port, FW_RULE_TARGET_SNAT);
}

int fw_rule_set_target_chain(fw_rule_t* const rule, const char* chain) {
    int retval = -1;

    when_null(rule, exit);
    when_null(chain, exit);
    when_false(fw_rule_is_feature_valid_for_target(rule), exit);
    when_true(fw_rule_clear_target(rule), exit);

    retval = 0;

    retval |= fw_rule_ht_set(uint32_t, rule->ht, "target", (uint32_t) FW_RULE_TARGET_CHAIN);
    retval |= fw_rule_ht_set(cstring_t, rule->ht, "target_option", chain);

    if(retval) {
        fw_rule_clear_target(rule);
    }

exit:
    return retval;
}

const char* fw_rule_get_target_chain_option(const fw_rule_t* const rule) {
    const char* chain = NULL;

    when_true(fw_rule_get_target(rule) != FW_RULE_TARGET_CHAIN, exit);
    chain = fw_rule_ht_get(cstring_t, rule->ht, "target_option");

exit:
    return chain;
}

int fw_rule_set_target_nfqueue_options(fw_rule_t* const rule, const uint32_t qnum, const uint32_t qtotal, const uint32_t flags) {
    int retval = -1;
    amxc_var_t* ht = NULL;

    when_null(rule, exit);
    when_false(fw_rule_is_feature_valid_for_target(rule), exit);
    when_true(fw_rule_clear_target(rule), exit);

    ht = amxc_var_add_key(amxc_htable_t, rule->ht, "target_option", NULL);
    when_null(ht, error);

    when_null(amxc_var_add_key(uint32_t, ht, "qnum", qnum), error);
    when_null(amxc_var_add_key(uint32_t, ht, "qtotal", qtotal), error);
    when_null(amxc_var_add_key(uint32_t, ht, "flags", flags), error);

    retval = fw_rule_ht_set(uint32_t, rule->ht, "target", (uint32_t) FW_RULE_TARGET_NFQUEUE);
    when_failed(retval, error);

exit:
    return retval;
error:
    fw_rule_clear_target(rule);
    amxc_var_delete(&ht);
    return retval;
}

int fw_rule_get_target_nfqueue_options(const fw_rule_t* const rule, uint32_t* qnum, uint32_t* qtotal, uint32_t* flags) {
    int retval = -1;
    amxc_var_t* ht = NULL;

    when_null(rule, exit);
    when_null(qnum, exit);
    when_null(qtotal, exit);
    when_null(flags, exit);
    when_true(fw_rule_get_target(rule) != FW_RULE_TARGET_NFQUEUE, exit);

    ht = amxc_var_get_key(rule->ht, "target_option", AMXC_VAR_FLAG_DEFAULT);
    when_null(ht, exit);

    *qnum = fw_rule_ht_get(uint32_t, ht, "qnum");
    *qtotal = fw_rule_ht_get(uint32_t, ht, "qtotal");
    *flags = fw_rule_ht_get(uint32_t, ht, "flags");

    retval = 0;

exit:
    return retval;
}

int fw_rule_set_target_nflog_options(fw_rule_t* const rule, const uint32_t gnum, const uint32_t len, const uint32_t threshold, const uint32_t flags, const char* prefix) {
    int retval = -1;
    amxc_var_t* ht = NULL;

    when_null(rule, exit);
    when_false(fw_rule_is_feature_valid_for_target(rule), exit);
    when_true(fw_rule_clear_target(rule), exit);

    ht = amxc_var_add_key(amxc_htable_t, rule->ht, "target_option", NULL);
    when_null(ht, error);

    when_null(amxc_var_add_key(uint32_t, ht, "gnum", gnum), error);
    when_null(amxc_var_add_key(uint32_t, ht, "len", len), error);
    when_null(amxc_var_add_key(uint32_t, ht, "threshold", threshold), error);
    when_null(amxc_var_add_key(uint32_t, ht, "flags", flags), error);
    when_null(amxc_var_add_key(cstring_t, ht, "prefix", prefix), error);

    retval = fw_rule_ht_set(uint32_t, rule->ht, "target", (uint32_t) FW_RULE_TARGET_NFLOG);
    when_failed(retval, error);

exit:
    return retval;
error:
    fw_rule_clear_target(rule);
    amxc_var_delete(&ht);
    return retval;
}

int fw_rule_get_target_nflog_options(const fw_rule_t* const rule, uint32_t* gnum, uint32_t* len, uint32_t* threshold, uint32_t* flags, const char** prefix) {
    int retval = -1;
    amxc_var_t* ht = NULL;

    when_null(rule, exit);
    when_null(gnum, exit);
    when_null(len, exit);
    when_null(threshold, exit);
    when_null(flags, exit);
    when_null(prefix, exit);
    when_true(fw_rule_get_target(rule) != FW_RULE_TARGET_NFLOG, exit);

    ht = amxc_var_get_key(rule->ht, "target_option", AMXC_VAR_FLAG_DEFAULT);
    when_null(ht, exit);

    *gnum = fw_rule_ht_get(uint32_t, ht, "gnum");
    *len = fw_rule_ht_get(uint32_t, ht, "len");
    *threshold = fw_rule_ht_get(uint32_t, ht, "threshold");
    *flags = fw_rule_ht_get(uint32_t, ht, "flags");
    *prefix = fw_rule_ht_get(cstring_t, ht, "prefix");

    retval = 0;

exit:
    return retval;
}

int fw_rule_set_target_log_options(fw_rule_t* const rule, uint8_t level, uint8_t logflags, const char* prefix) {
    int retval = -1;
    amxc_var_t* ht = NULL;

    when_null(rule, exit);
    when_null(prefix, exit);
    when_false(fw_rule_is_feature_valid_for_target(rule), exit);
    when_true(fw_rule_clear_target(rule), exit);

    ht = amxc_var_add_key(amxc_htable_t, rule->ht, "target_option", NULL);
    when_null(ht, error);

    when_null(amxc_var_add_key(uint8_t, ht, "level", level), error);
    when_null(amxc_var_add_key(uint8_t, ht, "logflags", logflags), error);
    when_null(amxc_var_add_key(cstring_t, ht, "prefix", prefix), error);

    retval = fw_rule_ht_set(uint32_t, rule->ht, "target", (uint32_t) FW_RULE_TARGET_LOG);
    when_failed(retval, error);

exit:
    return retval;
error:
    fw_rule_clear_target(rule);
    amxc_var_delete(&ht);
    return retval;
}

int fw_rule_get_target_log_options(const fw_rule_t* const rule, uint8_t* level, uint8_t* logflags, const char** prefix) {
    int retval = -1;
    amxc_var_t* ht = NULL;

    when_null(rule, exit);
    when_null(level, exit);
    when_null(logflags, exit);
    when_null(prefix, exit);
    when_true(fw_rule_get_target(rule) != FW_RULE_TARGET_LOG, exit);

    ht = amxc_var_get_key(rule->ht, "target_option", AMXC_VAR_FLAG_DEFAULT);
    when_null(ht, exit);

    *level = fw_rule_ht_get(uint8_t, ht, "level");
    *logflags = fw_rule_ht_get(uint8_t, ht, "logflags");
    *prefix = fw_rule_ht_get(cstring_t, ht, "prefix");

    retval = 0;

exit:
    return retval;
}

static int fw_rule_sync_target(fw_rule_t* const dest, const fw_rule_t* const src) {
    int retval = -1;
    uint32_t type = 0;
    amxc_var_t* target = NULL;
    amxc_var_t* target_option = NULL;

    when_null(dest, exit);
    when_null(src, exit);
    when_true(fw_rule_clear_target(dest), exit);

    target = amxc_var_get_key(src->ht, "target", AMXC_VAR_FLAG_DEFAULT);

    if(target) {
        when_null(amxc_var_add_key(uint32_t, dest->ht, "target",
                                   amxc_var_constcast(uint32_t, target)), error);

        target_option = amxc_var_get_key(src->ht, "target_option", AMXC_VAR_FLAG_DEFAULT);

        if(target_option) {
            type = amxc_var_type_of(target_option);

            if(AMXC_VAR_ID_HTABLE == type) {
                when_null(amxc_var_add_key(amxc_htable_t, dest->ht, "target_option",
                                           amxc_var_constcast(amxc_htable_t, target_option)), error);
            } else if(AMXC_VAR_ID_UINT32 == type) {
                when_null(amxc_var_add_key(uint32_t, dest->ht, "target_option",
                                           amxc_var_constcast(uint32_t, target_option)), error);
            } else if(AMXC_VAR_ID_CSTRING == type) {
                when_null(amxc_var_add_key(cstring_t, dest->ht, "target_option",
                                           amxc_var_constcast(cstring_t, target_option)), error);
            } else {
                goto error;
            }
        }
    }

    retval = 0;

exit:
    return retval;
error:
    fw_rule_clear_target(dest);
    return retval;
}

int fw_rule_sync(fw_rule_t* const dest, const fw_rule_t* const src) {
    int retval = -1;
    fw_feature_t feature_src_rule = FW_FEATURE_LAST;

    when_null(dest, exit);
    when_null(src, exit);
    when_false(fw_rule_get_current_feature(dest) == FW_FEATURE_LAST, exit);

    feature_src_rule = fw_rule_get_current_feature(src);

    fw_rule_set_current_feature(dest, feature_src_rule);

    retval = 0;

    switch(feature_src_rule) {
    case FW_FEATURE_IN_INTERFACE:
        retval |= fw_rule_sync_cstring_t(dest, src, in_interface);
        retval |= fw_rule_sync_string(dest, src, chain);
        break;
    case FW_FEATURE_DESTINATION:
        retval |= fw_rule_sync_cstring_t(dest, src, destination);
        retval |= fw_rule_sync_cstring_t(dest, src, destination_mask);
        break;
    case FW_FEATURE_DESTINATION_PORT:
        retval |= fw_rule_sync_uint32_t(dest, src, destination_port);
        retval |= fw_rule_sync_uint32_t(dest, src, destination_port_range_max);
        break;
    case FW_FEATURE_PROTOCOL:
        retval |= fw_rule_sync_uint32_t(dest, src, protocol);
        break;
    case FW_FEATURE_SOURCE:
        retval |= fw_rule_sync_cstring_t(dest, src, source);
        retval |= fw_rule_sync_cstring_t(dest, src, source_mask);
        break;
    case FW_FEATURE_SOURCE_MAC:
        retval |= fw_rule_sync_cstring_t(dest, src, source_mac_address);
        retval |= fw_rule_sync_bool(dest, src, source_mac_excluded);
        break;
    case FW_FEATURE_WHITELIST:
        retval |= fw_rule_sync_cstring_t(dest, src, source);
        retval |= fw_rule_sync_cstring_t(dest, src, source_mask);
        retval |= fw_rule_sync_cstring_t(dest, src, source_mac_address);
        retval |= fw_rule_sync_cstring_t(dest, src, in_interface);
        break;
    case FW_FEATURE_SOURCE_PORT:
        retval |= fw_rule_sync_uint32_t(dest, src, source_port);
        retval |= fw_rule_sync_uint32_t(dest, src, source_port_range_max);
        break;
    case FW_FEATURE_TARGET:
        retval |= fw_rule_sync_target(dest, src);
        break;
    case FW_FEATURE_DSCP:
        retval |= fw_rule_sync_uint32_t(dest, src, dscp);
        break;
    case FW_FEATURE_IPVERSION:
        retval |= fw_rule_sync_bool(dest, src, ipv6);
        break;
    case FW_FEATURE_TABLE:
        retval |= fw_rule_sync_string(dest, src, table);
        break;
    case FW_FEATURE_DNAT:
        retval |= fw_rule_sync_uint32_t(dest, src, destination_port);
        retval |= fw_rule_sync_uint32_t(dest, src, destination_port_range_max);
        retval |= fw_rule_sync_target(dest, src);
        break;
    case FW_FEATURE_SNAT:
        retval |= fw_rule_sync_uint32_t(dest, src, source_port);
        retval |= fw_rule_sync_uint32_t(dest, src, source_port_range_max);
        retval |= fw_rule_sync_target(dest, src);
        break;
    case FW_FEATURE_POLICY:
        retval |= fw_rule_sync_cstring_t(dest, src, in_interface);
        retval |= fw_rule_sync_cstring_t(dest, src, out_interface);
        retval |= fw_rule_sync_target(dest, src);
        break;
    case FW_FEATURE_SPOOFING:
        retval |= fw_rule_sync_cstring_t(dest, src, in_interface);
        retval |= fw_rule_sync_bool(dest, src, in_interface_excluded);
        retval |= fw_rule_sync_cstring_t(dest, src, source);
        retval |= fw_rule_sync_cstring_t(dest, src, source_mask);
        retval |= fw_rule_sync_bool(dest, src, source_excluded);
        retval |= fw_rule_sync_cstring_t(dest, src, destination);
        retval |= fw_rule_sync_cstring_t(dest, src, destination_mask);
        retval |= fw_rule_sync_target(dest, src);
        break;
    case FW_FEATURE_ISOLATION:
        retval |= fw_rule_sync_cstring_t(dest, src, in_interface);
        retval |= fw_rule_sync_cstring_t(dest, src, destination);
        retval |= fw_rule_sync_cstring_t(dest, src, destination_mask);
        retval |= fw_rule_sync_target(dest, src);
        break;
    case FW_FEATURE_OUT_INTERFACE:
        retval |= fw_rule_sync_cstring_t(dest, src, out_interface);
        retval |= fw_rule_sync_string(dest, src, chain);
        break;
    case FW_FEATURE_STRING:
        retval |= fw_rule_sync_cstring_t(dest, src, filter_string);
        break;
    case FW_FEATURE_TIME:
        retval |= fw_rule_sync_cstring_t(dest, src, filter_weekdays);
        retval |= fw_rule_sync_cstring_t(dest, src, filter_start_time);
        retval |= fw_rule_sync_cstring_t(dest, src, filter_end_time);
        break;
    case FW_FEATURE_PHYSDEV_IN:
        retval |= fw_rule_sync_cstring_t(dest, src, physdev_in);
        retval |= fw_rule_sync_bool(dest, src, physdev_in_excluded);
        break;
    case FW_FEATURE_PHYSDEV_OUT:
        retval |= fw_rule_sync_cstring_t(dest, src, physdev_out);
        retval |= fw_rule_sync_bool(dest, src, physdev_out_excluded);
        break;
    case FW_FEATURE_SOURCE_IPSET:
        retval |= fw_rule_sync_cstring_t(dest, src, source_ipset);
        retval |= fw_rule_sync_bool(dest, src, source_ipset_excluded);
        break;
    case FW_FEATURE_DESTINATION_IPSET:
        retval |= fw_rule_sync_cstring_t(dest, src, destination_ipset);
        retval |= fw_rule_sync_bool(dest, src, destination_ipset_excluded);
        break;
    case FW_FEATURE_LAST:
        //Nothing to sync.
        break;
    }

    fw_rule_set_current_feature(dest, FW_FEATURE_LAST);

exit:
    return retval;
}

const amxc_llist_t* fw_rule_get_global_list(void) {
    return (const amxc_llist_t*) &g_rules;
}

void fw_rule_set_traversed(fw_rule_t* const rule, const int traversed) {
    if(rule) {
        rule->traversed = traversed;
    }
}

int fw_rule_get_traversed(const fw_rule_t* const rule) {
    return (rule ? rule->traversed : 0);
}

int fw_rule_set_filter_string(fw_rule_t* const rule, const char* filter_string) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_STRING,
               exit);

    retval = fw_rule_ht_set(cstring_t, rule->ht, "string", filter_string);

exit:
    return retval;
}

const char* fw_rule_get_filter_string(const fw_rule_t* const rule) {
    const char* filter_string = NULL;

    when_null(rule, exit);
    filter_string = fw_rule_ht_get(cstring_t, rule->ht, "string");

exit:
    return filter_string;
}

int fw_rule_set_filter_weekdays(fw_rule_t* const rule, const char* weekdays) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_TIME,
               exit);

    retval = fw_rule_ht_set(cstring_t, rule->ht, "weekdays", weekdays);

exit:
    return retval;
}

const char* fw_rule_get_filter_weekdays(const fw_rule_t* const rule) {
    const char* filter_weekdays = NULL;

    when_null(rule, exit);
    filter_weekdays = fw_rule_ht_get(cstring_t, rule->ht, "weekdays");

exit:
    return filter_weekdays;
}

int fw_rule_set_filter_start_time(fw_rule_t* const rule, const char* start_time) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_TIME,
               exit);

    retval = fw_rule_ht_set(cstring_t, rule->ht, "startTime", start_time);

exit:
    return retval;
}

const char* fw_rule_get_filter_start_time(const fw_rule_t* const rule) {
    const char* filter_start_time = NULL;

    when_null(rule, exit);
    filter_start_time = fw_rule_ht_get(cstring_t, rule->ht, "startTime");

exit:
    return filter_start_time;
}

int fw_rule_set_filter_end_time(fw_rule_t* const rule, const char* end_time) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_TIME,
               exit);

    retval = fw_rule_ht_set(cstring_t, rule->ht, "endTime", end_time);

exit:
    return retval;
}

const char* fw_rule_get_filter_end_time(const fw_rule_t* const rule) {
    const char* filter_end_time = NULL;

    when_null(rule, exit);
    filter_end_time = fw_rule_ht_get(cstring_t, rule->ht, "endTime");

exit:
    return filter_end_time;
}

int fw_rule_set_physdev_out(fw_rule_t* const rule, const char* physdev_out) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_PHYSDEV_OUT,
               exit);

    retval = fw_rule_ht_set(cstring_t, rule->ht, "physdev_out", physdev_out);
exit:
    return retval;
}

const char* fw_rule_get_physdev_out(const fw_rule_t* const rule) {
    const char* physdev_out = NULL;

    when_null(rule, exit);
    physdev_out = fw_rule_ht_get(cstring_t, rule->ht, "physdev_out");

exit:
    return physdev_out;
}

int fw_rule_set_physdev_out_excluded(fw_rule_t* const rule, const bool is_physdev_out_excluded) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_PHYSDEV_OUT,
               exit);

    retval = fw_rule_ht_set(bool, rule->ht, "physdev_out_excluded", is_physdev_out_excluded);

exit:
    return retval;
}

bool fw_rule_get_physdev_out_excluded(const fw_rule_t* const rule) {
    bool physdev_out_excluded = false;

    when_null(rule, exit);
    physdev_out_excluded = fw_rule_ht_get(bool, rule->ht, "physdev_out_excluded");

exit:
    return physdev_out_excluded;
}

int fw_rule_set_physdev_in(fw_rule_t* const rule, const char* physdev_in) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_PHYSDEV_IN,
               exit);

    retval = fw_rule_ht_set(cstring_t, rule->ht, "physdev_in", physdev_in);
exit:
    return retval;
}

const char* fw_rule_get_physdev_in(const fw_rule_t* const rule) {
    const char* physdev_in = NULL;

    when_null(rule, exit);
    physdev_in = fw_rule_ht_get(cstring_t, rule->ht, "physdev_in");

exit:
    return physdev_in;
}

int fw_rule_set_physdev_in_excluded(fw_rule_t* const rule, const bool is_physdev_in_excluded) {
    int retval = -1;

    when_null(rule, exit);
    when_false(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST ||
               fw_rule_get_current_feature(rule) == FW_FEATURE_PHYSDEV_IN,
               exit);

    retval = fw_rule_ht_set(bool, rule->ht, "physdev_in_excluded", is_physdev_in_excluded);

exit:
    return retval;
}

bool fw_rule_get_physdev_in_excluded(const fw_rule_t* const rule) {
    bool physdev_in_excluded = false;

    when_null(rule, exit);
    physdev_in_excluded = fw_rule_ht_get(bool, rule->ht, "physdev_in_excluded");

exit:
    return physdev_in_excluded;
}
