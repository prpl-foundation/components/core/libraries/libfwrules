# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v2.8.0 - 2024-07-05(13:49:28 +0000)

### New

- - Sync ICMP type code pairs with IANA

## Release v2.7.1 - 2024-06-12(15:11:32 +0000)

### Fixes

- Allow ICMP type 0 code 0 in fw_rule

## Release v2.7.0 - 2024-06-10(10:11:50 +0000)

### Other

- - [tr181-firewall] portmapping hairpin NAT rule can be missconfigured

## Release v2.6.2 - 2024-05-28(08:01:33 +0000)

### Fixes

- add support for iptc extension string and time

## Release v2.6.1 - 2024-05-22(07:33:54 +0000)

### Fixes

- It must be possible to configure fw rules based on VendorClassID

## Release v2.6.0 - 2024-05-16(13:09:42 +0000)

### New

- Add getters/setters for ICMP code and type/code matching function

## Release v2.5.0 - 2024-04-05(13:33:53 +0000)

### Fixes

- Cleaning rules of folder messes up the rule order

## Release v2.4.1 - 2024-04-05(06:03:41 +0000)

### Other

- Improve fw_rule_dump to show chain and table.

## Release v2.4.0 - 2024-04-03(14:16:15 +0000)

### New

- Revert "It must be possible to delete specific rules from a folder"

### Fixes

- fw_commit can delete rule at incorrect index
- fw_folder_set_order can cause folder to be deleted from list of folders

### Changes

- Revert "Add missing functionality to set output interface"

## Release v2.3.1 - 2024-03-21(17:30:48 +0000)

### Fixes

- - Fix fw_rule_get_target_log_options

## Release v2.3.0 - 2024-03-20(17:06:28 +0000)

### New

- add support for iptc extension string and time

## Release v2.2.1 - 2024-03-13(12:22:40 +0000)

### Fixes

- Add missing functionality to set output interface

## Release v2.2.0 - 2024-02-29(12:21:01 +0000)

### New

- It must be possible to delete specific rules from a folder

## Release v2.1.5 - 2024-02-28(08:57:31 +0000)

### Changes

- [PacketInterception] Add Flowmonitor support

## Release v2.1.4 - 2024-02-19(07:48:45 +0000)

### Other

- - [Firewall][libfwrules] - Second optimization of fw_commit() function

## Release v2.1.3 - 2024-02-15(15:45:09 +0000)

### Changes

- [PacketInterception] Add NFLOG support

## Release v2.1.2 - 2024-01-31(15:34:18 +0000)

### Fixes

- [Firewall][libfwrules] Optimization of fw_commit() used in Firewall.Service implementation

## Release v2.1.1 - 2023-12-20(15:18:41 +0000)

### Other

- [QOS]Support hardware based QoS

## Release v2.1.0 - 2023-11-16(15:12:13 +0000)

### New

-  [TR-181-Firewall] Add the possibility to LOG functionality in the firewall

## Release v2.0.2 - 2023-04-05(11:08:50 +0000)

### Fixes

- LAN Users cannot take advantage from NAT when using the Box Wan IP to reach service

## Release v2.0.1 - 2023-01-10(10:03:08 +0000)

### Fixes

- avoidable copies of strings, htables and lists

## Release v2.0.0 - 2022-01-25(13:37:57 +0000)

### Breaking

- [Libfwrules] add mask to mark

## Release v1.3.5 - 2022-01-24(14:47:41 +0000)

### Other

- Change documentation path

## Release v1.3.4 - 2022-01-13(11:06:01 +0000)

### Other

- - [libfwrules] Document code
- - [libfwrules] Document code

## Release v1.3.3 - 2022-01-12(16:08:44 +0000)

### Other

- Document code

## Release v1.3.2 - 2022-01-06(21:59:30 +0000)

### Fixes

- [folder] Cleanup unused rules when invoking fw_folder_delete

## Release v1.3.1 - 2022-01-03(14:45:22 +0000)

### Other

- Use BSD-2-Clause-Patent license

## Release v1.3.0 - 2021-11-05(13:13:43 +0000)

### New

- - [libfwrules] Support for ordered rules

## Release v1.2.0 - 2021-10-25(07:43:55 +0000)

### New

- support target NFQUEUE

## Release v1.1.2 - 2021-10-15(08:24:14 +0000)

### Fixes

- fw_folder_fetch_default_rule failure

## Release v1.1.1 - 2021-09-24(10:06:25 +0000)

### Fixes

- Remove linker flag to libfwrules

## Release v1.1.0 - 2021-09-22(10:04:58 +0000)

### New

- match rules by connection state

## Release v1.0.8 - 2021-09-14(07:32:37 +0000)

### Fixes

- cannot fetch new default rule after calling fw_folder_delete_rules

## Release v1.0.7 - 2021-09-12(06:59:44 +0000)

### Other

- Issue: soft.at.home/libraries/libfwrules#7 delete fails because index is too big

## Release v1.0.6 - 2021-08-17(13:18:14 +0000)

### Fixes

- [libfwrules] Pipeline doesn't stop when memory leaks are detected

## Release v1.0.5 - 2021-07-26(14:27:24 +0000)

- Issue: soft.at.home/libraries/libfwrules#6 deb: change library name in copyright file

## Release v1.0.4 - 2021-07-19(12:12:52 +0000)

- Issue: soft.at.home/libraries/libfwrules#5 fw_commit: push only enabled rules to the callback function

## Release v1.0.3 - 2021-07-19(11:51:28 +0000)

- Issue: soft.at.home/libraries/libfwrules#4 fw_rule_clone: the enabled member is not copied when a rule is cloned

## Release v1.0.2 - 2021-07-15(07:08:04 +0000)

- Issue: soft.at.home/libraries/libfwrules#3 baf: missing prorietary parameter

## Release v1.0.1 - 2021-07-14(17:50:47 +0000)

- Issue: soft.at.home/libraries/libfwrules#1 Fix baf.yml
- Issue: soft.at.home/libraries/libfwrules#2 Fix changelog

## Release v1.0.0 - 2021-07-08(09:42:04 +0000)

- Merge 'dev_init' into 'main'

