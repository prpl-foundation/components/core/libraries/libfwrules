/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__FW_FOLDER_H__)
#define __FW_FOLDER_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>

#include <fwrules/fw_features.h>
#include <fwrules/fw_rule.h>

/**
   @defgroup folder Folder API

   @brief
   A folder is a group of rules, belonging together. For example, a folder can
   contain firewall rules for a particular table-chain combination, or for
   specific tasks, like port forwarding, whitelisting, ...
 */

/**
   @ingroup folder
   @brief
   The anonymous @c _fw_folder structure is defined to make sure the API is used
   when manipulating a folder. Therefore, rule struct members cannot be accessed directly.
 */
typedef struct _fw_folder fw_folder_t;

/**
   @ingroup folder
   @brief
   Create a new folder. On creation, one default @ref fw_rule_t is added to the
   folder, which can be used to set common rule parameters, shared by all
   future rules.

   After the folder is created, set the required folder features using @ref
   fw_folder_set_feature. The list of features is represented by @ref
   fw_feature_t.

   Next, the default rule can be fetched by @ref fw_folder_fetch_default_rule.
   Use the Rule API to set the common rule parameters first. When ready, push
   the rule back in the folder using @ref fw_folder_push_rule.

   After the default rule is pushed back in the folder, create zero, one or
   more feature rules. A feature rule can be fetched by @ref
   fw_folder_fetch_feature_rule. Use the Rule API again, this time to set
   specific parameters. When ready, push the rule back in the folder using @ref
   fw_folder_push_rule.

   All folder rules are disabled when the folder is created, but can be enabled
   by @ref fw_folder_set_enabled.

   When the folder contains all rules, commit them by invoking @ref fw_commit.

   @param[out] folder Double pointer to the empty folder struct. The pointer must be NULL.
   @returns
    - @c @c 0 on success, the folder is allocated and must be freed using @ref fw_folder_delete.
    - @c -1 on failure, no memory is allocated.
 */
int fw_folder_new(fw_folder_t** folder);

/**
   @ingroup folder
   @brief
   Delete a folder and free the allocated memory.

   This function frees all allocated memory for the folder. The internal rules
   wich are not processed by the application, i.e. new rules that are still
   waiting for @ref fw_commit to be invoked, are freed. These are new rules,
   having flag @ref FW_RULE_FLAG_NEW.

   In case folder rules have been processed by the application, via @ref
   fw_commit, the rules are not freed. Instead, they are marked for deletion.
   The application MUST call @ref fw_commit again, to make sure the rules are
   removed from the system and to actually free them.

   @attention
   Always invoke @ref fw_commit after this function.

   @param[out] folder Double pointer to the folder to be deleted.

   @returns
    - @c @c 0 on success, the folder is freed and @c folder is set to NULL.
    - @c -1 on failure, no memory is freed.
 */
int fw_folder_delete(fw_folder_t** folder);

/**
   @ingroup folder
   @brief
   Enable or disable a folder.

   @note
   The folder rules are only enabled when all feature requirements are
   fulfilled. That means, minimum one rule of each feature must exist in the
   folder, before the rules can be enabled.

   @param[in] folder Pointer to a folder.
   @param[in] enable @c true to enable, @c false to disable.
   @returns @c 0 on success, @c -1 on failure.
 */
int fw_folder_set_enabled(fw_folder_t* const folder, const bool enable);

/**
   @ingroup folder
   @brief
   Check if a folder is enabled.

   @param[in] folder Pointer to a folder.

   @returns @c true when enabled, otherwise @c false.
 */
bool fw_folder_get_enabled(const fw_folder_t* const folder);

/**
   @ingroup folder
   @brief
   Set a required feature for a folder.

   @param[in] folder Pointer to a folder.
   @param[in] feature The feature to be added to the folder.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_folder_set_feature(fw_folder_t* const folder, fw_feature_t feature);

/**
   @ingroup folder
   @brief
   Unset a feature from a folder.

   @param[in] folder Pointer to a folder.
   @param[in] feature The feature to be removed from the folder.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_folder_unset_feature(fw_folder_t* const folder, fw_feature_t feature);

/**
   @ingroup folder
   @brief
   Get the feature requirements from a folder as set by @ref fw_folder_set_feature.

   @param[in] folder Pointer to a folder.

   @returns
   A bitmap representing the @b required @b feature @b set (@ref fw_feature_t) for this
   folder.  Use @ref fw_feature_contains to check if a specific feature is set.
 */
int fw_folder_get_feature_requirements(const fw_folder_t* const folder);

/**
   @ingroup folder
   @brief
   Get the feature marks from a folder, as set by @ref fw_folder_fetch_feature_rule.

   This function shows the current features rules that are already present in the folder.

   @note
   A folder's feature set is complete when the feature marks (this function),
   equals the feature requirements (@ref fw_folder_get_feature_requirements.

   @param[in] folder Pointer to a folder.

   @returns
   A bitmap representing the @b current @b feature @b set (@ref fw_feature_t) for this
   folder.  Use @ref fw_feature_contains to check if a specific feature is set.
 */
int fw_folder_get_feature_marks(const fw_folder_t* const folder);

/**
   @ingroup folder
   @brief
   Fetch the default rule from a folder. This function should be called after
   creating the folder (@ref fw_folder_new) and setting the required features
   (@ref fw_folder_set_feature).

   The default rule is used to set common rule parameters, like a table, chain,
   policy, ... Use the Rule API for that. When ready, push the rule back in the
   folder (@ref fw_folder_push_rule).

   @note
   The default rule can only be fetched when there is only one rule in the
   folder's rules list. After adding feature rules, it is not possible to fetch
   the default rule again, unless all rules are removed (@ref
   fw_folder_delete_rules).

   @attention
   Always push the rule back in the folder using @ref fw_folder_push_rule,
   otherwise it is impossible to fetch the default rule again or a new feature rule.

   @param[in] folder Pointer to a folder.

   @returns
   Pointer to the default rule (@ref fw_rule_t), otherwise NULL.
 */
fw_rule_t* fw_folder_fetch_default_rule(fw_folder_t* const folder);

/**
   @ingroup folder
   @brief
   Fetch a feature rule from a folder. This function should be called after
   setting the common rule parameters in the default rule (@ref
   fw_folder_fetch_default_rule).

   A feature rule is used to set specific rule parameters. Check @ref
   fw_feature_t for the list of features. When a feature rule is fetched, use
   the Rule API to set the parameters. When ready, push the rule back in the
   folder (@ref fw_folder_push_rule).

   @note
   - Not all Rule API functions support a particular feature. Check the
   documentation.
   - Fetching a feature rule is only possible if the feature is set by @ref
   fw_folder_set_feature.

   @attention
   Always push the rule back in the folder using @ref fw_folder_push_rule,
   otherwise it is impossible to fetch another feature rule.

   @param[in] folder Pointer to a folder.
   @param[in] feature The feature for which a rule must be fetched.

   @returns
   Pointer to the feature rule (@ref fw_rule_t), otherwise NULL.
 */
fw_rule_t* fw_folder_fetch_feature_rule(fw_folder_t* const folder, const fw_feature_t feature);

/**
   @ingroup folder
   @brief
   Push a default rule or feature rule back in the folder.

   When a default or feature rule is fetched using @ref
   fw_folder_fetch_default_rule or @ref fw_folder_fetch_feature_rule, the rule
   must be pushed back in the folder to mark it as ready.

   Without calling this function, it is not possible to fetch the same default
   rule or a new feature rule.

   @param[in] folder Pointer to a folder.
   @param[in] rule Pointer to a fetched rule.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_folder_push_rule(fw_folder_t* const folder, fw_rule_t* const rule);

/**
   @ingroup folder
   @brief
   Delete all rules from a folder.

   This function removes folder rules immediately when their flag is still set
   to @ref FW_RULE_FLAG_NEW. These are the rules that are not processed by the
   application. The other rules are marked for deletion. Therefore it is
   required to invoke @ref fw_commit after this function.

   After @ref fw_commit is called, new rules can be added, starting from the
   default rule, followed by feature rules.

   @attention
   Invoke @ref fw_commit after this function.

   @param[in] folder Pointer to a folder.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_folder_delete_rules(fw_folder_t* const folder);

/**
   @ingroup folder
   @brief
   Delete feature rules from a folder.

   This function removes all folder rules having a particular feature. If the
   rules still have flag @ref FW_RULE_FLAG_NEW, then they are freed
   immediately. The other rules are marked for deletion. @ref fw_commit must be
   invoked after this function.

   Since one set of feature rules is deleted, all folder rules are disabled.
   The required feature set does not match with the current one at that moment.
   Therefore, new feature rules must be added (@ref
   fw_folder_fetch_feature_rule) or the required feature must be unset (@ref
   fw_folder_unset_feature).

   @attention
   Invoke @ref fw_commit after this function.

   @param[in] folder Pointer to a folder.
   @param[in] feature The feature for which folder rules need to be deleted.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_folder_delete_feature_rules(fw_folder_t* const folder, const fw_feature_t feature);

/**
   @ingroup folder
   @brief
   Set the order for a folder.

   When having multiple folders containing rules for the same table and chain,
   you might want to have those rules in a particular order so that rules in
   folder x are always applied before rules in folder y.

   @param[in] folder Pointer to a folder.
   @param[in] order The index this folder is assigned to. When 0, the order is
   applied automatically.  When the order is larger than the next available
   order, the order is set automatically to the next available one.

   @warning
   Make sure there is no fetched rule when calling this function. This results in a failure.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_folder_set_order(fw_folder_t* const folder, unsigned int order);

/**
   @ingroup folder
   @brief
   Get the order of a folder.

   @param[in] folder Pointer to a folder.

   @returns @c 0 on success, @c -1 on failure.
 */
unsigned int fw_folder_get_order(fw_folder_t* const folder);

#ifdef __cplusplus
}
#endif

#endif // __FW_FOLDER_H
