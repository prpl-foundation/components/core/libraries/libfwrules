/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__FW_RULE_H__)
#define __FW_RULE_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>
#include <stdint.h>

#include <amxc/amxc_llist.h>

/**
   @defgroup rule Rule API
   @brief
   The rules which can be added to the folder.
 */

#define FW_RULE_TABLE_LEN       64
#define FW_RULE_CHAIN_LEN       64

/**
   @ingroup rule
   @brief
   The flag describes the state a rule is in. For the application, a rule can
   be new, modified or deleted.
 */
typedef enum _fw_rule_flag {
    FW_RULE_FLAG_NEW,       /**< A rule is marked as new and does not yet exist in the firewall. */
    FW_RULE_FLAG_MODIFIED,  /**< A rule is marked as modified. It is already present in the firewall. */
    FW_RULE_FLAG_DELETED,   /**< A rule is marked as deleted. It is already present in the firewall. */
    FW_RULE_FLAG_HANDLED,   /**< Internal state, the rule is handled by the application. */
    FW_RULE_FLAG_LAST,      /**< Last item. Do not use. Can be returned on failure. */
} fw_rule_flag_t;

/**
   @ingroup rule
   @brief
   The rule's policy, which can be used to accept, reject, drop or masquerade a packet.
 */
typedef enum _fw_rule_policy {
    FW_RULE_POLICY_DROP,        /**< Drop a matching packet. */
    FW_RULE_POLICY_ACCEPT,      /**< Accept a matching packet. */
    FW_RULE_POLICY_REJECT,      /**< Reject a matching packet. */
    FW_RULE_POLICY_MASQUERADE,  /**< Masquerade a matching packet. */
    FW_RULE_POLICY_LAST         /**< Last item. Do not use. Can be returned on failure. */
} fw_rule_policy_t;

/**
   @ingroup rule
   @brief
   The target of the rule. It defines what to do when a matching packet is found.
 */
typedef enum _fw_rule_target {
    FW_RULE_TARGET_NONE,        /**< The target is not set. */
    FW_RULE_TARGET_RETURN,      /**< Stop traversing the current table-chain and return. */
    FW_RULE_TARGET_MARK,        /**< Mark the packet internally. */
    FW_RULE_TARGET_DSCP,        /**< Set the DSCP field in the packet header. */
    FW_RULE_TARGET_QUEUE,       /**< Obsolete. */
    FW_RULE_TARGET_PBIT,        /**< Set the skb->priority value. */
    FW_RULE_TARGET_POLICY,      /**< Set the target's policy. See @ref fw_rule_policy_t. */
    FW_RULE_TARGET_CHAIN,       /**< Jump to a chain. */
    FW_RULE_TARGET_DNAT,        /**< Jump to the DNAT target. */
    FW_RULE_TARGET_SNAT,        /**< Jump to the SNAT target. */
    FW_RULE_TARGET_REDIRECT,    /**< Obsolete. */
    FW_RULE_TARGET_NFQUEUE,     /**< Pass a packet, via a particular queue, to userspace. */
    FW_RULE_TARGET_NFLOG,       /**< Log a packet, via a particular group, to userspace. */
    FW_RULE_TARGET_LOG,         /**< Log packet and passtrough.  */
    FW_RULE_TARGET_CLASSIFY,    /**< Classify the packet internally */
    FW_RULE_TARGET_LAST         /**< Last item. Do not use. Can be returned on failure. */
} fw_rule_target_t;

/**
   @ingroup rule
   @brief
   NFQUEUE target options.
 */
typedef enum _fw_rule_nfqueue_flag {
    FW_RULE_NFQ_FLAG_BYPASS     = 1, /**< Accept packets if no userspace application is listening on a NFQUEUE. */
    FW_RULE_NFQ_FLAG_CPU_FANOUT = 2, /**< Use CPU ID as an index to map packets to queues. */
} fw_rule_nfqueue_flag;

/**
   @ingroup rule
   @brief
   The rule struct.

   The anonymous @c _fw_rule structure is defined to make sure the API is used
   when manipulating a rule. Therefore, rule struct members cannot be accessed directly.
 */
typedef struct _fw_rule fw_rule_t;

/**
   @ingroup rule
   @brief
   Create a new rule. The IP version is set to IPv4. If IPv6 is required, the
   version can be changed by @ref fw_rule_set_ipv6.

   @param[out] rule Double pointer to the empty rule struct. The pointer must be NULL.
   @returns
    - @c @c 0 on success, the rule is allocated and must be freed using @ref fw_rule_delete.
    - @c -1 on failure, no memory is allocated.
 */
int fw_rule_new(fw_rule_t** rule);

/**
   @ingroup rule
   @brief
   Delete a rule and free the allocated memory.

   @param[out] rule Double pointer to the rule to deleted.

   @returns
    - @c @c 0 on success, the rule is freed and @c rule is set to NULL.
    - @c -1 on failure, no memory is freed.
 */
int fw_rule_delete(fw_rule_t** rule);

/**
   @ingroup rule
   @brief
   Check if a rule is enabled.

   @param[in] rule Pointer to a rule.

   @returns @c true when enabled, otherwise @c false.
 */
bool fw_rule_is_enabled(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Enable or disable a rule. Use this function only when using a standalone
   rule. When using the @ref folder API, use @ref fw_folder_set_enabled to
   properly set the @c enabled flag.

   @param[in] rule Pointer to a rule.
   @param[in] enable @c true to enable, @c false to disable.

   @returns @c @c 0 on success, @c @c -1 on failure.
 */
int fw_rule_set_enabled(fw_rule_t* const rule, bool enable);

/**
   @ingroup rule
   @brief
   Get the rule flag.

   @param[in] rule Pointer to a rule.

   @returns The @ref fw_rule_flag_t flag which is currently set.
 */
fw_rule_flag_t fw_rule_get_flag(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Dump the internal hashtable with the table and chain, containing all rule's parameters, to a file descriptor.
   This function is suitable for debugging purposes.

   @details
   @param[in] rule Pointer to a rule.
   @param[in] fd The file descriptor the internal hashtable is dumped to.

   @returns @c @c 0 on success, @c @c -1 on failure.
 */
int fw_rule_dump(const fw_rule_t* const rule, int fd);

/**
   @ingroup rule
   @brief
   Set the IP version to IPv4. This function is the opposite of @ref fw_rule_set_ipv6.

   @par Supported features:
   - No feature is set.
   - @ref FW_FEATURE_IPVERSION

   @param[in] rule Pointer to a rule.
   @param[in] is_ipv4 @c true if the IP version is IPv4, otherwise @c false, which
   sets the IP version to IPv6.

   @returns @c @c 0 on success, @c @c -1 on failure
 */
int fw_rule_set_ipv4(fw_rule_t* const rule, bool is_ipv4);

/**
   @ingroup rule
   @brief
   Set the IP version to IPv6. This function is the opposite of @ref fw_rule_set_ipv4.

   @par Supported features:
   - No feature is set.
   - @ref FW_FEATURE_IPVERSION

   @param[in] rule Pointer to a rule.
   @param[in] is_ipv6 @c true if the IP version is IPv6, otherwise @c false, which
   sets the IP version to IPv4.

   @returns @c @c 0 on success, @c @c -1 on failure
 */
int fw_rule_set_ipv6(fw_rule_t* const rule, bool is_ipv6);

/**
   @ingroup rule
   @brief
   Set the rule's table.

   @par Supported features:
   - No feature is set.
   - @ref FW_FEATURE_TABLE

   @param[in] rule Pointer to a rule.
   @param[in] table The table to set. E.g. for netfilter, this can be filter,
   mangle, nat, ...

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_table(fw_rule_t* const rule, const char* table);

/**
   @ingroup rule
   @brief
   Set the source IP address.

   @par Supported features:
   - No feature is set.
   - @ref FW_FEATURE_SOURCE
   - @ref FW_FEATURE_WHITELIST
   - @ref FW_FEATURE_SPOOFING

   @param[in] rule Pointer to a rule.
   @param[in] source source IP address in IPv4 or IPv6 CIDR notation. It is allowed to add a prefix.

   @note
   When no prefix is added, @ref fw_rule_set_source_mask should be used.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_source(fw_rule_t* const rule, const char* source);

/**
   @ingroup rule
   @brief
   Set the source mask.

   @par Supported features:
   - No feature is set.
   - @ref FW_FEATURE_SOURCE
   - @ref FW_FEATURE_WHITELIST
   - @ref FW_FEATURE_SPOOFING

   @param[in] rule Pointer to a rule.
   @param[in] source_mask Mask in CIDR format.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_source_mask(fw_rule_t* const rule, const char* source_mask);

/**
   @ingroup rule
   @brief
   Set the source IPSet.

   @par Supported features:
   - No feature is set.
   - @ref FW_FEATURE_SOURCE_IPSET

   @param[in] rule Pointer to a rule.
   @param[in] source_set Set.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_source_ipset(fw_rule_t* const rule, const char* source_set);

/**
   @ingroup rule
   @brief
   Set the ingress interface.

   @par Supported features:
   - No feature is set.
   - @ref FW_FEATURE_IN_INTERFACE
   - @ref FW_FEATURE_POLICY
   - @ref FW_FEATURE_WHITELIST
   - @ref FW_FEATURE_SPOOFING
   - @ref FW_FEATURE_ISOLATION

   @param[in] rule Pointer to a rule.
   @param[in] in_interface The ingress interface, e.g. eth0, wwan0, ppp1, ...
 */
int fw_rule_set_in_interface(fw_rule_t* const rule, const char* in_interface);

/**
   @ingroup rule
   @brief
   Set the rule's chain.

   @par Supported features:
   - No feature is set.
   - @ref FW_FEATURE_IN_INTERFACE

   @param[in] rule Pointer to a rule.
   @param[in] chain The chain to set. E.g. for netfilter, this can be INPUT,
   OUTPUT, PREROUTING ...

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_chain(fw_rule_t* const rule, const char* chain);

/**
   @ingroup rule
   @brief
   Set the destination IP address.

   @par Supported features:
   - No feature is set.
   - @ref FW_FEATURE_DESTINATION
   - @ref FW_FEATURE_SPOOFING
   - @ref FW_FEATURE_ISOLATION

   @param[in] rule Pointer to a rule.
   @param[in] destination source IP address in IPv4 or IPv6 CIDR notation. It is allowed to add a prefix.

   @note
   When no prefix is added, @ref fw_rule_set_destination_mask should be used.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_destination(fw_rule_t* const rule, const char* destination);

/**
   @ingroup rule
   @brief
   Set the destination mask.

   @par Supported features:
   - No feature is set.
   - @ref FW_FEATURE_DESTINATION
   - @ref FW_FEATURE_SPOOFING
   - @ref FW_FEATURE_ISOLATION

   @param[in] rule Pointer to a rule.
   @param[in] destination_mask Mask in CIDR format.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_destination_mask(fw_rule_t* const rule, const char* destination_mask);

/**
   @ingroup rule
   @brief
   Set the destination IPSet.

   @par Supported features:
   - No feature is set.
   - @ref FW_FEATURE_DESTINATION_IPSET

   @param[in] rule Pointer to a rule.
   @param[in] destination_set Set.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_destination_ipset(fw_rule_t* const rule, const char* destination_set);

/**
   @ingroup rule
   @brief
   Set the destination port.

   @par Supported features:
   - No feature is set.
   - @ref FW_FEATURE_DESTINATION_PORT
   - @ref FW_FEATURE_DNAT

   @param[in] rule Pointer to a rule.
   @param[in] destination_port The destination port.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_destination_port(fw_rule_t* const rule, const uint32_t destination_port);

/**
   @ingroup rule
   @brief
   Set the destination port max range.

   @par Supported features:
   - No feature is set.
   - @ref FW_FEATURE_DESTINATION_PORT
   - @ref FW_FEATURE_DNAT

   @param[in] rule Pointer to a rule.
   @param[in] destination_port_range_max The range of the destination port is from
   the port set in @ref fw_rule_set_destination_port to the value of this
   parameter.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_destination_port_range_max(fw_rule_t* const rule, const uint32_t destination_port_range_max);

/**
   @ingroup rule
   @brief
   Set the rule protocol.

   @par Supported features:
   - No feature is set.
   - @ref FW_FEATURE_PROTOCOL

   @param[in] rule Pointer to a rule.
   @param[in] protocol The protocol to be set for the rule.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_protocol(fw_rule_t* const rule, const uint32_t protocol);

/**
   @ingroup rule
   @brief
   Set the source MAC address.

   @par Supported features:
   - No feature is set.
   - @ref FW_FEATURE_SOURCE_MAC
   - @ref FW_FEATURE_SPOOFING
   - @ref FW_FEATURE_WHITELIST

   @param[in] rule Pointer to a rule.
   @param[in] source_mac_address 6-byte MAC address in format xx:xx:xx:xx:xx:xx.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_source_mac_address(fw_rule_t* const rule, const char* source_mac_address);

/**
   @ingroup rule
   @brief
   Set a DSCP value to match.

   @par Supported features:
   - No feature is set.
   - @ref FW_FEATURE_DSCP

   @param[in] rule Pointer to a rule.
   @param[in] dscp DSCP value.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_dscp(fw_rule_t* const rule, const uint32_t dscp);

/**
   @ingroup rule
   @brief
   Exclude a DSCP value. This is a NOT operator for the DSCP value set in @ref
   fw_rule_set_dscp.

   @par Supported features:
   - No feature is set.
   - @ref FW_FEATURE_DSCP

   @param[in] rule Pointer to a rule.
   @param[in] is_dscp_excluded @c true if the DSCP value must be excluded, otherwise @c false.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_dscp_excluded(fw_rule_t* const rule, const bool is_dscp_excluded);

/**
   @ingroup rule
   @brief
   Set the egress interface.

   @par Supported features:
   - No feature is set.
   - @ref FW_FEATURE_POLICY

   @param[in] rule Pointer to a rule.
   @param[in] out_interface The egress interface, e.g. eth0, wwan0, ppp1, ...
 */
int fw_rule_set_out_interface(fw_rule_t* const rule, const char* out_interface);

/**
   @ingroup rule
   @brief
   Exclude an ingress interface. This is a NOT operator for the ingress
   interface set in @ref fw_rule_set_in_interface.

   @par Supported features:
   - No feature is set.
   - @ref FW_FEATURE_SPOOFING

   @param[in] rule Pointer to a rule.
   @param[in] is_in_interface_excluded @c true if the ingress interface must be excluded, otherwise @c false.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_in_interface_excluded(fw_rule_t* const rule, const bool is_in_interface_excluded);

/**
   @ingroup rule
   @brief
   Exclude an egress interface. This is a NOT operator for the egress
   interface set in @ref fw_rule_set_out_interface.

   @par Supported features:
   - No feature is set.

   @param[in] rule Pointer to a rule.
   @param[in] is_out_interface_excluded @c true if the egress interface must be excluded, otherwise @c false.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_out_interface_excluded(fw_rule_t* const rule, const bool is_out_interface_excluded);

/**
   @ingroup rule
   @brief
   Exclude a source IP address. This is a NOT operator for the IP address set
   in @ref fw_rule_set_source.

   @par Supported features:
   - No feature is set.
   - @ref FW_FEATURE_SPOOFING

   @param[in] rule Pointer to a rule.
   @param[in] is_source_excluded @c true if the source IP address must be excluded, otherwise @c false.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_source_excluded(fw_rule_t* const rule, const bool is_source_excluded);

/**
   @ingroup rule
   @brief
   Exclude a destination IP address. This is a NOT operator for the IP address
   set in @ref fw_rule_set_destination.

   @par Supported features:
   - No feature is set.

   @param[in] rule Pointer to a rule.
   @param[in] is_destination_excluded @c true if the destination IP address must be excluded, otherwise @c false.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_destination_excluded(fw_rule_t* const rule, const bool is_destination_excluded);

/**
   @ingroup rule
   @brief
   Exclude a source IPSet. This is a NOT operator for the IPSet set
   in @ref fw_rule_set_source_ipset.

   @par Supported features:
   - No feature is set.
   - @ref FW_FEATURE_SOURCE_IPSET

   @param[in] rule Pointer to a rule.
   @param[in] is_source_set_excluded @c true if the source IPSet must be excluded, otherwise @c false.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_source_ipset_excluded(fw_rule_t* const rule, const bool is_source_set_excluded);

/**
   @ingroup rule
   @brief
   Exclude a destination IPSet. This is a NOT operator for the IPSet set
   in @ref fw_rule_set_destination_ipset.

   @par Supported features:
   - No feature is set.
   - @ref FW_FEATURE_DESTINATION_IPSET

   @param[in] rule Pointer to a rule.
   @param[in] is_destination_set_excluded @c true if the destination IPSet must be excluded, otherwise @c false.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_destination_ipset_excluded(fw_rule_t* const rule, const bool is_destination_set_excluded);

/**
   @ingroup rule
   @brief
   Exclude an IP protocol. This is a NOT operator for the IP protocol set in
   @ref fw_rule_set_protocol.

   @par Supported features:
   - No feature is set.

   @param[in] rule Pointer to a rule.
   @param[in] is_protocol_excluded @c true if the IP protocol must be excluded, otherwise @c false.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_protocol_excluded(fw_rule_t* const rule, const bool is_protocol_excluded);

/**
   @ingroup rule
   @brief
   Exclude a source MAC address. This is a NOT operator for the source MAC
   address set in @ref fw_rule_set_source_mac_address.

   @par Supported features:
   - No feature is set.
   - @ref FW_FEATURE_SOURCE_MAC
   - @ref FW_FEATURE_SPOOFING

   @param[in] rule Pointer to a rule.
   @param[in] is_source_mac_excluded @c true if the DSCP value must be excluded, otherwise @c false.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_source_mac_excluded(fw_rule_t* const rule, const bool is_source_mac_excluded);

/**
   @ingroup rule
   @brief
   Exclude a destination MAC address. This is a NOT operator for the
   destination MAC address.

   @par Supported features:
   - No feature is set.

   @param[in] rule Pointer to a rule.
   @param[in] is_destination_mac_excluded @c true if the DSCP value must be excluded, otherwise @c false.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_destination_mac_excluded(fw_rule_t* const rule, const bool is_destination_mac_excluded);

/**
   @ingroup rule
   @brief
   Exclude a source port. This is a NOT operator for the source port set in @ref fw_rule_set_source_port.

   @par Supported features:
   - No feature is set.
   - @ref FW_FEATURE_SOURCE_PORT
   - @ref FW_FEATURE_SNAT

   @param[in] rule Pointer to a rule.
   @param[in] is_source_port_excluded @c true if the source port must be excluded, otherwise @c false.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_source_port_excluded(fw_rule_t* const rule, const bool is_source_port_excluded);

/**
   @ingroup rule
   @brief
   Exclude a destination port. This is a NOT operator for the destination port set in @ref fw_rule_set_destination_port.

   @par Supported features:
   - No feature is set.
   - @ref FW_FEATURE_DESTINATION_PORT
   - @ref FW_FEATURE_DNAT

   @param[in] rule Pointer to a rule.
   @param[in] is_destination_port_excluded @c true if the destination port must be excluded, otherwise @c false.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_destination_port_excluded(fw_rule_t* const rule, const bool is_destination_port_excluded);

/**
   @ingroup rule
   @brief
   Set the source port.

   @par Supported features:
   - No feature is set.
   - @ref FW_FEATURE_SOURCE_PORT
   - @ref FW_FEATURE_SNAT

   @param[in] rule Pointer to a rule.
   @param[in] source_port The source port.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_source_port(fw_rule_t* const rule, const uint32_t source_port);

/**
   @ingroup rule
   @brief
   Set the source port max range.

   @par Supported features:
   - No feature is set.
   - @ref FW_FEATURE_SOURCE_PORT
   - @ref FW_FEATURE_SNAT

   @param[in] rule Pointer to a rule.
   @param[in] source_port_range_max The range of the source port is from
   the port set in @ref fw_rule_set_source_port to the value of this
   parameter.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_source_port_range_max(fw_rule_t* const rule, const uint32_t source_port_range_max);

/**
   @ingroup rule
   @brief
   Set the minimum length of an IP packet.

   @par Supported features:
   - No feature is set.

   @param[in] rule Pointer to a rule.
   @param[in] ip_min_length The minimum value of an IP packet length.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_ip_min_length(fw_rule_t* const rule, const uint32_t ip_min_length);

/**
   @ingroup rule
   @brief
   Set the maximum length of an IP packet.

   @par Supported features:
   - No feature is set.

   @param[in] rule Pointer to a rule.
   @param[in] ip_max_length The maximum value of an IP packet length.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_ip_max_length(fw_rule_t* const rule, const uint32_t ip_max_length);

/**
   @ingroup rule
   @brief
   Set the ICMP type to match.

   @par Supported features:
   - No feature is set.

   @param[in] rule Pointer to a rule.
   @param[in] icmp_type The ICMP type (see the Linux kernel source file
   include/linux/icmp.h). Examples of ICMP types: Echo Reply (0), Echo Request
   (8), ...

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_icmp_type(fw_rule_t* const rule, const int32_t icmp_type);

/**
   @ingroup rule
   @brief
   Set the ICMPv6 type to match.

   @par Supported features:
   - No feature is set.

   @param[in] rule Pointer to a rule.
   @param[in] icmpv6_type The ICMPv6 type (see the Linux kernel source file
   include/linux/icmpv6.h). Examples of ICMPv6 types: Echo Reply (129), Echo Request
   (128), ...

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_icmpv6_type(fw_rule_t* const rule, const int32_t icmpv6_type);


/**
   @ingroup rule
   @brief
   Set the ICMP code to match the ICMP type.

   @par Supported features:
   - No feature is set.

   @param[in] rule Pointer to a rule.
   @param[in] icmp_code The ICMP code attached to an ICMP type (see the Linux kernel source file
   include/linux/icmp.h). Examples of ICMP code: Host Unreachable (1) with type Destination Unreachable (3)

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_icmp_code(fw_rule_t* const rule, const int32_t icmp_code);

/**
   @ingroup rule
   @brief
   Set the ICMPv6 code to match the ICMPv6 type.

   @par Supported features:
   - No feature is set.

   @param[in] rule Pointer to a rule.
   @param[in] icmpv6_code The ICMPv6 code attached to an ICMPv6 type (see the Linux kernel source file
   include/linux/icmpv6.h). Examples of ICMPv6 code: Port Unreachable (4) with type Destination Unreachable (1)

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_icmpv6_code(fw_rule_t* const rule, const int32_t icmpv6_code);

/**
   @ingroup rule
   @brief
   Check that the ICMP type/code combination is supported before setting the ICMP code.
   Use "-1" as ICMP code to have the code be ignored in the rule.
   Returns an error if the combination is not supported or the function failed to set the code.

   @par Supported features:
   - No feature is set.

   @param[in] rule Pointer to a rule.
   @param[in] icmp_code An ICMP code.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_filtered_icmp_code(fw_rule_t* const rule, const int32_t icmp_code);

/**
   @ingroup rule
   @brief
   Check that the ICMPv6 type/code combination is supported before setting the ICMPv6 code.
   Use "-1" as ICMPv6 code to have the code be ignored in the rule.
   Returns an error if the combination is not supported or the function failed to set the code.

   @par Supported features:
   - No feature is set.

   @param[in] rule Pointer to a rule.
   @param[in] icmpv6_code An ICMPv6 code.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_filtered_icmpv6_code(fw_rule_t* const rule, const int32_t icmpv6_code);

/**
   @ingroup rule
   @brief
   Set the connection tracking state to match.

   @par Supported features:
   - No feature is set.

   @param[in] rule Pointer to a rule.
   @param[in] state A comma separated list of connection states to match. Possible
   values:
   - INVALID
   - NEW
   - ESTABLISHED
   - RELATED
   - UNTRACKED
   - SNAT
   - DNAT

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_conntrack_state(fw_rule_t* const rule, const char* state);

/**
   @ingroup rule
   @brief
   Set the minimum value of the connbytes parameter to match.

   @par Supported features:
   - No feature is set.

   @param[in] rule Pointer to a rule.
   @param[in] min the minimum value to match.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_connbytes_min(fw_rule_t* const rule, const uint64_t min);

/**
   @ingroup rule
   @brief
   Set the maximum value of the connbytes parameter to match.

   @par Supported features:
   - No feature is set.

   @param[in] rule Pointer to a rule.
   @param[in] max the maximum value to match.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_connbytes_max(fw_rule_t* const rule, const uint64_t max);

/**
   @ingroup rule
   @brief
   Set the connbytes monitored parameter to match.

   @par Supported features:
   - No feature is set.

   @param[in] rule Pointer to a rule.
   @param[in] param The parameter that should be monitored. Possible
   values:
   - packets         The amount of packets.
   - bytes           The number of bytes.
   - averagepacket   The average size (in bytes) of all packets received so far.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_connbytes_param(fw_rule_t* const rule, const char* param);

/**
   @ingroup rule
   @brief
   Set the direction of the packets to match.

   @par Supported features:
   - No feature is set.

   @param[in] rule Pointer to a rule.
   @param[in] direction A packet direction to match. Possible
   values:
   - original
   - reply
   - both

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_connbytes_direction(fw_rule_t* const rule, const char* direction);

/**
   @ingroup rule
   @brief
   Set a skb mark value to match.

   @par Supported features:
   - No feature is set.

   @param[in] rule Pointer to a rule.
   @param[in] mark skb mark value.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_skb_mark(fw_rule_t* const rule, const uint32_t mark);

/**
   @ingroup rule
   @brief
   Set a skb mark mask value to match.

   @par Supported features:
   - No feature is set.

   @param[in] rule Pointer to a rule.
   @param[in] mask skb mark mask value.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_skb_mark_mask(fw_rule_t* const rule, const uint32_t mask);

/**
   @ingroup rule
   @brief
   Exclude a skb mark value. This is a NOT operator for the skb mark value set in @ref
   fw_rule_set_skb_mark.

   @par Supported features:
   - No feature is set.

   @param[in] rule Pointer to a rule.
   @param[in] is_skb_mark_excluded @c true if the skb mark value must be excluded, otherwise @c false.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_skb_mark_excluded(fw_rule_t* const rule, const bool is_skb_mark_excluded);

/**
   @ingroup rule
   @brief
   Set a conn mark value to match.

   @par Supported features:
   - No feature is set.

   @param[in] rule Pointer to a rule.
   @param[in] mark conn mark value.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_conn_mark(fw_rule_t* const rule, const uint32_t mark);

/**
   @ingroup rule
   @brief
   Set a conn mark mask value to match.

   @par Supported features:
   - No feature is set.

   @param[in] rule Pointer to a rule.
   @param[in] mask conn mark mask value.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_conn_mark_mask(fw_rule_t* const rule, const uint32_t mask);

/**
   @ingroup rule
   @brief
   Exclude a conn mark value. This is a NOT operator for the conn mark value set in @ref
   fw_rule_set_conn_mark.

   @par Supported features:
   - No feature is set.

   @param[in] rule Pointer to a rule.
   @param[in] is_conn_mark_excluded @c true if the conn mark value must be excluded, otherwise @c false.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_conn_mark_excluded(fw_rule_t* const rule, const bool is_conn_mark_excluded);

/**
   @ingroup rule
   @brief
   Jump to the RETURN target when a match is found.

   @param[in] rule Pointer to a rule.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_target_return(fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Jump to the MARK target when a match is found and set the mark.

   @param[in] rule Pointer to a rule.
   @param[in] mark The mark to set for the packet.
   @param[in] mask The mask of the mark.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_target_mark(fw_rule_t* const rule, const uint32_t mark, const uint32_t mask);

/**
   @ingroup rule
   @brief
   Jump to the CLASSIFY target when a match is found and set the class.

   @param[in] rule Pointer to a rule.
   @param[in] class The class to set for the packet.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_target_classify(fw_rule_t* const rule, const uint32_t class);

/**
   @ingroup rule
   @brief
   Jump to the DSCP target when a match is found and set the DSCP field in the
   IP header.

   @param[in] rule Pointer to a rule.
   @param[in] dscp The DSCP value to set in the IP header.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_target_dscp(fw_rule_t* const rule, const uint32_t dscp);

/**
   @ingroup rule
   @brief
   Obsolete.
   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_target_queue(fw_rule_t* const rule, const uint32_t queue);

/**
   @ingroup rule
   @brief
   Jump to the CLASSIFY target when a match is found and set the priority bit
   field in the socket buffer (skb).

   @param[in] rule Pointer to a rule.
   @param[in] pbit The priority value.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_target_pbit(fw_rule_t* const rule, const uint32_t pbit);

/**
   @ingroup rule
   @brief
   Jump to the standard target when a match is found and accept, drop, reject
   or masquerade a packet.

   @param[in] rule Pointer to a rule.
   @param[in] policy The policy that is applied to the packet. See @ref fw_rule_policy_t.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_target_policy(fw_rule_t* const rule, const fw_rule_policy_t policy);

/**
   @ingroup rule
   @brief
   Jump to the DNAT target when a match is found and modify the destination
   address of the packet and optionally set a port range.

   @param[in] rule Pointer to a rule.
   @param[in] ip The packet's destination address is changed by this value. The IP
   must be in CIDR format.
   @param[in] min_port The minimum port in the range. Ignored if set to @c 0.
   @param[in] max_port The maximum port in the range. Ignored if set to @c 0.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_target_dnat(fw_rule_t* const rule, const char* ip, const uint32_t min_port, const uint32_t max_port);

/**
   @ingroup rule
   @brief
   Jump to the SNAT target when a match is found and modify the source address
   of the packet and optionally set a port range.

   @param[in] rule Pointer to a rule.
   @param[in] ip The packet's destination address is changed by this value. The IP
   must be in CIDR format.
   @param[in] min_port The minimum port in the range. Ignored if set to @c 0.
   @param[in] max_port The maximum port in the range. Ignored if set to @c 0.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_target_snat(fw_rule_t* const rule, const char* ip, const uint32_t min_port, const uint32_t max_port);

/**
   @ingroup rule
   @brief
   Jump to another chain when a match is found.

   @param[in] rule Pointer to a rule.
   @param[in] chain Chain to jump to after a match.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_target_chain(fw_rule_t* const rule, const char* chain);

/**
   @ingroup rule
   @brief
   Jump to the NFQUEUE target, to send packets via a particular queue to userspace.

   @param[in] rule Pointer to a rule.
   @param[in] qnum The queue number to use.
   @param[in] qtotal Maximum number of queues.
   @param[in] flags A bitmask of @ref fw_rule_nfqueue_flag.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_target_nfqueue_options(fw_rule_t* const rule, const uint32_t qnum, const uint32_t qtotal, const uint32_t flags);

/**
   @ingroup rule
   @brief
   Jump to the NFLOG target, to log packets via a particular group to userspace.

   @param[in] rule Pointer to a rule.
   @param[in] gnum The group number to use.
   @param[in] len Length of the packets to copy. 0 means all.
   @param[in] threshold Threshold of how many packets to queue to userspace.
   @param[in] flags Flags to add extra details to the nflog line
   @param[in] prefix String that is prepended to the nflog line.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_target_nflog_options(fw_rule_t* const rule, const uint32_t gnum, const uint32_t len, const uint32_t threshold, const uint32_t flags, const char* prefix);

/**
   @ingroup rule
   @brief
   Jump to the LOG target, to log packets.

   @param[in] rule Pointer to a rule
   @param[in] level Log level for this rule
   @param[in] logflags Flags to add extra details to the log line
   @param[in] prefix String that is prepended to the log line

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_target_log_options(fw_rule_t* const rule, uint8_t level, uint8_t logflags, const char* prefix);

/**
   @ingroup rule
   @brief
   Get a rule's table value as set by @ref fw_rule_set_table.

   @param[in] rule Pointer to a rule.

   @returns
   Pointer to a string if set, otherwise NULL.

   @attention
   The caller must not free the pointer.
 */
const char* fw_rule_get_table(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's source IP address as set by @ref fw_rule_set_source.

   @param[in] rule Pointer to a rule.

   @returns
   Pointer to a string if set, otherwise NULL.

   @attention
   The caller must not free the pointer.
 */
const char* fw_rule_get_source(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's source IP address mask as set by @ref
   fw_rule_set_source_mask.

   @param[in] rule Pointer to a rule.

   @returns
   Pointer to a string if set, otherwise NULL.

   @attention
   The caller must not free the pointer.
 */
const char* fw_rule_get_source_mask(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's source IPSet as set by @ref
   fw_rule_set_source_ipset.

   @param[in] rule Pointer to a rule.

   @returns
   Pointer to a string if set, otherwise NULL.

   @attention
   The caller must not free the pointer.
 */
const char* fw_rule_get_source_ipset(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's ingress interface as set by @ref fw_rule_set_in_interface.

   @param[in] rule Pointer to a rule.

   @returns
   Pointer to a string if set, otherwise NULL.

   @attention
   The caller must not free the pointer.
 */
const char* fw_rule_get_in_interface(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's chain as set by @ref fw_rule_set_chain.

   @param[in] rule Pointer to a rule.

   @returns
   Pointer to a string if set, otherwise NULL.

   @attention
   The caller must not free the pointer.
 */
const char* fw_rule_get_chain(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's destination IP address as set by @ref
   fw_rule_set_destination.

   @param[in] rule Pointer to a rule.

   @returns
   Pointer to a string if set, otherwise NULL.

   @attention
   The caller must not free the pointer.
 */
const char* fw_rule_get_destination(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's destination IP address mask as set by @ref
   fw_rule_set_destination_mask.

   @param[in] rule Pointer to a rule.

   @returns
   Pointer to a string if set, otherwise NULL.

   @attention
   The caller must not free the pointer.
 */
const char* fw_rule_get_destination_mask(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's destination IPSet as set by @ref
   fw_rule_set_destination_ipset.

   @param[in] rule Pointer to a rule.

   @returns
   Pointer to a string if set, otherwise NULL.

   @attention
   The caller must not free the pointer.
 */
const char* fw_rule_get_destination_ipset(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's source IP address as set by @ref fw_rule_set_source.

   @param[in] rule Pointer to a rule.

   @returns
   Pointer to a string if set, otherwise NULL.

   @attention
   The caller must not free the pointer.
 */
const char* fw_rule_get_source_mac_address(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's source IP address as set by @ref fw_rule_set_source.

   @param[in] rule Pointer to a rule.

   @returns
   Pointer to a string if set, otherwise NULL.

   @attention
   The caller must not free the pointer.
 */
const char* fw_rule_get_out_interface(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's destination port as set by @ref fw_rule_set_destination_port.

   @param[in] rule Pointer to a rule.

   @returns
   The port number if set, otherwise @c 0.
 */
uint32_t fw_rule_get_destination_port(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's destination maximum port as set by @ref fw_rule_set_destination_port_range_max.

   @param[in] rule Pointer to a rule.

   @returns
   The maximum port number if set, otherwise @c 0.
 */
uint32_t fw_rule_get_destination_port_range_max(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's IP protocol as set by @ref fw_rule_set_protocol.

   @param[in] rule Pointer to a rule.

   @returns
   The protocol if set, otherwise @c 0.
 */
uint32_t fw_rule_get_protocol(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's DSCP value to match as set by @ref fw_rule_set_dscp.

   @param[in] rule Pointer to a rule.

   @returns
   The DSCP value to match if set, otherwise @c 0.
 */
uint32_t fw_rule_get_dscp(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's source port as set by @ref fw_rule_set_source_port.

   @param[in] rule Pointer to a rule.

   @returns
   The port number if set, otherwise @c 0.
 */
uint32_t fw_rule_get_source_port(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's source maximum port as set by @ref fw_rule_set_source_port_range_max.

   @param[in] rule Pointer to a rule.

   @returns
   The maximum port number if set, otherwise @c 0.
 */
uint32_t fw_rule_get_source_port_range_max(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's IPv4 version as set by @ref fw_rule_set_ipv4.

   @note
   This is the opposite of @ref fw_rule_get_ipv6.

   @param[in] rule Pointer to a rule.

   @returns
   @c true if set, otherwise @c false.
 */
bool fw_rule_get_ipv4(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's IPv6 version as set by @ref fw_rule_set_ipv6.

   @note
   This is the opposite of @ref fw_rule_get_ipv4.

   @param[in] rule Pointer to a rule.

   @returns
   @c true if set, otherwise @c false.
 */
bool fw_rule_get_ipv6(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's excluded flag for the ingress interface as set by @ref
   fw_rule_set_in_interface_excluded.

   @param[in] rule Pointer to a rule.

   @returns
   @c true if excluded, otherwise @c false.
 */
bool fw_rule_get_in_interface_excluded(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's excluded flag for the egress interface as set by @ref
   fw_rule_set_out_interface_excluded.

   @param[in] rule Pointer to a rule.

   @returns
   @c true if excluded, otherwise @c false.
 */
bool fw_rule_get_out_interface_excluded(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's excluded flag for the source IP address as set by @ref
   fw_rule_set_source_excluded.

   @param[in] rule Pointer to a rule.

   @returns
   @c true if excluded, otherwise @c false.
 */
bool fw_rule_get_source_excluded(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's excluded flag for the destination IP address as set by @ref
   fw_rule_set_destination_excluded.

   @param[in] rule Pointer to a rule.

   @returns
   @c true if excluded, otherwise @c false.
 */
bool fw_rule_get_destination_excluded(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's excluded flag for the source IPSet as set by @ref
   fw_rule_set_source_ipset_excluded.

   @param[in] rule Pointer to a rule.

   @returns
   @c true if excluded, otherwise @c false.
 */
bool fw_rule_get_source_ipset_excluded(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's excluded flag for the destination IPSet as set by @ref
   fw_rule_set_destination_ipset_excluded.

   @param[in] rule Pointer to a rule.

   @returns
   @c true if excluded, otherwise @c false.
 */
bool fw_rule_get_destination_ipset_excluded(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's excluded flag for the IP protocol as set by @ref
   fw_rule_set_protocol_excluded.

   @param[in] rule Pointer to a rule.

   @returns
   @c true if excluded, otherwise @c false.
 */
bool fw_rule_get_protocol_excluded(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's excluded flag for the source MAC address as set by @ref
   fw_rule_set_source_mac_excluded.

   @param[in] rule Pointer to a rule.

   @returns
   @c true if excluded, otherwise @c false.
 */
bool fw_rule_get_source_mac_excluded(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's excluded flag for the destination MAC address as set by @ref
   fw_rule_set_destination_mac_excluded.

   @param[in] rule Pointer to a rule.

   @returns
   @c true if excluded, otherwise @c false.
 */
bool fw_rule_get_destination_mac_excluded(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's excluded flag for the source port as set by @ref
   fw_rule_set_source_port_excluded.

   @param[in] rule Pointer to a rule.

   @returns
   @c true if excluded, otherwise @c false.
 */
bool fw_rule_get_source_port_excluded(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's excluded flag for the destination port as set by @ref
   fw_rule_set_destination_port_excluded.

   @param[in] rule Pointer to a rule.

   @returns
   @c true if excluded, otherwise @c false.
 */
bool fw_rule_get_destination_port_excluded(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's excluded flag for the DSCP value as set by @ref
   fw_rule_set_dscp_excluded.

   @param[in] rule Pointer to a rule.

   @returns
   @c true if excluded, otherwise @c false.
 */
bool fw_rule_get_dscp_excluded(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's jump target.

   @param[in] rule Pointer to a rule.

   @returns
   An enum value of @ref fw_rule_target_t on success, @ref FW_RULE_TARGET_LAST on failure.
 */
fw_rule_target_t fw_rule_get_target(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's policy as set by @ref fw_rule_set_target_policy.

   @param[in] rule Pointer to a rule.

   @returns
   An enum value of @ref fw_rule_policy_t on success, @ref FW_RULE_POLICY_LAST on failure.
 */
fw_rule_policy_t fw_rule_get_target_policy_option(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's minimum IP length as set by @ref fw_rule_set_ip_min_length.

   @param[in] rule Pointer to a rule.

   @returns
   The minimum IP length if set, otherwise @c 0.
 */
uint32_t fw_rule_get_ip_min_length(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's minimum IP length as set by @ref fw_rule_set_ip_max_length.

   @param[in] rule Pointer to a rule.

   @returns
   The minimum IP length if set, otherwise @c 0.
 */
uint32_t fw_rule_get_ip_max_length(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's ICMP type as set by @ref fw_rule_set_icmp_type.

   @param[in] rule Pointer to a rule.

   @returns
   The ICMP type if set, otherwise @c -1.
 */
int32_t fw_rule_get_icmp_type(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's ICMPv6 type as set by @ref fw_rule_set_icmpv6_type.

   @param[in] rule Pointer to a rule.

   @returns
   The ICMPv6 type if set, otherwise @c -1.
 */
int32_t fw_rule_get_icmpv6_type(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's ICMP code as set by @ref fw_rule_set_icmp_code.

   @param[in] rule Pointer to a rule.

   @returns
   The ICMP code if set, otherwise @c -1.
 */
int32_t fw_rule_get_icmp_code(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's ICMPv6 code as set by @ref fw_rule_set_icmpv6_code.

   @param[in] rule Pointer to a rule.

   @returns
   The ICMPv6 code if set, otherwise @c -1.
 */
int32_t fw_rule_get_icmpv6_code(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's connection tracking state to match as set by @ref
   fw_rule_set_conntrack_state.

   @param[in] rule Pointer to a rule.

   @returns
   Pointer to a string if set, otherwise NULL.

   @attention
   The caller must not free the pointer.
 */
const char* fw_rule_get_conntrack_state(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's connbytes minimum value to match as set by @ref
   fw_rule_set_connbytes_min.

   @param[in] rule Pointer to a rule.

   @returns
   The minimum connbytes value to match if set, otherwise @c 0.
 */
uint64_t fw_rule_get_connbytes_min(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's connbytes maximum value to match as set by @ref
   fw_rule_set_connbytes_max.

   @param[in] rule Pointer to a rule.

   @returns
   The maximum connbytes value to match if set, otherwise @c 0.
 */
uint64_t fw_rule_get_connbytes_max(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's connbytes parameter to match as set by @ref
   fw_rule_set_connbytes_param.

   @param[in] rule Pointer to a rule.

   @returns
   Pointer to a string if set, otherwise NULL.

   @attention
   The caller must not free the pointer.
 */
const char* fw_rule_get_connbytes_param(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's connbytes direction to match as set by @ref
   fw_rule_set_connbytes_direction.

   @param[in] rule Pointer to a rule.

   @returns
   Pointer to a string if set, otherwise NULL.

   @attention
   The caller must not free the pointer.
 */
const char* fw_rule_get_connbytes_direction(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's skb mark value to match as set by @ref fw_rule_set_skb_mark.

   @param[in] rule Pointer to a rule.

   @returns
   The skb mark value to match if set, otherwise @c 0.
 */
uint32_t fw_rule_get_skb_mark(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's skb mark mask value to match as set by @ref fw_rule_set_skb_mark_mask.

   @param[in] rule Pointer to a rule.

   @returns
   The skb mark mask value to match if set, otherwise @c 0.
 */
uint32_t fw_rule_get_skb_mark_mask(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's excluded flag for the skb mark value as set by @ref
   fw_rule_set_skb_mark_excluded.

   @param[in] rule Pointer to a rule.

   @returns
   @c true if excluded, otherwise @c false.
 */
bool fw_rule_get_skb_mark_excluded(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's conn mark value to match as set by @ref fw_rule_set_conn_mark.

   @param[in] rule Pointer to a rule.

   @returns
   The conn mark value to match if set, otherwise @c 0.
 */
uint32_t fw_rule_get_conn_mark(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's conn mark mask value to match as set by @ref fw_rule_set_conn_mark_mask.

   @param[in] rule Pointer to a rule.

   @returns
   The conn mark mask value to match if set, otherwise @c 0.
 */
uint32_t fw_rule_get_conn_mark_mask(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's excluded flag for the conn mark value as set by @ref
   fw_rule_set_conn_mark_excluded.

   @param[in] rule Pointer to a rule.

   @returns
   @c true if excluded, otherwise @c false.
 */
bool fw_rule_get_conn_mark_excluded(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's mark target value as set by @ref fw_rule_set_target_mark.

   @param[in] rule Pointer to a rule.
   @param[out] mark Pointer to the target mark. @c 0
   when ignored.
   @param[out] mask Pointer to the mask. @c 0
   when ignored.

   @returns
   @c 0 on success, @c -1 on failure. On success, @c mark and @c mask are valid.
 */
int fw_rule_get_target_mark_options(const fw_rule_t* const rule, uint32_t* mark, uint32_t* mask);

/**
   @ingroup rule
   @brief
   Get a rule's classify target value as set by @ref fw_rule_set_target_classify.

   @param[in] rule Pointer to a rule.
   @param[out] class Pointer to the target mark. @c 0
   when ignored.

   @returns
   @c 0 on success, @c -1 on failure. On success, @c class is valid.
 */
int fw_rule_get_target_classify_option(const fw_rule_t* const rule, uint32_t* class);

/**
   @ingroup rule
   @brief
   Get a rule's DSCP target value as set by @ref fw_rule_set_target_dscp.

   @param[in] rule Pointer to a rule.

   @returns
   The DSCP value if set, otherwise @c 0.
 */
uint32_t fw_rule_get_target_dscp_option(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Obsolete.
 */
uint32_t fw_rule_get_target_queue_option(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's skb priority target value as set by @ref fw_rule_set_target_pbit.

   @param[in] rule Pointer to a rule.

   @returns
   The skb's priority value if set, otherwise @c 0.
 */
uint32_t fw_rule_get_target_pbit_option(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's DNAT target values as set by @ref fw_rule_set_target_dnat.

   @param[in] rule Pointer to a rule.
   @param[out] ip Double pointer to the destination IP address that is used to
   DNAT.
   @param[out] min_port Pointer to the minimum port in the port range. @c 0
   when ignored.
   @param[out] max_port Pointer to the maximum port in the port range. @c 0
   when ignored.

   @returns
   @c 0 on success, -1 on failure. On success, @c ip, @c min_port and @c max_port are valid.
 */
int fw_rule_get_target_dnat_options(const fw_rule_t* const rule, const char** ip, uint32_t* min_port, uint32_t* max_port);

/**
   @ingroup rule
   @brief
   Get a rule's SNAT target values as set by @ref fw_rule_set_target_snat.

   @param[in] rule Pointer to a rule.
   @param[out] ip Double pointer to the source IP address that is used to SNAT.
   @param[out] min_port Pointer to the minimum port in the port range. @c 0
   when ignored.
   @param[out] max_port Pointer to the maximum port in the port range. @c 0
   when ignored.

   @returns
   @c 0 on success, -1 on failure. On success, @c ip, @c min_port and @c max_port are valid.
 */
int fw_rule_get_target_snat_options(const fw_rule_t* const rule, const char** ip, uint32_t* min_port, uint32_t* max_port);

/**
   @ingroup rule
   @brief
   Get a rule's target chain to jump to after a match as set by @ref
   fw_rule_set_target_chain.

   @param[in] rule Pointer to a rule.

   @returns
   Pointer to a string if set, otherwise NULL.

   @attention
   The caller must not free the pointer.
 */
const char* fw_rule_get_target_chain_option(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Get a rule's NFQUEUE target values as set by @ref fw_rule_set_target_nfqueue_options.

   @param[in] rule Pointer to a rule.
   @param[out] qnum Pointer to the queue number to use.
   @param[out] qtotal Pointer to the Maximum number of queues.
   @param[out] flags Pointer to a bitmask of @ref fw_rule_nfqueue_flag.

   @returns
   @c 0 on success, -1 on failure. On success, @c qnum, @c qtotal and @c flags are valid.
 */
int fw_rule_get_target_nfqueue_options(const fw_rule_t* const rule, uint32_t* qnum, uint32_t* qtotal, uint32_t* flags);

/**
   @ingroup rule
   @brief
   Get a rule's NFLOG target values as set by @ref fw_rule_set_target_nflog_options.

   @param[in] rule Pointer to a rule.
   @param[out] gnum Pointer to the group number to use.
   @param[out] len Pointer to the length of packets to copy.
   @param[out] threshold Pointer to the threshold of how many packets to queue.
   @param[out] flags Pointer to the flags to add extra details to the nflog line
   @param[out] prefix Pointer to the string that is prepended to the nflog line

   @returns
   @c 0 on success, -1 on failure. On success, @c qnum, @c qtotal and @c flags are valid.
 */
int fw_rule_get_target_nflog_options(const fw_rule_t* const rule, uint32_t* gnum, uint32_t* len, uint32_t* threshold, uint32_t* flags, const char** prefix);

/**
   @ingroup rule
   @brief
   Get a rule's LOG target values as set by @ref fw_rule_set_target_log_options.

   @param[in] rule Pointer to a rule
   @param[out] level Pointer to the log level for this rule
   @param[out] logflags Pointer to the flags to add extra details to the log line
   @param[out] prefix Pointer to the string that is prepended to the log line

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_get_target_log_options(const fw_rule_t* const rule, uint8_t* level, uint8_t* logflags, const char** prefix);

/**
   @ingroup rule
   @brief
   Set a pattern to match with. E.g.: "GET /index.html".

   @par Supported features:
   - No feature is set.
   - @ref FW_FEATURE_STRING

   @param[in] rule Pointer to a rule
   @param[in] filter_string Pointer to the string that is the pattern to match with

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_filter_string(fw_rule_t* const rule, const char* filter_string);

/**
   @ingroup rule
   @brief
   Get a rule's pattern value as set by @ref fw_rule_set_filter_string.

   @param[in] rule Pointer to a rule

   @returns
   Pointer to a string if set, otherwise NULL.

   @attention
   The caller must not free the pointer.
 */
const char* fw_rule_get_filter_string(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Set the weekdays to match with: day[,day]. E.g.: "Mo,Tu,We,Th,Fr,Sa,Su" or "Mon,Tue,Thu,Fri,Sat,Sun" or "1,2,3,4,5,6,7".

   @par Supported features:
   - No feature is set.
   - @ref FW_FEATURE_TIME

   @param[in] rule Pointer to a rule
   @param[in] weekdays Pointer to the csv string that are the weekdays to match with

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_filter_weekdays(fw_rule_t* const rule, const char* weekdays);

/**
   @ingroup rule
   @brief
   Get a rule's weekday values as set by @ref fw_rule_set_filter_weekdays.

   @param[in] rule Pointer to a rule

   @returns
   Pointer to a string if set, otherwise NULL.

   @attention
   The caller must not free the pointer.
 */
const char* fw_rule_get_filter_weekdays(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Set the start of time range to match with: hh:mm[:ss]. Leading zeroes are allowed (e.g. "06:03") and interpreted as base-10.

   @par Supported features:
   - No feature is set.
   - @ref FW_FEATURE_TIME

   @param[in] rule Pointer to a rule
   @param[in] weekdays Pointer to the csv string that are the weekdays to match with

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_filter_start_time(fw_rule_t* const rule, const char* start_time);

/**
   @ingroup rule
   @brief
   Get a rule's start time value as set by @ref fw_rule_set_filter_start_time.

   @param[in] rule Pointer to a rule

   @returns
   Pointer to a string if set, otherwise NULL.

   @attention
   The caller must not free the pointer.
 */
const char* fw_rule_get_filter_start_time(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Set the end of time range to match with: hh:mm[:ss]. Leading zeroes are allowed (e.g. "06:03") and interpreted as base-10.

   @par Supported features:
   - No feature is set.
   - @ref FW_FEATURE_TIME

   @param[in] rule Pointer to a rule
   @param[in] weekdays Pointer to the csv string that are the weekdays to match with

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_filter_end_time(fw_rule_t* const rule, const char* end_time);

/**
   @ingroup rule
   @brief
   Get a rule's end time value as set by @ref fw_rule_set_filter_end_time.

   @param[in] rule Pointer to a rule

   @returns
   Pointer to a string if set, otherwise NULL.

   @attention
   The caller must not free the pointer.
 */
const char* fw_rule_get_filter_end_time(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Set an output bridge port value to match.

   Takes as input the name of a bridge port via which a packet is going to be sent (for packets
   entering the FORWARD, OUTPUT and POSTROUTING chains). If the interface name ends in a "+", then
   any interface which begins with this name will match.

   @par Supported features:
   - No feature is set.
   - @ref FW_FEATURE_PHYSDEV_OUT

   @param[in] rule Pointer to a rule.
   @param[in] physdev_out output bridge port.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_physdev_out(fw_rule_t* const rule, const char* physdev_out);

/**
   @ingroup rule
   @brief
   Get a rule's output bridge port value to match as set by @ref fw_rule_set_physdev_out.

   @param[in] rule Pointer to a rule.

   @returns
   The output bridge port value to match if set, otherwise @c NULL.
 */
const char* fw_rule_get_physdev_out(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Exclude an output bridge port value (physdev-out). This is a NOT operator for the output bridge
   port value set in @ref fw_rule_set_physdev_out.

   @par Supported features:
   - No feature is set.
   - @ref FW_FEATURE_PHYSDEV_OUT

   @param[in] rule Pointer to a rule.
   @param[in] is_physdev_out_excluded @c true if the output bridge port value must be excluded,
   otherwise @c false.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_physdev_out_excluded(fw_rule_t* const rule, const bool is_physdev_out_excluded);

/**
   @ingroup rule
   @brief
   Get a rule's excluded flag for the physdev-out value as set by @ref
   fw_rule_set_physdev_out_excluded.

   @param[in] rule Pointer to a rule.

   @returns
   @c true if excluded, otherwise @c false.
 */
bool fw_rule_get_physdev_out_excluded(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Set an input bridge port value to match.

   Takes as input the name of a bridge port via which a packet is received (only for packets
   entering the INPUT, FORWARD and PREROUTING chains). If the interface name ends in a "+", then any
   interface which begins with this name will match.

   @par Supported features:
   - No feature is set.
   - @ref FW_FEATURE_PHYSDEV_IN

   @param[in] rule Pointer to a rule.
   @param[in] physdev_in input bridge port.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_physdev_in(fw_rule_t* const rule, const char* physdev_in);

/**
   @ingroup rule
   @brief
   Get a rule's input bridge port value to match as set by @ref fw_rule_set_physdev_in.

   @param[in] rule Pointer to a rule.

   @returns
   The input bridge port value to match if set, otherwise @c NULL.
 */
const char* fw_rule_get_physdev_in(const fw_rule_t* const rule);

/**
   @ingroup rule
   @brief
   Exclude an input bridge port value (physdev-in). This is a NOT operator for the input bridge
   port value set in @ref fw_rule_set_physdev_in.

   @par Supported features:
   - No feature is set.
   - @ref FW_FEATURE_PHYSDEV_IN

   @param[in] rule Pointer to a rule.
   @param[in] is_physdev_in_excluded @c true if the input bridge port value must be excluded,
   otherwise @c false.

   @returns @c 0 on success, @c -1 on failure.
 */
int fw_rule_set_physdev_in_excluded(fw_rule_t* const rule, const bool is_physdev_in_excluded);

/**
   @ingroup rule
   @brief
   Get a rule's excluded flag for the physdev-in value as set by @ref
   fw_rule_set_physdev_in_excluded.

   @param[in] rule Pointer to a rule.

   @returns
   @c true if excluded, otherwise @c false.
 */
bool fw_rule_get_physdev_in_excluded(const fw_rule_t* const rule);



#ifdef __cplusplus
}
#endif

#endif   // __FW_RULE_H
