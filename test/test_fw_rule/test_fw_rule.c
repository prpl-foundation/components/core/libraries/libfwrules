/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <unistd.h>
#include <cmocka.h>
#include <fcntl.h>

#include <fwrules/fw_folder.h>
#include <fwrules/fw_rule.h>

#include "fw_rule_priv.h"
#include "test_fw_rule.h"

#ifndef UNUSED
#define UNUSED __attribute__((unused))
#endif

void test_fw_rule_new_delete(UNUSED void** state) {
    int retval = -1;
    fw_rule_t* rule = NULL;

    retval = fw_rule_new(NULL);
    assert_int_equal(retval, -1);

    retval = fw_rule_new(&rule);
    assert_int_equal(retval, 0);
    assert_non_null(rule);

    assert_false(fw_rule_is_enabled(NULL));
    assert_false(fw_rule_is_enabled(rule));

    retval = fw_rule_new(&rule);
    assert_int_equal(retval, -1);
    assert_non_null(rule);

    retval = fw_rule_delete(NULL);
    assert_int_equal(retval, -1);

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, -1);
    assert_null(rule);
}

void test_fw_rule_set_feature_mark(UNUSED void** state) {
    int retval = -1;
    fw_rule_t* rule = NULL;
    int feature_marks = 0;

    retval = fw_rule_new(&rule);
    assert_int_equal(retval, 0);
    assert_non_null(rule);

    retval = fw_rule_set_feature_mark(NULL, FW_FEATURE_TARGET);
    assert_int_equal(retval, -1);

    retval = fw_rule_set_feature_mark(rule, FW_FEATURE_LAST);
    assert_int_equal(retval, -1);
    feature_marks = fw_rule_get_feature_marks(rule);
    assert_int_equal(feature_marks, 0);

    retval = fw_rule_set_feature_mark(rule, FW_FEATURE_TARGET);
    assert_int_equal(retval, 0);
    retval = fw_rule_set_feature_mark(rule, FW_FEATURE_PROTOCOL);
    assert_int_equal(retval, 0);

    feature_marks = fw_rule_get_feature_marks(rule);
    assert_true(fw_feature_contains(feature_marks, FW_FEATURE_TARGET));
    assert_true(fw_feature_contains(feature_marks, FW_FEATURE_PROTOCOL));
    assert_false(fw_feature_contains(feature_marks, FW_FEATURE_SOURCE_PORT));

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

void test_fw_rule_set_enabled(UNUSED void** state) {
    int retval = -1;
    fw_rule_t* rule = NULL;

    retval = fw_rule_new(&rule);
    assert_int_equal(retval, 0);
    assert_non_null(rule);

    assert_false(fw_rule_is_enabled(NULL));
    assert_false(fw_rule_is_enabled(rule));

    retval = fw_rule_set_enabled(NULL, true);
    assert_int_equal(retval, -1);

    retval = fw_rule_set_enabled(NULL, false);
    assert_int_equal(retval, -1);

    retval = fw_rule_set_enabled(rule, true);
    assert_int_equal(retval, 0);
    assert_true(fw_rule_is_enabled(rule));

    retval = fw_rule_set_enabled(rule, false);
    assert_int_equal(retval, 0);
    assert_false(fw_rule_is_enabled(rule));

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

void test_fw_rule_set_flag(UNUSED void** state) {
    int retval = -1;
    fw_rule_t* rule = NULL;

    retval = fw_rule_new(&rule);
    assert_int_equal(retval, 0);
    assert_non_null(rule);

    assert_true(fw_rule_get_flag(rule) == FW_RULE_FLAG_NEW);
    assert_true(fw_rule_get_flag(NULL) == FW_RULE_FLAG_LAST);

    retval = fw_rule_set_flag(NULL, FW_RULE_FLAG_LAST);
    assert_int_equal(retval, -1);
    retval = fw_rule_set_flag(NULL, FW_RULE_FLAG_MODIFIED);
    assert_int_equal(retval, -1);

    retval = fw_rule_set_flag(rule, FW_RULE_FLAG_MODIFIED);
    assert_int_equal(retval, 0);
    assert_true(fw_rule_get_flag(rule) == FW_RULE_FLAG_MODIFIED);

    retval = fw_rule_set_flag(rule, FW_RULE_FLAG_DELETED);
    assert_int_equal(retval, 0);
    assert_true(fw_rule_get_flag(rule) == FW_RULE_FLAG_DELETED);

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

void test_fw_rule_current_feature(UNUSED void** state) {
    int retval = -1;
    fw_rule_t* rule = NULL;

    retval = fw_rule_new(&rule);
    assert_int_equal(retval, 0);
    assert_non_null(rule);

    assert_true(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST);

    retval = fw_rule_set_current_feature(NULL, FW_FEATURE_LAST);
    assert_int_equal(retval, -1);
    retval = fw_rule_set_current_feature(rule, FW_FEATURE_LAST);
    assert_int_equal(retval, 0);
    assert_true(fw_rule_get_current_feature(rule) == FW_FEATURE_LAST);

    retval = fw_rule_set_current_feature(rule, FW_FEATURE_DSCP);
    assert_int_equal(retval, 0);
    assert_true(fw_rule_get_current_feature(rule) == FW_FEATURE_DSCP);

    retval = fw_rule_set_current_feature(rule, FW_FEATURE_WHITELIST);
    assert_int_equal(retval, 0);
    assert_true(fw_rule_get_current_feature(rule) == FW_FEATURE_WHITELIST);

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

void test_fw_rule_set_get_ipv4_ipv6(UNUSED void** state) {
    int retval = -1;
    fw_rule_t* rule = NULL;
    bool ipv6 = false;
    bool ipv4 = false;

    retval = fw_rule_new(&rule);
    assert_int_equal(retval, 0);
    assert_non_null(rule);

    ipv6 = fw_rule_get_ipv6(NULL);
    assert_false(ipv6);

    ipv6 = fw_rule_get_ipv6(rule);
    assert_false(ipv6);

    retval = fw_rule_set_ipv6(NULL, true);
    assert_int_equal(retval, -1);
    retval = fw_rule_set_ipv6(NULL, false);
    assert_int_equal(retval, -1);

    retval = fw_rule_set_ipv6(rule, true);
    assert_int_equal(retval, 0);
    ipv6 = fw_rule_get_ipv6(rule);
    assert_true(ipv6);

    retval = fw_rule_set_ipv6(rule, false);
    assert_int_equal(retval, 0);
    ipv6 = fw_rule_get_ipv6(rule);
    assert_false(ipv6);

    ipv4 = fw_rule_get_ipv4(NULL);
    assert_true(ipv4);

    ipv4 = fw_rule_get_ipv4(rule);
    assert_true(ipv4);

    retval = fw_rule_set_ipv4(NULL, true);
    assert_int_equal(retval, -1);
    retval = fw_rule_set_ipv4(NULL, false);
    assert_int_equal(retval, -1);

    retval = fw_rule_set_ipv4(rule, true);
    assert_int_equal(retval, 0);
    ipv4 = fw_rule_get_ipv4(rule);
    assert_true(ipv4);

    retval = fw_rule_set_ipv4(rule, false);
    assert_int_equal(retval, 0);
    ipv4 = fw_rule_get_ipv4(rule);
    assert_false(ipv4);

    retval = fw_rule_set_ipv4(rule, true);
    assert_int_equal(retval, 0);
    ipv6 = fw_rule_get_ipv6(rule);
    assert_false(ipv6);

    retval = fw_rule_set_ipv6(rule, true);
    assert_int_equal(retval, 0);
    ipv4 = fw_rule_get_ipv4(rule);
    assert_false(ipv4);

    retval = fw_rule_set_ipv4(rule, false);
    assert_int_equal(retval, 0);
    ipv6 = fw_rule_get_ipv6(rule);
    assert_true(ipv6);

    retval = fw_rule_set_ipv6(rule, false);
    assert_int_equal(retval, 0);
    ipv4 = fw_rule_get_ipv4(rule);
    assert_true(ipv4);

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

void test_fw_rule_set_get_table(UNUSED void** state) {
    int retval = -1;
    fw_rule_t* rule = NULL;
    const char* table = NULL;

    retval = fw_rule_new(&rule);
    assert_int_equal(retval, 0);
    assert_non_null(rule);

    table = fw_rule_get_table(NULL);
    assert_null(table);

    table = fw_rule_get_table(rule);
    assert_string_equal(table, "");

    retval = fw_rule_set_table(NULL, NULL);
    assert_int_equal(retval, -1);

    retval = fw_rule_set_table(rule, NULL);
    assert_int_equal(retval, -1);

    retval = fw_rule_set_table(NULL, "");
    assert_int_equal(retval, -1);

    retval = fw_rule_set_table(rule, "mangle");
    assert_int_equal(retval, 0);
    table = fw_rule_get_table(rule);
    assert_string_equal(table, "mangle");

    retval = fw_rule_set_table(rule, "filter");
    assert_int_equal(retval, 0);
    table = fw_rule_get_table(rule);
    assert_string_equal(table, "filter");

    retval = fw_rule_set_table(rule, "");
    assert_int_equal(retval, 0);
    table = fw_rule_get_table(rule);
    assert_string_equal(table, "");

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

static void test_fw_rule_set_get_string(
    int (* set)(fw_rule_t*, const char*),
    const char* (*get)(const fw_rule_t* const),
    fw_feature_t features[], int n_features,
    char* strings[], int n_strings) {

    int retval = -1;
    fw_rule_t* rule = NULL;
    int i;
    const char* value = NULL;

    assert_int_equal(n_features, n_strings);

    retval = fw_rule_new(&rule);
    assert_int_equal(retval, 0);
    assert_non_null(rule);

    assert_int_equal(set(NULL, NULL), -1);
    assert_int_equal(set(NULL, "fault"), -1);
    assert_int_equal(set(rule, NULL), -1);
    assert_null(get(NULL));

    //The good ones.
    for(i = 0; i < n_features - 1; i++) {
        retval = fw_rule_set_current_feature(rule, features[i]);
        assert_int_equal(retval, 0);
        retval = set(rule, strings[i]);
        assert_int_equal(retval, 0);
        value = get(rule);
        assert_non_null(value);
        assert_string_equal(value, strings[i]);
    }

    //The bad one.
    retval = fw_rule_set_current_feature(rule, features[i]);
    assert_int_equal(retval, 0);
    retval = set(rule, strings[i]);
    assert_int_equal(retval, -1);
    value = get(rule);
    assert_non_null(value);
    assert_string_equal(value, strings[i - 1]);

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

static void test_fw_rule_set_get_uint32_t(
    int (* set)(fw_rule_t*, const uint32_t),
    uint32_t (* get)(const fw_rule_t* const),
    fw_feature_t features[], int n_features,
    uint32_t* numbers, int n_numbers) {

    int retval = -1;
    fw_rule_t* rule = NULL;
    int i;
    uint32_t value = 0;

    assert_int_equal(n_features, n_numbers);

    retval = fw_rule_new(&rule);
    assert_int_equal(retval, 0);
    assert_non_null(rule);

    assert_int_equal(set(NULL, 0), -1);
    assert_int_equal(get(NULL), 0);

    //The good ones.
    for(i = 0; i < n_features - 1; i++) {
        retval = fw_rule_set_current_feature(rule, features[i]);
        assert_int_equal(retval, 0);
        retval = set(rule, numbers[i]);
        assert_int_equal(retval, 0);
        value = get(rule);
        assert_int_equal(value, numbers[i]);
    }

    //The bad one.
    retval = fw_rule_set_current_feature(rule, features[i]);
    assert_int_equal(retval, 0);
    retval = set(rule, numbers[i]);
    assert_int_equal(retval, -1);
    value = get(rule);
    assert_int_equal(value, numbers[i - 1]);

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

static void test_fw_rule_set_get_int32_t(
    int (* set)(fw_rule_t*, const int32_t),
    int32_t (* get)(const fw_rule_t* const),
    fw_feature_t features[], int n_features,
    int32_t* numbers, int n_numbers, bool ipv6) {

    int retval = -1;
    fw_rule_t* rule = NULL;
    int i;
    int32_t value = 0;

    assert_int_equal(n_features, n_numbers);

    retval = fw_rule_new(&rule);
    assert_int_equal(retval, 0);
    assert_non_null(rule);

    if(ipv6) {
        retval = fw_rule_set_ipv6(rule, true);
        assert_int_equal(retval, 0);
    }

    assert_int_equal(set(NULL, 0), -1);
    assert_int_equal(get(NULL), -1);

    //The good ones.
    for(i = 0; i < n_features - 1; i++) {
        retval = fw_rule_set_current_feature(rule, features[i]);
        assert_int_equal(retval, 0);
        retval = set(rule, numbers[i]);
        assert_int_equal(retval, 0);
        value = get(rule);
        assert_int_equal(value, numbers[i]);
    }

    //The bad one.
    retval = fw_rule_set_current_feature(rule, features[i]);
    assert_int_equal(retval, 0);
    retval = set(rule, numbers[i]);
    assert_int_equal(retval, -1);
    value = get(rule);
    assert_int_equal(value, numbers[i - 1]);

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

static void test_fw_rule_set_get_uint64_t(
    int (* set)(fw_rule_t*, const uint64_t),
    uint64_t (* get)(const fw_rule_t* const),
    fw_feature_t features[], int n_features,
    uint64_t* numbers, int n_numbers) {

    int retval = -1;
    fw_rule_t* rule = NULL;
    int i;
    uint64_t value = 0;

    assert_int_equal(n_features, n_numbers);

    retval = fw_rule_new(&rule);
    assert_int_equal(retval, 0);
    assert_non_null(rule);

    assert_int_equal(set(NULL, 0), -1);
    assert_int_equal(get(NULL), 0);

    //The good ones.
    for(i = 0; i < n_features - 1; i++) {
        retval = fw_rule_set_current_feature(rule, features[i]);
        assert_int_equal(retval, 0);
        retval = set(rule, numbers[i]);
        assert_int_equal(retval, 0);
        value = get(rule);
        assert_int_equal(value, numbers[i]);
    }

    //The bad one.
    retval = fw_rule_set_current_feature(rule, features[i]);
    assert_int_equal(retval, 0);
    retval = set(rule, numbers[i]);
    assert_int_equal(retval, -1);
    value = get(rule);
    assert_int_equal(value, numbers[i - 1]);

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

static void test_fw_rule_set_get_bool(
    int (* set)(fw_rule_t*, const bool),
    bool (* get)(const fw_rule_t* const),
    fw_feature_t features[], int n_features,
    bool* bools, int n_bools) {

    int retval = -1;
    fw_rule_t* rule = NULL;
    int i;
    bool value = 0;

    assert_int_equal(n_features, n_bools);

    assert_int_equal(set(NULL, false), -1);
    assert_int_equal(set(NULL, true), -1);
    assert_int_equal(get(NULL), false);

    retval = fw_rule_new(&rule);
    assert_int_equal(retval, 0);
    assert_non_null(rule);

    //The good ones.
    for(i = 0; i < n_features - 1; i++) {
        retval = fw_rule_set_current_feature(rule, features[i]);
        assert_int_equal(retval, 0);
        retval = set(rule, bools[i]);
        assert_int_equal(retval, 0);
        value = get(rule);
        assert_true(value == bools[i]);
    }

    //The bad one.
    retval = fw_rule_set_current_feature(rule, features[i]);
    assert_int_equal(retval, 0);
    retval = set(rule, bools[i]);
    assert_int_equal(retval, -1);
    value = get(rule);
    assert_true(value == bools[i - 1]);

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

void test_fw_rule_set_get_source(UNUSED void** state) {
    fw_feature_t features[] = { FW_FEATURE_LAST, FW_FEATURE_SOURCE, FW_FEATURE_WHITELIST,
        FW_FEATURE_SPOOFING, FW_FEATURE_DSCP };
    char* strings[] = { "192.168.120.10", "192.168.120.11", "192.168.120.12",
        "192.168.120.13", "192.168.120.14" };

    test_fw_rule_set_get_string(fw_rule_set_source,
                                fw_rule_get_source, features, sizeof(features) / sizeof(features[0]),
                                strings, sizeof(strings) / sizeof(strings[0]));
}

void test_fw_rule_set_get_source_mask(UNUSED void** state) {
    fw_feature_t features[] = { FW_FEATURE_LAST, FW_FEATURE_SOURCE, FW_FEATURE_WHITELIST,
        FW_FEATURE_SPOOFING, FW_FEATURE_DSCP };
    char* strings[] = { "255.255.255.0", "255.0.0.0", "255.255.255.254",
        "255.255.0.0", "255.255.0.0" };

    test_fw_rule_set_get_string(fw_rule_set_source_mask,
                                fw_rule_get_source_mask, features, sizeof(features) / sizeof(features[0]),
                                strings, sizeof(strings) / sizeof(strings[0]));
}

void test_fw_rule_set_get_source_ipset(UNUSED void** state) {
    fw_feature_t features[] = { FW_FEATURE_LAST, FW_FEATURE_SOURCE_IPSET, FW_FEATURE_SOURCE};
    char* strings[] = { "set1", "set2", "set3"};

    test_fw_rule_set_get_string(fw_rule_set_source_ipset,
                                fw_rule_get_source_ipset, features, sizeof(features) / sizeof(features[0]),
                                strings, sizeof(strings) / sizeof(strings[0]));
}

void test_fw_rule_set_get_in_interface(UNUSED void** state) {
    fw_feature_t features[] = { FW_FEATURE_LAST, FW_FEATURE_WHITELIST, FW_FEATURE_POLICY, FW_FEATURE_SPOOFING, FW_FEATURE_ISOLATION, FW_FEATURE_IN_INTERFACE, FW_FEATURE_DSCP };
    char* strings[] = { "eth0", "atm_data", "wwan0", "ppp_vvdata", "eth4.1", "bridge", "eth3.1" };

    test_fw_rule_set_get_string(fw_rule_set_in_interface,
                                fw_rule_get_in_interface, features, sizeof(features) / sizeof(features[0]),
                                strings, sizeof(strings) / sizeof(strings[0]));
}

void test_fw_rule_set_get_chain(UNUSED void** state) {
    fw_feature_t features[] = { FW_FEATURE_LAST, FW_FEATURE_IN_INTERFACE, FW_FEATURE_DSCP };
    char* strings[] = { "prerouting", "postrouting", "postrouting" };

    test_fw_rule_set_get_string(fw_rule_set_chain,
                                fw_rule_get_chain, features, sizeof(features) / sizeof(features[0]),
                                strings, sizeof(strings) / sizeof(strings[0]));
}

void test_fw_rule_set_get_destination(UNUSED void** state) {
    fw_feature_t features[] = { FW_FEATURE_LAST, FW_FEATURE_DESTINATION, FW_FEATURE_SPOOFING,
        FW_FEATURE_ISOLATION, FW_FEATURE_DSCP };
    char* strings[] = { "192.168.120.10", "192.168.120.11", "192.168.120.12",
        "192.168.120.13", "192.168.120.14" };

    test_fw_rule_set_get_string(fw_rule_set_destination,
                                fw_rule_get_destination, features, sizeof(features) / sizeof(features[0]),
                                strings, sizeof(strings) / sizeof(strings[0]));
}

void test_fw_rule_set_get_destination_mask(UNUSED void** state) {
    fw_feature_t features[] = { FW_FEATURE_LAST, FW_FEATURE_DESTINATION, FW_FEATURE_SPOOFING,
        FW_FEATURE_ISOLATION, FW_FEATURE_DSCP };
    char* strings[] = { "255.255.255.0", "255.0.0.0", "255.255.255.254",
        "255.255.0.0", "255.255.0.0" };

    test_fw_rule_set_get_string(fw_rule_set_destination_mask,
                                fw_rule_get_destination_mask, features, sizeof(features) / sizeof(features[0]),
                                strings, sizeof(strings) / sizeof(strings[0]));
}

void test_fw_rule_set_get_destination_ipset(UNUSED void** state) {
    fw_feature_t features[] = { FW_FEATURE_LAST, FW_FEATURE_DESTINATION_IPSET, FW_FEATURE_DESTINATION};
    char* strings[] = { "set1", "set2", "set3"};

    test_fw_rule_set_get_string(fw_rule_set_destination_ipset,
                                fw_rule_get_destination_ipset, features, sizeof(features) / sizeof(features[0]),
                                strings, sizeof(strings) / sizeof(strings[0]));
}

void test_fw_rule_set_get_destination_port(UNUSED void** state) {
    fw_feature_t features[] = { FW_FEATURE_LAST, FW_FEATURE_DESTINATION_PORT, FW_FEATURE_DNAT, FW_FEATURE_TARGET };
    uint32_t numbers[] = { 80, 22, 23, 5000};

    test_fw_rule_set_get_uint32_t(fw_rule_set_destination_port,
                                  fw_rule_get_destination_port, features, sizeof(features) / sizeof(features[0]),
                                  numbers, sizeof(numbers) / sizeof(numbers[0]));
}

void test_fw_rule_set_get_destination_port_range(UNUSED void** state) {
    fw_feature_t features[] = { FW_FEATURE_LAST, FW_FEATURE_DESTINATION_PORT, FW_FEATURE_DNAT, FW_FEATURE_TARGET };
    uint32_t numbers[] = { 80, 22, 23, 5000};

    test_fw_rule_set_get_uint32_t(fw_rule_set_destination_port_range_max,
                                  fw_rule_get_destination_port_range_max, features, sizeof(features) / sizeof(features[0]),
                                  numbers, sizeof(numbers) / sizeof(numbers[0]));
}

void test_fw_rule_set_get_source_port(UNUSED void** state) {
    fw_feature_t features[] = { FW_FEATURE_LAST, FW_FEATURE_SOURCE_PORT, FW_FEATURE_SNAT, FW_FEATURE_TARGET };
    uint32_t numbers[] = { 80, 22, 23, 5000};

    test_fw_rule_set_get_uint32_t(fw_rule_set_source_port,
                                  fw_rule_get_source_port, features, sizeof(features) / sizeof(features[0]),
                                  numbers, sizeof(numbers) / sizeof(numbers[0]));
}

void test_fw_rule_set_get_source_port_range(UNUSED void** state) {
    fw_feature_t features[] = { FW_FEATURE_LAST, FW_FEATURE_SOURCE_PORT, FW_FEATURE_SNAT, FW_FEATURE_TARGET };
    uint32_t numbers[] = { 81, 23, 24, 6000};

    test_fw_rule_set_get_uint32_t(fw_rule_set_source_port_range_max,
                                  fw_rule_get_source_port_range_max, features, sizeof(features) / sizeof(features[0]),
                                  numbers, sizeof(numbers) / sizeof(numbers[0]));
}

void test_fw_rule_set_get_protocol(UNUSED void** state) {
    fw_feature_t features[] = { FW_FEATURE_LAST, FW_FEATURE_PROTOCOL, FW_FEATURE_DNAT};
    uint32_t numbers[] = { 1, 2, 300};

    test_fw_rule_set_get_uint32_t(fw_rule_set_protocol,
                                  fw_rule_get_protocol, features, sizeof(features) / sizeof(features[0]),
                                  numbers, sizeof(numbers) / sizeof(numbers[0]));
}

void test_fw_rule_set_get_source_mac_address(UNUSED void** state) {
    fw_feature_t features[] = { FW_FEATURE_LAST, FW_FEATURE_SOURCE_MAC, FW_FEATURE_SPOOFING, FW_FEATURE_WHITELIST, FW_FEATURE_DSCP };
    char* strings[] = { "ab:12:cd:34:55", "ab:12:cd:34:56", "ab:12:cd:34:57", "ab:12:cd:34:60", "ab:12:cd:34:60" };

    test_fw_rule_set_get_string(fw_rule_set_source_mac_address,
                                fw_rule_get_source_mac_address, features, sizeof(features) / sizeof(features[0]),
                                strings, sizeof(strings) / sizeof(strings[0]));
}

void test_fw_rule_set_get_dscp(UNUSED void** state) {
    fw_feature_t features[] = { FW_FEATURE_LAST, FW_FEATURE_DSCP, FW_FEATURE_DNAT};
    uint32_t numbers[] = { 40, 48, 30};

    test_fw_rule_set_get_uint32_t(fw_rule_set_dscp,
                                  fw_rule_get_dscp, features, sizeof(features) / sizeof(features[0]),
                                  numbers, sizeof(numbers) / sizeof(numbers[0]));
}

void test_fw_rule_set_get_out_interface(UNUSED void** state) {
    fw_feature_t features[] = { FW_FEATURE_LAST, FW_FEATURE_WHITELIST, FW_FEATURE_POLICY, FW_FEATURE_SPOOFING, FW_FEATURE_ISOLATION, FW_FEATURE_OUT_INTERFACE, FW_FEATURE_DSCP };
    char* strings[] = { "eth1", "ppp_adata", "wwan0", "atm_data", "eth4.1", "bridge", "eth3.1" };

    test_fw_rule_set_get_string(fw_rule_set_out_interface,
                                fw_rule_get_out_interface, features, sizeof(features) / sizeof(features[0]),
                                strings, sizeof(strings) / sizeof(strings[0]));

}

void test_fw_rule_set_get_in_interface_excluded(UNUSED void** state) {
    fw_feature_t features[] = { FW_FEATURE_LAST, FW_FEATURE_SPOOFING, FW_FEATURE_TARGET };
    bool bools[] = { true, false, true };

    test_fw_rule_set_get_bool(fw_rule_set_in_interface_excluded,
                              fw_rule_get_in_interface_excluded, features, sizeof(features) / sizeof(features[0]),
                              bools, sizeof(bools) / sizeof(bools[0]));
}

void test_fw_rule_set_get_source_excluded(UNUSED void** state) {
    fw_feature_t features[] = { FW_FEATURE_LAST, FW_FEATURE_SPOOFING, FW_FEATURE_SOURCE, FW_FEATURE_DSCP };
    bool bools[] = { true, false, true, true };

    test_fw_rule_set_get_bool(fw_rule_set_source_excluded,
                              fw_rule_get_source_excluded, features, sizeof(features) / sizeof(features[0]),
                              bools, sizeof(bools) / sizeof(bools[0]));
}

void test_fw_rule_set_get_source_ipset_excluded(UNUSED void** state) {
    fw_feature_t features[] = { FW_FEATURE_LAST, FW_FEATURE_SOURCE_IPSET, FW_FEATURE_SOURCE};
    bool bools[] = { false, true, false};

    test_fw_rule_set_get_bool(fw_rule_set_source_ipset_excluded,
                              fw_rule_get_source_ipset_excluded, features, sizeof(features) / sizeof(features[0]),
                              bools, sizeof(bools) / sizeof(bools[0]));
}

void test_fw_rule_set_get_source_mac_excluded(UNUSED void** state) {
    fw_feature_t features[] = { FW_FEATURE_LAST, FW_FEATURE_SOURCE_MAC, FW_FEATURE_SPOOFING, FW_FEATURE_WHITELIST };
    bool bools[] = { false, true, true, false };

    test_fw_rule_set_get_bool(fw_rule_set_source_mac_excluded,
                              fw_rule_get_source_mac_excluded, features, sizeof(features) / sizeof(features[0]),
                              bools, sizeof(bools) / sizeof(bools[0]));
}

void test_fw_rule_set_get_source_port_excluded(UNUSED void** state) {
    fw_feature_t features[] = { FW_FEATURE_LAST, FW_FEATURE_SOURCE_PORT, FW_FEATURE_SNAT, FW_FEATURE_TARGET };
    bool bools[] = { true, false, true, true };

    test_fw_rule_set_get_bool(fw_rule_set_source_port_excluded,
                              fw_rule_get_source_port_excluded, features, sizeof(features) / sizeof(features[0]),
                              bools, sizeof(bools) / sizeof(bools[0]));
}

void test_fw_rule_set_get_destination_port_excluded(UNUSED void** state) {
    fw_feature_t features[] = { FW_FEATURE_LAST, FW_FEATURE_DESTINATION_PORT, FW_FEATURE_DNAT, FW_FEATURE_TARGET };
    bool bools[] = { true, false, true, true };

    test_fw_rule_set_get_bool(fw_rule_set_destination_port_excluded,
                              fw_rule_get_destination_port_excluded, features, sizeof(features) / sizeof(features[0]),
                              bools, sizeof(bools) / sizeof(bools[0]));
}

void test_fw_rule_set_get_conntrack_state(UNUSED void** state) {
    fw_feature_t features[] = { FW_FEATURE_LAST, FW_FEATURE_DESTINATION_PORT };
    char* strings[] = { "RELATED,ESTABLISHED", "RELATED,ESTABLISHED" };

    test_fw_rule_set_get_string(fw_rule_set_conntrack_state,
                                fw_rule_get_conntrack_state, features, sizeof(features) / sizeof(features[0]),
                                strings, sizeof(strings) / sizeof(strings[0]));
}

void test_fw_rule_set_get_connbytes_min(UNUSED void** state) {
    fw_feature_t features[] = { FW_FEATURE_LAST, FW_FEATURE_DESTINATION_PORT };
    uint64_t numbers[] = { 40, 48 };

    test_fw_rule_set_get_uint64_t(fw_rule_set_connbytes_min,
                                  fw_rule_get_connbytes_min, features, sizeof(features) / sizeof(features[0]),
                                  numbers, sizeof(numbers) / sizeof(numbers[0]));
}

void test_fw_rule_set_get_connbytes_max(UNUSED void** state) {
    fw_feature_t features[] = { FW_FEATURE_LAST, FW_FEATURE_DESTINATION_PORT };
    uint64_t numbers[] = { 40, 48 };

    test_fw_rule_set_get_uint64_t(fw_rule_set_connbytes_max,
                                  fw_rule_get_connbytes_max, features, sizeof(features) / sizeof(features[0]),
                                  numbers, sizeof(numbers) / sizeof(numbers[0]));
}

void test_fw_rule_set_get_connbytes_param(UNUSED void** state) {
    fw_feature_t features[] = { FW_FEATURE_LAST, FW_FEATURE_DESTINATION_PORT };
    char* strings[] = { "packets", "bytes" };

    test_fw_rule_set_get_string(fw_rule_set_connbytes_param,
                                fw_rule_get_connbytes_param, features, sizeof(features) / sizeof(features[0]),
                                strings, sizeof(strings) / sizeof(strings[0]));
}

void test_fw_rule_set_get_connbytes_direction(UNUSED void** state) {
    fw_feature_t features[] = { FW_FEATURE_LAST, FW_FEATURE_DESTINATION_PORT };
    char* strings[] = { "reply", "both" };

    test_fw_rule_set_get_string(fw_rule_set_connbytes_direction,
                                fw_rule_get_connbytes_direction, features, sizeof(features) / sizeof(features[0]),
                                strings, sizeof(strings) / sizeof(strings[0]));
}

void test_fw_rule_set_get_skb_mark(UNUSED void** state) {
    fw_feature_t features[] = { FW_FEATURE_LAST, FW_FEATURE_DESTINATION_PORT };
    uint32_t numbers[] = { 40, 48 };

    test_fw_rule_set_get_uint32_t(fw_rule_set_skb_mark,
                                  fw_rule_get_skb_mark, features, sizeof(features) / sizeof(features[0]),
                                  numbers, sizeof(numbers) / sizeof(numbers[0]));
}

void test_fw_rule_set_get_skb_mark_mask(UNUSED void** state) {
    fw_feature_t features[] = { FW_FEATURE_LAST, FW_FEATURE_DESTINATION_PORT };
    uint32_t numbers[] = { 40, 48 };

    test_fw_rule_set_get_uint32_t(fw_rule_set_skb_mark_mask,
                                  fw_rule_get_skb_mark_mask, features, sizeof(features) / sizeof(features[0]),
                                  numbers, sizeof(numbers) / sizeof(numbers[0]));
}

void test_fw_rule_set_get_conn_mark(UNUSED void** state) {
    fw_feature_t features[] = { FW_FEATURE_LAST, FW_FEATURE_DESTINATION_PORT };
    uint32_t numbers[] = { 40, 48 };

    test_fw_rule_set_get_uint32_t(fw_rule_set_conn_mark,
                                  fw_rule_get_conn_mark, features, sizeof(features) / sizeof(features[0]),
                                  numbers, sizeof(numbers) / sizeof(numbers[0]));
}

void test_fw_rule_set_get_conn_mark_mask(UNUSED void** state) {
    fw_feature_t features[] = { FW_FEATURE_LAST, FW_FEATURE_DESTINATION_PORT };
    uint32_t numbers[] = { 40, 48 };

    test_fw_rule_set_get_uint32_t(fw_rule_set_conn_mark_mask,
                                  fw_rule_get_conn_mark_mask, features, sizeof(features) / sizeof(features[0]),
                                  numbers, sizeof(numbers) / sizeof(numbers[0]));
}

void test_fw_rule_set_filtered_icmp_code(UNUSED void** state) {
    int retval = -1;
    fw_rule_t* rule_icmp = NULL;
    fw_rule_t* rule_icmpv6 = NULL;

    retval = fw_rule_new(&rule_icmp);
    assert_int_equal(retval, 0);
    assert_non_null(rule_icmp);

    retval = fw_rule_new(&rule_icmpv6);
    assert_int_equal(retval, 0);
    assert_non_null(rule_icmpv6);

    retval = fw_rule_set_ipv4(rule_icmp, true);
    assert_int_equal(retval, 0);

    retval = fw_rule_set_ipv6(rule_icmpv6, true);
    assert_int_equal(retval, 0);

    retval = fw_rule_set_current_feature(rule_icmp, FW_FEATURE_PROTOCOL);
    assert_int_equal(retval, 0);
    retval = fw_rule_set_protocol(rule_icmp, 1);
    assert_int_equal(retval, 0);

    retval = fw_rule_set_filtered_icmp_code(rule_icmp, 6);
    assert_int_equal(retval, -1);

    retval = fw_rule_set_current_feature(rule_icmp, FW_FEATURE_LAST);
    assert_int_equal(retval, 0);
    retval = fw_rule_set_icmp_type(rule_icmp, 3);
    assert_int_equal(retval, 0);
    assert_int_equal(fw_rule_get_icmp_type(rule_icmp), 3);

    retval = fw_rule_set_filtered_icmp_code(rule_icmp, 6);
    assert_int_equal(retval, 0);
    assert_int_equal(fw_rule_get_icmp_code(rule_icmp), 6);

    retval = fw_rule_set_filtered_icmp_code(rule_icmp, 98);
    assert_int_equal(retval, -1);

    retval = fw_rule_set_filtered_icmp_code(rule_icmp, -1);
    assert_int_equal(retval, 0);
    assert_int_equal(fw_rule_get_icmp_code(rule_icmp), -1);

    retval = fw_rule_set_icmp_type(rule_icmp, 5);
    assert_int_equal(retval, 0);
    assert_int_equal(fw_rule_get_icmp_type(rule_icmp), 5);

    retval = fw_rule_set_filtered_icmp_code(rule_icmp, -1);
    assert_int_equal(retval, 0);
    assert_int_equal(fw_rule_get_icmp_code(rule_icmp), -1);


    retval = fw_rule_set_current_feature(rule_icmpv6, FW_FEATURE_PROTOCOL);
    assert_int_equal(retval, 0);
    retval = fw_rule_set_protocol(rule_icmpv6, 58);
    assert_int_equal(retval, 0);

    retval = fw_rule_set_filtered_icmpv6_code(rule_icmpv6, 5);
    assert_int_equal(retval, -1);

    retval = fw_rule_set_current_feature(rule_icmpv6, FW_FEATURE_LAST);
    assert_int_equal(retval, 0);
    retval = fw_rule_set_icmpv6_type(rule_icmpv6, 1);
    assert_int_equal(retval, 0);
    assert_int_equal(fw_rule_get_icmpv6_type(rule_icmpv6), 1);

    retval = fw_rule_set_filtered_icmpv6_code(rule_icmpv6, 5);
    assert_int_equal(retval, 0);
    assert_int_equal(fw_rule_get_icmpv6_code(rule_icmpv6), 5);

    retval = fw_rule_set_filtered_icmpv6_code(rule_icmpv6, 72);
    assert_int_equal(retval, -1);

    retval = fw_rule_set_filtered_icmpv6_code(rule_icmpv6, -1);
    assert_int_equal(retval, 0);
    assert_int_equal(fw_rule_get_icmpv6_code(rule_icmpv6), -1);

    retval = fw_rule_set_icmpv6_type(rule_icmpv6, 129);
    assert_int_equal(retval, 0);
    assert_int_equal(fw_rule_get_icmpv6_type(rule_icmpv6), 129);

    retval = fw_rule_set_filtered_icmpv6_code(rule_icmpv6, -1);
    assert_int_equal(retval, 0);
    assert_int_equal(fw_rule_get_icmpv6_code(rule_icmpv6), -1);


    retval = fw_rule_delete(&rule_icmpv6);
    assert_int_equal(retval, 0);
    assert_null(rule_icmpv6);

    retval = fw_rule_delete(&rule_icmp);
    assert_int_equal(retval, 0);
    assert_null(rule_icmp);
}

void test_fw_rule_sync(UNUSED void** state) {
    int retval = -1;
    fw_rule_t* rule = NULL;
    fw_rule_t* dest = NULL;
    const char* str_value = NULL;
    uint32_t uint32_value = 0;
    bool bool_value = false;

    retval = fw_rule_new(&rule);
    assert_int_equal(retval, 0);
    assert_non_null(rule);

    retval = fw_rule_new(&dest);
    assert_int_equal(retval, 0);
    assert_non_null(dest);

    retval = fw_rule_sync(NULL, NULL);
    assert_int_equal(retval, -1);

    retval = fw_rule_set_current_feature(dest, FW_FEATURE_PROTOCOL);
    assert_int_equal(retval, 0);
    retval = fw_rule_sync(dest, rule);
    assert_int_equal(retval, -1);
    retval = fw_rule_set_current_feature(dest, FW_FEATURE_LAST);
    assert_int_equal(retval, 0);

    retval = fw_rule_set_current_feature(rule, FW_FEATURE_IN_INTERFACE);
    assert_int_equal(retval, 0);
    retval = fw_rule_set_in_interface(rule, "ptm0");
    assert_int_equal(retval, 0);
    retval = fw_rule_set_chain(rule, "prerouting");
    assert_int_equal(retval, 0);
    retval = fw_rule_sync(dest, rule);
    assert_int_equal(retval, 0);
    str_value = fw_rule_get_in_interface(dest);
    assert_non_null(str_value);
    assert_string_equal(str_value, "ptm0");
    str_value = fw_rule_get_chain(dest);
    assert_non_null(str_value);
    assert_string_equal(str_value, "prerouting");

    retval = fw_rule_set_current_feature(rule, FW_FEATURE_DESTINATION);
    assert_int_equal(retval, 0);
    retval = fw_rule_set_destination(rule, "172.25.68.10");
    assert_int_equal(retval, 0);
    retval = fw_rule_set_destination_mask(rule, "255.255.0.0");
    assert_int_equal(retval, 0);
    retval = fw_rule_sync(dest, rule);
    assert_int_equal(retval, 0);
    str_value = fw_rule_get_destination(dest);
    assert_non_null(str_value);
    assert_string_equal(str_value, "172.25.68.10");
    str_value = fw_rule_get_destination_mask(dest);
    assert_non_null(str_value);
    assert_string_equal(str_value, "255.255.0.0");

    retval = fw_rule_set_current_feature(rule, FW_FEATURE_DESTINATION_PORT);
    assert_int_equal(retval, 0);
    retval = fw_rule_set_destination_port(rule, 22);
    assert_int_equal(retval, 0);
    retval = fw_rule_set_destination_port_range_max(rule, 5000);
    assert_int_equal(retval, 0);
    retval = fw_rule_sync(dest, rule);
    assert_int_equal(retval, 0);
    uint32_value = fw_rule_get_destination_port(dest);
    assert_int_equal(uint32_value, 22);
    uint32_value = fw_rule_get_destination_port_range_max(dest);
    assert_int_equal(uint32_value, 5000);

    retval = fw_rule_set_current_feature(rule, FW_FEATURE_PROTOCOL);
    assert_int_equal(retval, 0);
    retval = fw_rule_set_protocol(rule, 1); //ICMP
    assert_int_equal(retval, 0);
    retval = fw_rule_sync(dest, rule);
    assert_int_equal(retval, 0);
    uint32_value = fw_rule_get_protocol(dest);
    assert_int_equal(uint32_value, 1);

    retval = fw_rule_set_current_feature(rule, FW_FEATURE_SOURCE);
    assert_int_equal(retval, 0);
    retval = fw_rule_set_source(rule, "123.20.21.10");
    assert_int_equal(retval, 0);
    retval = fw_rule_set_source_mask(rule, "255.0.0.0");
    assert_int_equal(retval, 0);
    retval = fw_rule_sync(dest, rule);
    assert_int_equal(retval, 0);
    str_value = fw_rule_get_source(dest);
    assert_non_null(str_value);
    assert_string_equal(str_value, "123.20.21.10");
    str_value = fw_rule_get_source_mask(dest);
    assert_non_null(str_value);
    assert_string_equal(str_value, "255.0.0.0");

    retval = fw_rule_set_current_feature(rule, FW_FEATURE_SOURCE_MAC);
    assert_int_equal(retval, 0);
    retval = fw_rule_set_source_mac_address(rule, "ab:12:34:cd:5e");
    assert_int_equal(retval, 0);
    retval = fw_rule_set_source_mac_excluded(rule, true);
    assert_int_equal(retval, 0);
    retval = fw_rule_sync(dest, rule);
    assert_int_equal(retval, 0);
    str_value = fw_rule_get_source_mac_address(dest);
    assert_non_null(str_value);
    assert_string_equal(str_value, "ab:12:34:cd:5e");
    bool_value = fw_rule_get_source_mac_excluded(dest);
    assert_true(bool_value == true);

    retval = fw_rule_set_current_feature(rule, FW_FEATURE_WHITELIST);
    assert_int_equal(retval, 0);
    retval = fw_rule_set_source(rule, "10.20.1.15");
    assert_int_equal(retval, 0);
    retval = fw_rule_set_source_mask(rule, "255.255.0.0");
    assert_int_equal(retval, 0);
    retval = fw_rule_set_source_mac_address(rule, "ab:cb:ef:12:34");
    assert_int_equal(retval, 0);
    retval = fw_rule_set_in_interface(rule, "wwan0");
    assert_int_equal(retval, 0);
    retval = fw_rule_sync(dest, rule);
    assert_int_equal(retval, 0);
    str_value = fw_rule_get_source(dest);
    assert_non_null(str_value);
    assert_string_equal(str_value, "10.20.1.15");
    str_value = fw_rule_get_source_mask(dest);
    assert_non_null(str_value);
    assert_string_equal(str_value, "255.255.0.0");
    str_value = fw_rule_get_source_mac_address(dest);
    assert_non_null(str_value);
    assert_string_equal(str_value, "ab:cb:ef:12:34");
    str_value = fw_rule_get_in_interface(dest);
    assert_non_null(str_value);
    assert_string_equal(str_value, "wwan0");

    retval = fw_rule_set_current_feature(rule, FW_FEATURE_SOURCE_PORT);
    assert_int_equal(retval, 0);
    retval = fw_rule_set_source_port(rule, 23);
    assert_int_equal(retval, 0);
    retval = fw_rule_set_source_port_range_max(rule, 120);
    assert_int_equal(retval, 0);
    retval = fw_rule_sync(dest, rule);
    assert_int_equal(retval, 0);
    uint32_value = fw_rule_get_source_port(dest);
    assert_int_equal(uint32_value, 23);
    uint32_value = fw_rule_get_source_port_range_max(dest);
    assert_int_equal(uint32_value, 120);

    retval = fw_rule_set_current_feature(rule, FW_FEATURE_DSCP);
    assert_int_equal(retval, 0);
    retval = fw_rule_set_dscp(rule, 48);
    assert_int_equal(retval, 0);
    retval = fw_rule_sync(dest, rule);
    assert_int_equal(retval, 0);
    uint32_value = fw_rule_get_dscp(dest);
    assert_int_equal(uint32_value, 48);

    retval = fw_rule_set_current_feature(rule, FW_FEATURE_IPVERSION);
    assert_int_equal(retval, 0);
    retval = fw_rule_set_ipv6(rule, true); //Of course, this is a lie :-)
    assert_int_equal(retval, 0);
    retval = fw_rule_sync(dest, rule);
    assert_int_equal(retval, 0);
    bool_value = fw_rule_get_ipv6(dest);
    assert_true(bool_value == true);
    retval = fw_rule_set_ipv6(rule, false);
    assert_int_equal(retval, 0);
    retval = fw_rule_sync(dest, rule);
    assert_int_equal(retval, 0);
    bool_value = fw_rule_get_ipv6(dest);
    assert_true(bool_value == false);

    retval = fw_rule_set_current_feature(rule, FW_FEATURE_TABLE);
    assert_int_equal(retval, 0);
    retval = fw_rule_set_table(rule, "mangle");
    assert_int_equal(retval, 0);
    retval = fw_rule_sync(dest, rule);
    assert_int_equal(retval, 0);
    str_value = fw_rule_get_table(dest);
    assert_non_null(str_value);
    assert_string_equal(str_value, "mangle");

    retval = fw_rule_set_current_feature(rule, FW_FEATURE_DNAT);
    assert_int_equal(retval, 0);
    retval = fw_rule_set_destination_port(rule, 56);
    assert_int_equal(retval, 0);
    retval = fw_rule_set_destination_port_range_max(rule, 150);
    assert_int_equal(retval, 0);
    retval = fw_rule_sync(dest, rule);
    assert_int_equal(retval, 0);
    uint32_value = fw_rule_get_destination_port(dest);
    assert_int_equal(uint32_value, 56);
    uint32_value = fw_rule_get_destination_port_range_max(dest);
    assert_int_equal(uint32_value, 150);

    retval = fw_rule_set_current_feature(rule, FW_FEATURE_SNAT);
    assert_int_equal(retval, 0);
    retval = fw_rule_set_source_port(rule, 80);
    assert_int_equal(retval, 0);
    retval = fw_rule_set_source_port_range_max(rule, 88);
    assert_int_equal(retval, 0);
    retval = fw_rule_sync(dest, rule);
    assert_int_equal(retval, 0);
    uint32_value = fw_rule_get_source_port(dest);
    assert_int_equal(uint32_value, 80);
    uint32_value = fw_rule_get_source_port_range_max(dest);
    assert_int_equal(uint32_value, 88);

    retval = fw_rule_set_current_feature(rule, FW_FEATURE_POLICY);
    assert_int_equal(retval, 0);
    retval = fw_rule_set_in_interface(rule, "atm_data");
    assert_int_equal(retval, 0);
    retval = fw_rule_set_out_interface(rule, "bridge");
    assert_int_equal(retval, 0);
    retval = fw_rule_sync(dest, rule);
    assert_int_equal(retval, 0);
    str_value = fw_rule_get_in_interface(dest);
    assert_non_null(str_value);
    assert_string_equal(str_value, "atm_data");
    str_value = fw_rule_get_out_interface(dest);
    assert_non_null(str_value);
    assert_string_equal(str_value, "bridge");

    retval = fw_rule_set_current_feature(rule, FW_FEATURE_SPOOFING);
    assert_int_equal(retval, 0);
    retval = fw_rule_set_source(rule, "12.50.60.32");
    assert_int_equal(retval, 0);
    retval = fw_rule_set_source_mask(rule, "255.255.255.0");
    assert_int_equal(retval, 0);
    retval = fw_rule_set_in_interface_excluded(rule, true);
    assert_int_equal(retval, 0);
    retval = fw_rule_set_source_excluded(rule, true);
    assert_int_equal(retval, 0);
    retval = fw_rule_set_in_interface(rule, "wwan2");
    assert_int_equal(retval, 0);
    retval = fw_rule_set_destination(rule, "172.25.68.15");
    assert_int_equal(retval, 0);
    retval = fw_rule_set_destination_mask(rule, "255.0.0.0");
    assert_int_equal(retval, 0);
    retval = fw_rule_sync(dest, rule);
    assert_int_equal(retval, 0);
    str_value = fw_rule_get_source(dest);
    assert_non_null(str_value);
    assert_string_equal(str_value, "12.50.60.32");
    str_value = fw_rule_get_source_mask(dest);
    assert_non_null(str_value);
    assert_string_equal(str_value, "255.255.255.0");
    str_value = fw_rule_get_in_interface(dest);
    assert_non_null(str_value);
    assert_string_equal(str_value, "wwan2");
    str_value = fw_rule_get_destination(dest);
    assert_non_null(str_value);
    assert_string_equal(str_value, "172.25.68.15");
    str_value = fw_rule_get_destination_mask(dest);
    assert_non_null(str_value);
    assert_string_equal(str_value, "255.0.0.0");
    bool_value = fw_rule_get_in_interface_excluded(dest);
    assert_true(bool_value == true);
    bool_value = fw_rule_get_source_excluded(dest);
    assert_true(bool_value == true);

    retval = fw_rule_set_current_feature(rule, FW_FEATURE_ISOLATION);
    assert_int_equal(retval, 0);
    retval = fw_rule_set_in_interface(rule, "eth4.1");
    assert_int_equal(retval, 0);
    retval = fw_rule_set_destination(rule, "192.168.1.10");
    assert_int_equal(retval, 0);
    retval = fw_rule_set_destination_mask(rule, "255.255.255.0");
    assert_int_equal(retval, 0);
    retval = fw_rule_set_target_chain(rule, "output");
    assert_int_equal(retval, 0);
    retval = fw_rule_sync(dest, rule);
    assert_int_equal(retval, 0);
    str_value = fw_rule_get_in_interface(dest);
    assert_non_null(str_value);
    assert_string_equal(str_value, "eth4.1");
    str_value = fw_rule_get_destination(dest);
    assert_non_null(str_value);
    assert_string_equal(str_value, "192.168.1.10");
    str_value = fw_rule_get_destination_mask(dest);
    assert_non_null(str_value);
    assert_string_equal(str_value, "255.255.255.0");
    str_value = fw_rule_get_target_chain_option(dest);
    assert_non_null(str_value);
    assert_string_equal(str_value, "output");

    retval = fw_rule_set_current_feature(rule, FW_FEATURE_OUT_INTERFACE);
    assert_int_equal(retval, 0);
    retval = fw_rule_set_out_interface(rule, "eth0");
    assert_int_equal(retval, 0);
    retval = fw_rule_sync(dest, rule);
    assert_int_equal(retval, 0);
    str_value = fw_rule_get_out_interface(dest);
    assert_non_null(str_value);
    assert_string_equal(str_value, "eth0");

    assert_true(fw_rule_get_current_feature(dest) == FW_FEATURE_LAST);

    fw_rule_dump(rule, STDOUT_FILENO);
    fw_rule_dump(dest, STDOUT_FILENO);

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);

    retval = fw_rule_delete(&dest);
    assert_int_equal(retval, 0);
    assert_null(dest);
}

void test_fw_rule_dump(UNUSED void** state) {
    int retval = -1;
    fw_rule_t* rule = NULL;
    int fds[2]; // file descriptors array
    char buffer[9128];
    ssize_t bytes = 0;
    const char* expected_dump_rule = "\
{\n\
    chain = \"INPUT_Test\",\n\
    destination = \"172.25.68.10\",\n\
    destination_mask = \"255.255.0.0\",\n\
    destination_port = 22,\n\
    destination_port_range_max = 5000,\n\
    protocol = 1,\n\
    source = \"123.20.21.10\",\n\
    source_mask = \"255.0.0.0\",\n\
    table = \"filter\"\n\
}\n";

    retval = fw_rule_new(&rule);
    assert_int_equal(retval, 0);
    assert_non_null(rule);

    retval = fw_rule_set_current_feature(rule, FW_FEATURE_TABLE);
    assert_int_equal(retval, 0);
    retval = fw_rule_set_table(rule, "filter");
    assert_int_equal(retval, 0);

    retval = fw_rule_set_current_feature(rule, FW_FEATURE_IN_INTERFACE);
    assert_int_equal(retval, 0);
    retval = fw_rule_set_chain(rule, "INPUT_Test");
    assert_int_equal(retval, 0);

    retval = fw_rule_set_current_feature(rule, FW_FEATURE_DESTINATION);
    assert_int_equal(retval, 0);
    retval = fw_rule_set_destination(rule, "172.25.68.10");
    assert_int_equal(retval, 0);
    retval = fw_rule_set_destination_mask(rule, "255.255.0.0");
    assert_int_equal(retval, 0);

    retval = fw_rule_set_current_feature(rule, FW_FEATURE_DESTINATION_PORT);
    assert_int_equal(retval, 0);
    retval = fw_rule_set_destination_port(rule, 22);
    assert_int_equal(retval, 0);
    retval = fw_rule_set_destination_port_range_max(rule, 5000);
    assert_int_equal(retval, 0);

    retval = fw_rule_set_current_feature(rule, FW_FEATURE_PROTOCOL);
    assert_int_equal(retval, 0);
    retval = fw_rule_set_protocol(rule, 1); //ICMP
    assert_int_equal(retval, 0);

    retval = fw_rule_set_current_feature(rule, FW_FEATURE_SOURCE);
    assert_int_equal(retval, 0);
    retval = fw_rule_set_source(rule, "123.20.21.10");
    assert_int_equal(retval, 0);
    retval = fw_rule_set_source_mask(rule, "255.0.0.0");
    assert_int_equal(retval, 0);

    // Create pipe and test fw_rule_dump
    assert_int_not_equal(pipe(fds), -1);
    retval = fw_rule_dump(rule, fds[1]);
    assert_int_not_equal(retval, -1);
    bytes = read(fds[0], buffer, 9128);
    assert_in_range(bytes, 1, 9128);
    assert_true(buffer[0] != '\0');
    buffer[bytes] = 0;
    close(fds[0]);
    close(fds[1]);
    assert_string_equal(buffer, expected_dump_rule);

    //Test dumping a rule, with an invalid FD.
    retval = fw_rule_dump(rule, -1);
    assert_int_equal(retval, -1);

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);

    //Test dumping a NULL rule
    assert_int_not_equal(pipe(fds), -1);
    retval = fw_rule_dump(NULL, fds[1]);
    assert_int_equal(retval, -1);
    close(fds[0]);
    close(fds[1]);
}

static const int valid_target_features[] = {
    FW_FEATURE_TARGET,
    FW_FEATURE_DNAT,
    FW_FEATURE_SNAT,
    FW_FEATURE_SPOOFING,
    FW_FEATURE_ISOLATION,
    FW_FEATURE_POLICY,
    FW_FEATURE_DESTINATION,
    FW_FEATURE_LAST
};

static const int n_valid_target_features =
    (int) sizeof(valid_target_features) / sizeof(valid_target_features[0]);

void test_fw_rule_set_get_target_return(UNUSED void** state) {
    int retval = -1;
    fw_rule_t* rule = NULL;
    fw_rule_target_t target = FW_RULE_TARGET_LAST;

    retval = fw_rule_new(&rule);
    assert_int_equal(retval, 0);
    assert_non_null(rule);

    target = fw_rule_get_target(NULL);
    assert_int_equal(target, FW_RULE_TARGET_LAST);

    target = fw_rule_get_target(rule);
    assert_int_equal(target, FW_RULE_TARGET_NONE);

    retval = fw_rule_set_target_return(NULL);
    assert_int_equal(retval, -1);

    for(int i = 0; i < n_valid_target_features; i++) {
        retval = fw_rule_set_current_feature(rule, valid_target_features[i]);
        assert_int_equal(retval, 0);
        retval = fw_rule_set_target_return(rule);
        assert_int_equal(retval, 0);
    }
    retval = fw_rule_set_current_feature(rule, FW_FEATURE_LAST);
    assert_int_equal(retval, 0);

    target = fw_rule_get_target(rule);
    assert_int_equal(target, FW_RULE_TARGET_RETURN);

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

void test_fw_rule_set_get_target_mark(UNUSED void** state) {
    int retval = -1;
    fw_rule_t* rule = NULL;
    fw_rule_target_t target = FW_RULE_TARGET_LAST;
    uint32_t mark = 0;
    uint32_t mask = 0;

    retval = fw_rule_new(&rule);
    assert_int_equal(retval, 0);
    assert_non_null(rule);

    retval = fw_rule_get_target_mark_options(NULL, NULL, NULL);
    assert_int_equal(retval, -1);

    retval = fw_rule_get_target_mark_options(rule, NULL, NULL);
    assert_int_equal(retval, -1);

    retval = fw_rule_get_target_mark_options(rule, &mark, NULL);
    assert_int_equal(retval, -1);

    retval = fw_rule_get_target_mark_options(rule, &mark, &mask);

    retval = fw_rule_set_target_mark(NULL, 10, 5);
    assert_int_equal(retval, -1);

    for(int i = 0; i < n_valid_target_features; i++) {
        retval = fw_rule_set_current_feature(rule, valid_target_features[i]);
        assert_int_equal(retval, 0);
        retval = fw_rule_set_target_mark(rule, 20, 3);
        assert_int_equal(retval, 0);
    }
    retval = fw_rule_set_current_feature(rule, FW_FEATURE_LAST);
    assert_int_equal(retval, 0);

    target = fw_rule_get_target(rule);
    assert_int_equal(target, FW_RULE_TARGET_MARK);

    retval = fw_rule_get_target_mark_options(rule, &mark, &mask);
    assert_int_equal(retval, 0);
    assert_int_equal(mark, 20);
    assert_int_equal(mask, 3);

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

void test_fw_rule_set_get_target_classify(UNUSED void** state) {
    int retval = -1;
    fw_rule_t* rule = NULL;
    fw_rule_target_t target = FW_RULE_TARGET_LAST;
    uint32_t class = 0;

    retval = fw_rule_new(&rule);
    assert_int_equal(retval, 0);
    assert_non_null(rule);

    retval = fw_rule_get_target_classify_option(NULL, &class);
    assert_int_equal(retval, -1);

    retval = fw_rule_get_target_classify_option(rule, NULL);
    assert_int_equal(retval, -1);

    retval = fw_rule_get_target_classify_option(rule, &class);
    assert_int_equal(retval, -1);

    retval = fw_rule_set_target_classify(NULL, 1);
    assert_int_equal(retval, -1);

    for(int i = 0; i < n_valid_target_features; i++) {
        retval = fw_rule_set_current_feature(rule, valid_target_features[i]);
        assert_int_equal(retval, 0);
        retval = fw_rule_set_target_classify(rule, 1);
        assert_int_equal(retval, 0);
    }
    retval = fw_rule_set_current_feature(rule, FW_FEATURE_LAST);
    assert_int_equal(retval, 0);

    target = fw_rule_get_target(rule);
    assert_int_equal(target, FW_RULE_TARGET_CLASSIFY);

    retval = fw_rule_get_target_classify_option(rule, &class);
    assert_int_equal(retval, 0);
    assert_int_equal(class, 1);

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

void test_fw_rule_set_get_target_dscp(UNUSED void** state) {
    int retval = -1;
    fw_rule_t* rule = NULL;
    fw_rule_target_t target = FW_RULE_TARGET_LAST;
    uint32_t dscp = 0;

    retval = fw_rule_new(&rule);
    assert_int_equal(retval, 0);
    assert_non_null(rule);

    dscp = fw_rule_get_target_dscp_option(NULL);
    assert_int_equal(dscp, 0);

    dscp = fw_rule_get_target_dscp_option(rule);
    assert_int_equal(dscp, 0);

    retval = fw_rule_set_target_dscp(NULL, 46);
    assert_int_equal(retval, -1);

    for(int i = 0; i < n_valid_target_features; i++) {
        retval = fw_rule_set_current_feature(rule, valid_target_features[i]);
        assert_int_equal(retval, 0);
        retval = fw_rule_set_target_dscp(rule, 48);
        assert_int_equal(retval, 0);
    }
    retval = fw_rule_set_current_feature(rule, FW_FEATURE_LAST);
    assert_int_equal(retval, 0);

    target = fw_rule_get_target(rule);
    assert_int_equal(target, FW_RULE_TARGET_DSCP);

    dscp = fw_rule_get_target_dscp_option(rule);
    assert_int_equal(dscp, 48);

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

void test_fw_rule_set_get_target_queue(UNUSED void** state) {
    int retval = -1;
    fw_rule_t* rule = NULL;
    fw_rule_target_t target = FW_RULE_TARGET_LAST;
    uint32_t queue = 0;

    retval = fw_rule_new(&rule);
    assert_int_equal(retval, 0);
    assert_non_null(rule);

    queue = fw_rule_get_target_queue_option(NULL);
    assert_int_equal(queue, 0);

    queue = fw_rule_get_target_queue_option(rule);
    assert_int_equal(queue, 0);

    retval = fw_rule_set_target_queue(NULL, 0x801);
    assert_int_equal(retval, -1);

    for(int i = 0; i < n_valid_target_features; i++) {
        retval = fw_rule_set_current_feature(rule, valid_target_features[i]);
        assert_int_equal(retval, 0);
        retval = fw_rule_set_target_queue(rule, 0x802);
        assert_int_equal(retval, 0);
    }
    retval = fw_rule_set_current_feature(rule, FW_FEATURE_LAST);
    assert_int_equal(retval, 0);

    target = fw_rule_get_target(rule);
    assert_int_equal(target, FW_RULE_TARGET_QUEUE);

    queue = fw_rule_get_target_queue_option(rule);
    assert_int_equal(queue, 0x802);

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

void test_fw_rule_set_get_target_pbit(UNUSED void** state) {
    int retval = -1;
    fw_rule_t* rule = NULL;
    fw_rule_target_t target = FW_RULE_TARGET_LAST;
    uint32_t pbit = 0;

    retval = fw_rule_new(&rule);
    assert_int_equal(retval, 0);
    assert_non_null(rule);

    pbit = fw_rule_get_target_pbit_option(NULL);
    assert_int_equal(pbit, 0);

    pbit = fw_rule_get_target_pbit_option(rule);
    assert_int_equal(pbit, 0);

    retval = fw_rule_set_target_pbit(NULL, 5);
    assert_int_equal(retval, -1);

    for(int i = 0; i < n_valid_target_features; i++) {
        retval = fw_rule_set_current_feature(rule, valid_target_features[i]);
        assert_int_equal(retval, 0);
        retval = fw_rule_set_target_pbit(rule, 6);
        assert_int_equal(retval, 0);
    }
    retval = fw_rule_set_current_feature(rule, FW_FEATURE_LAST);
    assert_int_equal(retval, 0);

    target = fw_rule_get_target(rule);
    assert_int_equal(target, FW_RULE_TARGET_PBIT);

    pbit = fw_rule_get_target_pbit_option(rule);
    assert_int_equal(pbit, 6);

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

void test_fw_rule_set_get_target_policy(UNUSED void** state) {
    int retval = -1;
    fw_rule_t* rule = NULL;
    fw_rule_target_t target = FW_RULE_TARGET_LAST;
    fw_rule_policy_t policy = FW_RULE_POLICY_LAST;

    retval = fw_rule_new(&rule);
    assert_int_equal(retval, 0);
    assert_non_null(rule);

    policy = fw_rule_get_target_policy_option(NULL);
    assert_int_equal(policy, FW_RULE_POLICY_LAST);

    policy = fw_rule_get_target_policy_option(rule);
    assert_int_equal(policy, FW_RULE_POLICY_LAST);

    retval = fw_rule_set_target_policy(NULL, FW_RULE_POLICY_ACCEPT);
    assert_int_equal(retval, -1);

    for(int i = 0; i < n_valid_target_features; i++) {
        retval = fw_rule_set_current_feature(rule, valid_target_features[i]);
        assert_int_equal(retval, 0);
        retval = fw_rule_set_target_policy(rule, FW_RULE_POLICY_ACCEPT);
        assert_int_equal(retval, 0);
        target = fw_rule_get_target(rule);
        assert_int_equal(target, FW_RULE_TARGET_POLICY);
        policy = fw_rule_get_target_policy_option(rule);
        assert_int_equal(policy, FW_RULE_POLICY_ACCEPT);
    }
    retval = fw_rule_set_current_feature(rule, FW_FEATURE_LAST);
    assert_int_equal(retval, 0);

    retval = fw_rule_set_target_policy(rule, FW_RULE_POLICY_REJECT);
    assert_int_equal(retval, 0);
    target = fw_rule_get_target(rule);
    assert_int_equal(target, FW_RULE_TARGET_POLICY);
    policy = fw_rule_get_target_policy_option(rule);
    assert_int_equal(policy, FW_RULE_POLICY_REJECT);

    retval = fw_rule_set_target_policy(rule, FW_RULE_POLICY_DROP);
    assert_int_equal(retval, 0);
    target = fw_rule_get_target(rule);
    assert_int_equal(target, FW_RULE_TARGET_POLICY);
    policy = fw_rule_get_target_policy_option(rule);
    assert_int_equal(policy, FW_RULE_POLICY_DROP);

    retval = fw_rule_set_target_policy(rule, FW_RULE_POLICY_MASQUERADE);
    assert_int_equal(retval, 0);
    target = fw_rule_get_target(rule);
    assert_int_equal(target, FW_RULE_TARGET_POLICY);
    policy = fw_rule_get_target_policy_option(rule);
    assert_int_equal(policy, FW_RULE_POLICY_MASQUERADE);

    retval = fw_rule_set_target_policy(rule, FW_RULE_POLICY_LAST);
    assert_int_equal(retval, -1);
    target = fw_rule_get_target(rule);
    assert_int_equal(target, FW_RULE_TARGET_POLICY);
    policy = fw_rule_get_target_policy_option(rule);
    assert_int_equal(policy, FW_RULE_POLICY_MASQUERADE);

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

void test_fw_rule_set_get_target_nat(UNUSED void** state) {
    int retval = -1;
    fw_rule_t* rule = NULL;
    fw_rule_target_t target = FW_RULE_TARGET_LAST;
    const char* ip = NULL;
    const char* ip_not_null;
    uint32_t min_port = 0;
    uint32_t max_port = 0;

    retval = fw_rule_new(&rule);
    assert_int_equal(retval, 0);
    assert_non_null(rule);

    retval = fw_rule_set_target_dnat(NULL, NULL, 0, 0);
    assert_int_equal(retval, -1);
    retval = fw_rule_set_target_snat(NULL, NULL, 0, 0);
    assert_int_equal(retval, -1);

    retval = fw_rule_set_target_dnat(NULL, "192.168.10.1", 0, 0);
    assert_int_equal(retval, -1);
    retval = fw_rule_set_target_snat(NULL, "15.47.69.10", 0, 0);
    assert_int_equal(retval, -1);

    retval = fw_rule_get_target_dnat_options(NULL, NULL, NULL, NULL);
    assert_int_equal(retval, -1);
    retval = fw_rule_get_target_snat_options(NULL, NULL, NULL, NULL);
    assert_int_equal(retval, -1);

    retval = fw_rule_get_target_dnat_options(NULL, NULL, NULL, &max_port);
    assert_int_equal(retval, -1);
    retval = fw_rule_get_target_snat_options(NULL, NULL, NULL, &max_port);
    assert_int_equal(retval, -1);

    retval = fw_rule_get_target_dnat_options(NULL, NULL, &min_port, &max_port);
    assert_int_equal(retval, -1);
    retval = fw_rule_get_target_snat_options(NULL, NULL, &min_port, &max_port);
    assert_int_equal(retval, -1);

    retval = fw_rule_get_target_dnat_options(NULL, &ip_not_null, &min_port, &max_port);
    assert_int_equal(retval, -1);
    retval = fw_rule_get_target_snat_options(NULL, &ip_not_null, &min_port, &max_port);
    assert_int_equal(retval, -1);

    for(int i = 0; i < n_valid_target_features; i++) {
        retval = fw_rule_set_current_feature(rule, valid_target_features[i]);
        assert_int_equal(retval, 0);
        retval = fw_rule_set_target_dnat(rule, "192.168.10.1", 80, 600);
        assert_int_equal(retval, 0);
        target = fw_rule_get_target(rule);
        assert_int_equal(target, FW_RULE_TARGET_DNAT);
        retval = fw_rule_get_target_dnat_options(rule, &ip, &min_port, &max_port);
        assert_int_equal(retval, 0);
        assert_non_null(ip);
        assert_string_equal(ip, "192.168.10.1");
        assert_int_equal(min_port, 80);
        assert_int_equal(max_port, 600);
        ip = NULL;
    }
    retval = fw_rule_set_current_feature(rule, FW_FEATURE_LAST);
    assert_int_equal(retval, 0);

    for(int i = 0; i < n_valid_target_features; i++) {
        retval = fw_rule_set_current_feature(rule, valid_target_features[i]);
        assert_int_equal(retval, 0);
        retval = fw_rule_set_target_snat(rule, "15.47.69.10", 22, 23);
        assert_int_equal(retval, 0);
        target = fw_rule_get_target(rule);
        assert_int_equal(target, FW_RULE_TARGET_SNAT);
        retval = fw_rule_get_target_snat_options(rule, &ip, &min_port, &max_port);
        assert_int_equal(retval, 0);
        assert_non_null(ip);
        assert_string_equal(ip, "15.47.69.10");
        assert_int_equal(min_port, 22);
        assert_int_equal(max_port, 23);
        ip = NULL;
    }
    retval = fw_rule_set_current_feature(rule, FW_FEATURE_LAST);
    assert_int_equal(retval, 0);

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

void test_fw_rule_set_targets(UNUSED void** state) {
    int retval = -1;
    fw_rule_t* rule = NULL;

    retval = fw_rule_new(&rule);
    assert_int_equal(retval, 0);
    assert_non_null(rule);

    retval = fw_rule_set_target_return(rule);
    assert_int_equal(retval, 0);

    retval = fw_rule_set_target_mark(rule, 20, 3);
    assert_int_equal(retval, 0);

    retval = fw_rule_set_target_dnat(rule, "192.168.10.1", 80, 600);
    assert_int_equal(retval, 0);

    retval = fw_rule_set_target_policy(rule, FW_RULE_POLICY_REJECT);
    assert_int_equal(retval, 0);

    retval = fw_rule_set_target_chain(rule, "prerouting");
    assert_int_equal(retval, 0);

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

void test_fw_rule_set_target_invalid_feature(UNUSED void** state) {
    int retval = -1;
    fw_rule_t* rule = NULL;
    const fw_feature_t invalid_features[] = {
        FW_FEATURE_IN_INTERFACE,
        FW_FEATURE_DESTINATION_PORT,
        FW_FEATURE_PROTOCOL,
        FW_FEATURE_SOURCE,
        FW_FEATURE_SOURCE_MAC,
        FW_FEATURE_WHITELIST,
        FW_FEATURE_SOURCE_PORT,
        FW_FEATURE_DSCP,
        FW_FEATURE_IPVERSION,
        FW_FEATURE_TABLE
    };
    const int n_invalid_features = sizeof(invalid_features) / sizeof(invalid_features[0]);

    retval = fw_rule_new(&rule);
    assert_int_equal(retval, 0);
    assert_non_null(rule);

    for(int i = 0; i < n_invalid_features; i++) {
        retval = fw_rule_set_current_feature(rule, invalid_features[i]);
        assert_int_equal(retval, 0);
        retval = fw_rule_set_target_return(rule);
        assert_int_equal(retval, -1);
    }

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

void test_fw_rule_sync_target(UNUSED void** state) {
    int retval = -1;
    fw_rule_t* rule = NULL;
    fw_rule_t* dest = NULL;
    fw_rule_target_t target = FW_RULE_TARGET_LAST;
    uint32_t min_port = 0;
    uint32_t max_port = 0;
    const char* ip = NULL;
    uint32_t mark = 0;
    uint32_t mask = 0;

    retval = fw_rule_new(&rule);
    assert_int_equal(retval, 0);
    assert_non_null(rule);

    retval = fw_rule_new(&dest);
    assert_int_equal(retval, 0);
    assert_non_null(rule);

    retval = fw_rule_set_current_feature(rule, FW_FEATURE_TARGET);
    assert_int_equal(retval, 0);
    retval = fw_rule_set_target_return(rule);
    assert_int_equal(retval, 0);
    retval = fw_rule_sync(dest, rule);
    assert_int_equal(retval, 0);
    target = fw_rule_get_target(dest);
    assert_true(target == FW_RULE_TARGET_RETURN);

    retval = fw_rule_set_current_feature(rule, FW_FEATURE_SPOOFING);
    assert_int_equal(retval, 0);
    retval = fw_rule_set_target_mark(rule, 1200, 5);
    assert_int_equal(retval, 0);
    retval = fw_rule_sync(dest, rule);
    assert_int_equal(retval, 0);
    target = fw_rule_get_target(dest);
    assert_true(target == FW_RULE_TARGET_MARK);
    retval = fw_rule_get_target_mark_options(dest, &mark, &mask);
    assert_int_equal(retval, 0);
    assert_int_equal(mark, 1200);
    assert_int_equal(mask, 5);
    retval = fw_rule_set_current_feature(rule, FW_FEATURE_DNAT);
    retval = fw_rule_set_target_dnat(rule, "192.168.10.1", 80, 600);
    assert_int_equal(retval, 0);
    retval = fw_rule_sync(dest, rule);
    assert_int_equal(retval, 0);
    target = fw_rule_get_target(dest);
    assert_int_equal(target, FW_RULE_TARGET_DNAT);
    retval = fw_rule_get_target_dnat_options(dest, &ip, &min_port, &max_port);
    assert_int_equal(retval, 0);
    assert_non_null(ip);
    assert_string_equal(ip, "192.168.10.1");
    assert_int_equal(min_port, 80);
    assert_int_equal(max_port, 600);
    ip = NULL;

    retval = fw_rule_set_current_feature(rule, FW_FEATURE_LAST);
    assert_int_equal(retval, 0);
    retval = fw_rule_set_target_return(rule);
    assert_int_equal(retval, 0);
    retval = fw_rule_sync(dest, rule);
    assert_int_equal(retval, 0);
    target = fw_rule_get_target(dest);
    assert_true(target == FW_RULE_TARGET_DNAT);

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);

    retval = fw_rule_delete(&dest);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

void test_fw_rule_set_get_target_chain(UNUSED void** state) {
    int retval = -1;
    fw_rule_t* rule = NULL;
    fw_rule_target_t target = FW_RULE_TARGET_LAST;
    const char* chain = NULL;

    retval = fw_rule_new(&rule);
    assert_int_equal(retval, 0);
    assert_non_null(rule);

    chain = fw_rule_get_target_chain_option(NULL);
    assert_null(chain);

    chain = fw_rule_get_target_chain_option(rule);
    assert_null(chain);

    retval = fw_rule_set_target_chain(NULL, NULL);
    assert_int_equal(retval, -1);

    retval = fw_rule_set_target_chain(NULL, "input");
    assert_int_equal(retval, -1);

    for(int i = 0; i < n_valid_target_features; i++) {
        retval = fw_rule_set_current_feature(rule, valid_target_features[i]);
        assert_int_equal(retval, 0);
        retval = fw_rule_set_target_chain(rule, "postrouting");
        assert_int_equal(retval, 0);
    }
    retval = fw_rule_set_current_feature(rule, FW_FEATURE_LAST);
    assert_int_equal(retval, 0);

    target = fw_rule_get_target(rule);
    assert_int_equal(target, FW_RULE_TARGET_CHAIN);

    chain = fw_rule_get_target_chain_option(rule);
    assert_non_null(chain);
    assert_string_equal(chain, "postrouting");

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

void test_fw_rule_set_get_out_interface_excluded(UNUSED void** state) {
    fw_feature_t features[] = { FW_FEATURE_LAST, FW_FEATURE_LAST, FW_FEATURE_TARGET };
    bool bools[] = { true, false, false };

    test_fw_rule_set_get_bool(fw_rule_set_out_interface_excluded,
                              fw_rule_get_out_interface_excluded, features, sizeof(features) / sizeof(features[0]),
                              bools, sizeof(bools) / sizeof(bools[0]));
}

void test_fw_rule_set_get_destination_excluded(UNUSED void** state) {
    fw_feature_t features[] = { FW_FEATURE_LAST, FW_FEATURE_LAST, FW_FEATURE_DESTINATION, FW_FEATURE_DSCP };
    bool bools[] = { true, false, true, false };

    test_fw_rule_set_get_bool(fw_rule_set_destination_excluded,
                              fw_rule_get_destination_excluded, features, sizeof(features) / sizeof(features[0]),
                              bools, sizeof(bools) / sizeof(bools[0]));
}

void test_fw_rule_set_get_destination_ipset_excluded(UNUSED void** state) {
    fw_feature_t features[] = { FW_FEATURE_LAST, FW_FEATURE_DESTINATION_IPSET, FW_FEATURE_DESTINATION};
    bool bools[] = { false, true, false};

    test_fw_rule_set_get_bool(fw_rule_set_destination_ipset_excluded,
                              fw_rule_get_destination_ipset_excluded, features, sizeof(features) / sizeof(features[0]),
                              bools, sizeof(bools) / sizeof(bools[0]));
}

void test_fw_rule_set_get_destination_mac_excluded(UNUSED void** state) {
    fw_feature_t features[] = { FW_FEATURE_LAST, FW_FEATURE_LAST, FW_FEATURE_SPOOFING };
    bool bools[] = { false, true, true };

    test_fw_rule_set_get_bool(fw_rule_set_destination_mac_excluded,
                              fw_rule_get_destination_mac_excluded, features, sizeof(features) / sizeof(features[0]),
                              bools, sizeof(bools) / sizeof(bools[0]));
}

void test_fw_rule_set_get_protocol_excluded(UNUSED void** state) {
    fw_feature_t features[] = { FW_FEATURE_LAST, FW_FEATURE_LAST, FW_FEATURE_DSCP };
    bool bools[] = { true, false, false };

    test_fw_rule_set_get_bool(fw_rule_set_protocol_excluded,
                              fw_rule_get_protocol_excluded, features, sizeof(features) / sizeof(features[0]),
                              bools, sizeof(bools) / sizeof(bools[0]));
}

void test_fw_rule_set_get_ip_min_length(UNUSED void** state) {
    fw_feature_t features[] = { FW_FEATURE_LAST, FW_FEATURE_LAST, FW_FEATURE_LAST, FW_FEATURE_TARGET };
    uint32_t numbers[] = { 40, 48, 30, 30};

    test_fw_rule_set_get_uint32_t(fw_rule_set_ip_min_length,
                                  fw_rule_get_ip_min_length, features, sizeof(features) / sizeof(features[0]),
                                  numbers, sizeof(numbers) / sizeof(numbers[0]));
}

void test_fw_rule_set_get_ip_max_length(UNUSED void** state) {
    fw_feature_t features[] = { FW_FEATURE_LAST, FW_FEATURE_LAST, FW_FEATURE_LAST, FW_FEATURE_TARGET };
    uint32_t numbers[] = { 40, 48, 30, 30};

    test_fw_rule_set_get_uint32_t(fw_rule_set_ip_max_length,
                                  fw_rule_get_ip_max_length, features, sizeof(features) / sizeof(features[0]),
                                  numbers, sizeof(numbers) / sizeof(numbers[0]));
}

void test_fw_rule_set_get_icmp_type(UNUSED void** state) {
    fw_feature_t features[] = { FW_FEATURE_LAST, FW_FEATURE_LAST, FW_FEATURE_TARGET };
    int32_t numbers[] = { 0, 3, 3};

    test_fw_rule_set_get_int32_t(fw_rule_set_icmp_type,
                                 fw_rule_get_icmp_type, features, sizeof(features) / sizeof(features[0]),
                                 numbers, sizeof(numbers) / sizeof(numbers[0]), false);
}

void test_fw_rule_set_get_icmpv6_type(UNUSED void** state) {
    fw_feature_t features[] = { FW_FEATURE_LAST, FW_FEATURE_LAST, FW_FEATURE_TARGET };
    int32_t numbers[] = { 0, 4, 4};

    test_fw_rule_set_get_int32_t(fw_rule_set_icmpv6_type,
                                 fw_rule_get_icmpv6_type, features, sizeof(features) / sizeof(features[0]),
                                 numbers, sizeof(numbers) / sizeof(numbers[0]), true);
}

void test_fw_rule_set_get_icmp_code(UNUSED void** state) {
    fw_feature_t features[] = { FW_FEATURE_LAST, FW_FEATURE_LAST, FW_FEATURE_TARGET };
    int32_t numbers[] = { 0, 6, 6};

    test_fw_rule_set_get_int32_t(fw_rule_set_icmp_code,
                                 fw_rule_get_icmp_code, features, sizeof(features) / sizeof(features[0]),
                                 numbers, sizeof(numbers) / sizeof(numbers[0]), false);
}

void test_fw_rule_set_get_icmpv6_code(UNUSED void** state) {
    fw_feature_t features[] = { FW_FEATURE_LAST, FW_FEATURE_LAST, FW_FEATURE_TARGET };
    int32_t numbers[] = { 0, 2, 2};

    test_fw_rule_set_get_int32_t(fw_rule_set_icmpv6_code,
                                 fw_rule_get_icmpv6_code, features, sizeof(features) / sizeof(features[0]),
                                 numbers, sizeof(numbers) / sizeof(numbers[0]), true);
}

void test_fw_rule_set_get_dscp_excluded(UNUSED void** state) {
    fw_feature_t features[] = { FW_FEATURE_LAST, FW_FEATURE_DSCP, FW_FEATURE_SOURCE_PORT };
    bool bools[] = { true, false, false };

    test_fw_rule_set_get_bool(fw_rule_set_dscp_excluded,
                              fw_rule_get_dscp_excluded, features, sizeof(features) / sizeof(features[0]),
                              bools, sizeof(bools) / sizeof(bools[0]));
}

void test_fw_rule_set_get_skb_mark_excluded(UNUSED void** state) {
    fw_feature_t features[] = { FW_FEATURE_LAST, FW_FEATURE_SOURCE_PORT };
    bool bools[] = { true, false };

    test_fw_rule_set_get_bool(fw_rule_set_skb_mark_excluded,
                              fw_rule_get_skb_mark_excluded, features, sizeof(features) / sizeof(features[0]),
                              bools, sizeof(bools) / sizeof(bools[0]));
}

void test_fw_rule_set_get_conn_mark_excluded(UNUSED void** state) {
    fw_feature_t features[] = { FW_FEATURE_LAST, FW_FEATURE_SOURCE_PORT };
    bool bools[] = { true, false };

    test_fw_rule_set_get_bool(fw_rule_set_conn_mark_excluded,
                              fw_rule_get_conn_mark_excluded, features, sizeof(features) / sizeof(features[0]),
                              bools, sizeof(bools) / sizeof(bools[0]));
}

void test_fw_rule_clone(UNUSED void** state) {
    int retval = -1;
    fw_rule_t* rule = NULL;
    fw_rule_t* clone = NULL;

    retval = fw_rule_new(&rule);
    assert_int_equal(retval, 0);
    assert_non_null(rule);

    retval |= fw_rule_set_table(rule, "mangle");
    retval |= fw_rule_set_chain(rule, "PREROUTING");
    retval |= fw_rule_set_target_dscp(rule, 0xef);
    retval |= fw_rule_set_enabled(rule, true);
    assert_int_equal(retval, 0);

    retval = fw_rule_clone(&clone, rule);
    assert_int_equal(retval, 0);
    assert_non_null(clone);

    assert_string_equal("mangle", fw_rule_get_table(clone));
    assert_string_equal("PREROUTING", fw_rule_get_chain(clone));
    assert_true(fw_rule_is_enabled(clone));

    retval = fw_rule_delete(&clone);
    assert_int_equal(retval, 0);
    assert_null(clone);

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

void test_fw_rule_set_get_target_nfqueue(UNUSED void** state) {
    int retval = -1;
    fw_rule_t* rule = NULL;
    fw_rule_target_t target = FW_RULE_TARGET_LAST;
    uint32_t qnum = 0;
    uint32_t qtotal = 0;
    uint32_t flags = 0;

    retval = fw_rule_new(&rule);
    assert_int_equal(retval, 0);
    assert_non_null(rule);

    retval = fw_rule_set_target_nfqueue_options(NULL, 2, 1, FW_RULE_NFQ_FLAG_BYPASS);
    assert_int_equal(retval, -1);

    retval = fw_rule_get_target_nfqueue_options(NULL, NULL, NULL, &flags);
    assert_int_equal(retval, -1);

    retval = fw_rule_get_target_nfqueue_options(NULL, NULL, &qtotal, &flags);
    assert_int_equal(retval, -1);

    retval = fw_rule_get_target_nfqueue_options(NULL, &qnum, &qtotal, &flags);
    assert_int_equal(retval, -1);

    for(int i = 0; i < n_valid_target_features; i++) {
        retval = fw_rule_set_current_feature(rule, valid_target_features[i]);
        assert_int_equal(retval, 0);
        retval = fw_rule_set_target_nfqueue_options(rule, 2, 1, FW_RULE_NFQ_FLAG_BYPASS);
        assert_int_equal(retval, 0);
        target = fw_rule_get_target(rule);
        assert_int_equal(target, FW_RULE_TARGET_NFQUEUE);
        retval = fw_rule_get_target_nfqueue_options(rule, &qnum, &qtotal, &flags);
        assert_int_equal(retval, 0);
        assert_int_equal(qnum, 2);
        assert_int_equal(qtotal, 1);
        assert_int_equal(flags, FW_RULE_NFQ_FLAG_BYPASS);
    }

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

void test_fw_rule_set_get_target_nflog(UNUSED void** state) {
    int retval = -1;
    fw_rule_t* rule = NULL;
    fw_rule_target_t target = FW_RULE_TARGET_LAST;
    uint32_t gnum = 0;
    uint32_t len = 0;
    uint32_t threshold = 0;
    uint32_t flags = 0;
    const char* prefix = "";

    retval = fw_rule_new(&rule);
    assert_int_equal(retval, 0);
    assert_non_null(rule);

    retval = fw_rule_set_target_nflog_options(NULL, 2, 5, 10, 0x20, "prefix");
    assert_int_equal(retval, -1);

    retval = fw_rule_get_target_nflog_options(NULL, NULL, NULL, NULL, NULL, &prefix);
    assert_int_equal(retval, -1);

    retval = fw_rule_get_target_nflog_options(NULL, NULL, NULL, NULL, &flags, &prefix);
    assert_int_equal(retval, -1);

    retval = fw_rule_get_target_nflog_options(NULL, NULL, NULL, &threshold, &flags, &prefix);
    assert_int_equal(retval, -1);

    retval = fw_rule_get_target_nflog_options(NULL, NULL, &len, &threshold, &flags, &prefix);
    assert_int_equal(retval, -1);

    retval = fw_rule_get_target_nflog_options(NULL, &gnum, &len, &threshold, &flags, &prefix);
    assert_int_equal(retval, -1);

    for(int i = 0; i < n_valid_target_features; i++) {
        retval = fw_rule_set_target_nflog_options(rule, 2, 5, 10, 0x20, "prefix");
        assert_int_equal(retval, 0);
        target = fw_rule_get_target(rule);
        assert_int_equal(target, FW_RULE_TARGET_NFLOG);
        retval = fw_rule_get_target_nflog_options(rule, &gnum, &len, &threshold, &flags, &prefix);
        assert_int_equal(retval, 0);
        assert_int_equal(gnum, 2);
        assert_int_equal(len, 5);
        assert_int_equal(threshold, 10);
        assert_int_equal(flags, 0x20);
        assert_string_equal(prefix, "prefix");
    }

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

void test_fw_rule_set_get_target_log(UNUSED void** state) {
    int retval = -1;
    fw_rule_t* rule = NULL;
    fw_rule_target_t target = FW_RULE_TARGET_LAST;
    uint8_t level = 0;
    uint8_t logflags = 0;
    const char* prefix = "";

    retval = fw_rule_new(&rule);
    assert_int_equal(retval, 0);
    assert_non_null(rule);

    retval = fw_rule_set_target_log_options(NULL, 5, 0x20, "prefix");
    assert_int_equal(retval, -1);

    retval = fw_rule_get_target_log_options(NULL, NULL, NULL, &prefix);
    assert_int_equal(retval, -1);

    retval = fw_rule_get_target_log_options(NULL, NULL, &logflags, &prefix);
    assert_int_equal(retval, -1);

    retval = fw_rule_get_target_log_options(NULL, &level, &logflags, &prefix);
    assert_int_equal(retval, -1);

    for(int i = 0; i < n_valid_target_features; i++) {
        retval = fw_rule_set_target_log_options(rule, 5, 0x20, "prefix");
        assert_int_equal(retval, 0);
        target = fw_rule_get_target(rule);
        assert_int_equal(target, FW_RULE_TARGET_LOG);
        retval = fw_rule_get_target_log_options(rule, &level, &logflags, &prefix);
        assert_int_equal(retval, 0);
        assert_int_equal(level, 5);
        assert_int_equal(logflags, 0x20);
        assert_string_equal(prefix, "prefix");
    }

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

void test_fw_rule_set_get_filter_string(UNUSED void** state) {
    int retval = -1;
    fw_rule_t* rule = NULL;
    fw_feature_t features[] = { FW_FEATURE_STRING };
    size_t n_features = sizeof(features) / sizeof(features[0]);
    const char* string = NULL;

    retval = fw_rule_new(&rule);
    assert_int_equal(retval, 0);
    assert_non_null(rule);

    string = fw_rule_get_filter_string(NULL);
    assert_null(string);

    string = fw_rule_get_filter_string(rule);
    assert_null(string);

    retval = fw_rule_set_filter_string(NULL, NULL);
    assert_int_equal(retval, -1);

    retval = fw_rule_set_filter_string(NULL, "GET /index.html");
    assert_int_equal(retval, -1);

    for(size_t i = 0; i < n_features; i++) {
        retval = fw_rule_set_current_feature(rule, features[i]);
        assert_int_equal(retval, 0);
        retval = fw_rule_set_filter_string(rule, "GET /index.html");
        assert_int_equal(retval, 0);
    }
    retval = fw_rule_set_current_feature(rule, FW_FEATURE_LAST);
    assert_int_equal(retval, 0);

    string = fw_rule_get_filter_string(rule);
    assert_non_null(string);
    assert_string_equal(string, "GET /index.html");

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

void test_fw_rule_set_get_time(UNUSED void** state) {
    int retval = -1;
    fw_rule_t* rule = NULL;
    fw_feature_t features[] = { FW_FEATURE_TIME };
    size_t n_features = sizeof(features) / sizeof(features[0]);
    const char* weekdays = NULL;
    const char* start = NULL;
    const char* end = NULL;

    retval = fw_rule_new(&rule);
    assert_int_equal(retval, 0);
    assert_non_null(rule);

    weekdays = fw_rule_get_filter_weekdays(NULL);
    assert_null(weekdays);

    weekdays = fw_rule_get_filter_weekdays(rule);
    assert_null(weekdays);

    retval = fw_rule_set_filter_weekdays(NULL, NULL);
    assert_int_equal(retval, -1);

    retval = fw_rule_set_filter_weekdays(NULL, "Monday");
    assert_int_equal(retval, -1);

    start = fw_rule_get_filter_start_time(NULL);
    assert_null(start);

    start = fw_rule_get_filter_start_time(rule);
    assert_null(start);

    retval = fw_rule_set_filter_start_time(NULL, NULL);
    assert_int_equal(retval, -1);

    retval = fw_rule_set_filter_start_time(NULL, "00:00");
    assert_int_equal(retval, -1);

    end = fw_rule_get_filter_end_time(NULL);
    assert_null(end);

    end = fw_rule_get_filter_end_time(rule);
    assert_null(end);

    retval = fw_rule_set_filter_end_time(NULL, NULL);
    assert_int_equal(retval, -1);

    retval = fw_rule_set_filter_end_time(NULL, "23:59");
    assert_int_equal(retval, -1);

    for(size_t i = 0; i < n_features; i++) {
        retval = fw_rule_set_current_feature(rule, features[i]);
        assert_int_equal(retval, 0);
        retval = fw_rule_set_filter_weekdays(rule, "Monday");
        assert_int_equal(retval, 0);
        retval = fw_rule_set_filter_start_time(rule, "00:00");
        assert_int_equal(retval, 0);
        retval = fw_rule_set_filter_end_time(rule, "23:59");
        assert_int_equal(retval, 0);
    }
    retval = fw_rule_set_current_feature(rule, FW_FEATURE_LAST);
    assert_int_equal(retval, 0);

    weekdays = fw_rule_get_filter_weekdays(rule);
    assert_non_null(weekdays);
    assert_string_equal(weekdays, "Monday");

    start = fw_rule_get_filter_start_time(rule);
    assert_non_null(start);
    assert_string_equal(start, "00:00");

    end = fw_rule_get_filter_end_time(rule);
    assert_non_null(end);
    assert_string_equal(end, "23:59");

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

void test_fw_rule_set_get_physdev(UNUSED void** state) {
    int retval = -1;
    fw_rule_t* rule = NULL;
    const char* physdev_out = "eth0";
    const char* physdev_in = "eth1";
    const char* physdev_get = NULL;

    retval = fw_rule_new(&rule);
    assert_int_equal(retval, 0);
    retval = fw_rule_set_chain(rule, "FORWARD");
    assert_int_equal(retval, 0);
    retval = fw_rule_set_table(rule, "mangle");
    assert_int_equal(retval, 0);
    retval = fw_rule_set_physdev_out(rule, physdev_out);
    assert_int_equal(retval, 0);
    retval = fw_rule_set_physdev_out_excluded(rule, true);
    assert_int_equal(retval, 0);
    retval = fw_rule_set_physdev_in(rule, physdev_in);
    assert_int_equal(retval, 0);
    retval = fw_rule_set_physdev_in_excluded(rule, true);
    assert_int_equal(retval, 0);

    physdev_get = fw_rule_get_physdev_out(rule);
    assert_non_null(physdev_get);
    assert_string_equal(physdev_get, physdev_out);
    assert_true(fw_rule_get_physdev_out_excluded(rule));

    physdev_get = fw_rule_get_physdev_in(rule);
    assert_non_null(physdev_get);
    assert_string_equal(physdev_get, physdev_in);
    assert_true(fw_rule_get_physdev_in_excluded(rule));

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
}
