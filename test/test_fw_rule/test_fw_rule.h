/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef __TEST_FW_RULE_H__
#define __TEST_FW_RULE_H__

void test_fw_rule_new_delete(void** state);
void test_fw_rule_set_feature_mark(void** state);
void test_fw_rule_set_enabled(void** state);
void test_fw_rule_set_flag(void** state);
void test_fw_rule_current_feature(void** state);
void test_fw_rule_set_get_ipv4_ipv6(void** state);
void test_fw_rule_set_get_table(void** state);
void test_fw_rule_set_get_source(void** state);
void test_fw_rule_set_get_source_mask(void** state);
void test_fw_rule_set_get_source_ipset(void** state);
void test_fw_rule_set_get_in_interface(void** state);
void test_fw_rule_set_get_chain(void** state);
void test_fw_rule_set_get_destination(void** state);
void test_fw_rule_set_get_destination_mask(void** state);
void test_fw_rule_set_get_destination_ipset(void** state);
void test_fw_rule_set_get_destination_port(void** state);
void test_fw_rule_set_get_destination_port_range(void** state);
void test_fw_rule_set_get_source_port(void** state);
void test_fw_rule_set_get_source_port_range(void** state);
void test_fw_rule_set_get_protocol(void** state);
void test_fw_rule_set_get_source_mac_address(void** state);
void test_fw_rule_set_get_dscp(void** state);
void test_fw_rule_set_get_out_interface(void** state);
void test_fw_rule_set_get_in_interface_excluded(void** state);
void test_fw_rule_set_get_source_excluded(void** state);
void test_fw_rule_set_get_source_ipset_excluded(void** state);
void test_fw_rule_set_get_source_mac_excluded(void** state);
void test_fw_rule_set_get_conntrack_state(void** state);
void test_fw_rule_set_get_connbytes_min(void** state);
void test_fw_rule_set_get_connbytes_max(void** state);
void test_fw_rule_set_get_connbytes_param(void** state);
void test_fw_rule_set_get_connbytes_direction(void** state);
void test_fw_rule_set_get_skb_mark(void** state);
void test_fw_rule_set_get_skb_mark_mask(void** state);
void test_fw_rule_set_get_conn_mark(void** state);
void test_fw_rule_set_get_conn_mark_mask(void** state);
void test_fw_rule_set_get_skb_mark_excluded(void** state);
void test_fw_rule_set_get_conn_mark_excluded(void** state);
void test_fw_rule_sync(void** state);
void test_fw_rule_set_filtered_icmp_code(void** state);
void test_fw_rule_dump(void** state);
void test_fw_rule_set_get_target_return(void** state);
void test_fw_rule_set_get_target_mark(void** state);
void test_fw_rule_set_get_target_classify(void** state);
void test_fw_rule_set_get_target_dscp(void** state);
void test_fw_rule_set_get_target_queue(void** state);
void test_fw_rule_set_get_target_pbit(void** state);
void test_fw_rule_set_get_target_policy(void** state);
void test_fw_rule_set_get_target_nat(void** state);
void test_fw_rule_set_targets(void** state);
void test_fw_rule_set_target_invalid_feature(void** state);
void test_fw_rule_sync_target(void** state);
void test_fw_rule_set_get_target_chain(void** state);
void test_fw_rule_set_get_out_interface_excluded(void** state);
void test_fw_rule_set_get_destination_excluded(void** state);
void test_fw_rule_set_get_destination_ipset_excluded(void** state);
void test_fw_rule_set_get_destination_mac_excluded(void** state);
void test_fw_rule_set_get_protocol_excluded(void** state);
void test_fw_rule_set_get_ip_min_length(void** state);
void test_fw_rule_set_get_ip_max_length(void** state);
void test_fw_rule_set_get_icmp_type(void** state);
void test_fw_rule_set_get_icmpv6_type(void** state);
void test_fw_rule_set_get_icmp_code(void** state);
void test_fw_rule_set_get_icmpv6_code(void** state);
void test_fw_rule_set_get_source_port_excluded(void** state);
void test_fw_rule_set_get_destination_port_excluded(void** state);
void test_fw_rule_set_get_dscp_excluded(void** state);
void test_fw_rule_clone(void** state);
void test_fw_rule_set_get_target_nfqueue(void** state);
void test_fw_rule_set_get_target_nflog(void** state);
void test_fw_rule_set_get_target_log(void** state);
void test_fw_rule_set_get_filter_string(void** state);
void test_fw_rule_set_get_time(void** state);
void test_fw_rule_set_get_physdev(void** state);

#endif // __TEST_FW_RULE_H__
